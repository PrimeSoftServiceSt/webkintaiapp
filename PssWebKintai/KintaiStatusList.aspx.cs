﻿using PssCommonLib;
using PssWebKintai.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PssWebKintai
{
    public partial class KintaiStatusList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    var start = DateTime.Today.GetFirstDateOfMonth();
                    // 年月ドロップダウンのデータ作成
                    drpNengetsu.SetItem(CreateDropMonthDataSource(start));

                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var selectedY = int.Parse(drpNengetsu.SelectedValue.Substring(0, 4));
            var selectedM = int.Parse(drpNengetsu.SelectedValue.Substring(4, 2));
        }

        protected void TORIKESHI_Click(object sender, EventArgs e)
        {

        }

        /// <summary> 年月ドロップダウンデータソース作成
        /// </summary>
        /// <param name="start"></param>
        /// <returns></returns>
        private List<ListItem> CreateDropMonthDataSource(DateTime start)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var lstMonth = new List<ListItem>();
                lstMonth.AddRange(Enumerable.Range(0, 24).Select(x => new ListItem(start.AddMonths(-x).ToString("yyyy年MM月"), start.AddMonths(-x).ToString("yyyyMM"))));
                return lstMonth;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

    }
}