﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.DBA;
using System;
using System.Reflection;
using System.Web.UI;

namespace PssWebKintai
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                    Master.LoginEmployeeInfo = null;
                    Form.DefaultButton = btnLogin.UniqueID;
                    txtEmployeeNo.Focus();
                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                    Master.ShowErrorAlart();
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
                }
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                var employeeNo = txtEmployeeNo.Text;
                var password = txtPassword.Text;

                var dba = new DbaEmployee(string.Empty);
                var employee = dba.GetEmployee(employeeNo);

                if (employee == null || employee.PASSWORD != password)
                {
                    lblMessage.Text = "社員番号またはパスワードに誤りがあります。";
                }
                else if (!ckbKanrisya.Checked)
                {
                    Master.LoginEmployeeInfo = employee;
                    Response.Redirect("Kintai.aspx");
                }
                else if (employee.KINTAI_KENGEN == (int)Role.Clerk)
                {
                    Master.LoginEmployeeInfo = employee;
                    Response.Redirect("Menu.aspx");
                }
                else
                {
                    lblMessage.Text = "管理者権限がありません。";
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
            }
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("Password.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
            }
        }
    }
}