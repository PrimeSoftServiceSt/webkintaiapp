﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Menu.aspx.cs" Inherits="PssWebKintai.Menu" %>

<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:LinkButton runat="server" PostBackUrl="~/Employee.aspx">社員情報</asp:LinkButton><br />
    <asp:LinkButton runat="server" PostBackUrl="~/WorkType.aspx">勤務形態情報</asp:LinkButton><br />
    <asp:LinkButton runat="server" PostBackUrl="~/Tankin.aspx">単金情報</asp:LinkButton><br />
    <asp:LinkButton runat="server" PostBackUrl="~/Calendar.aspx">カレンダー情報</asp:LinkButton><br />
    <asp:LinkButton runat="server" PostBackUrl="~/KintaiStatusList.aspx">勤怠一覧</asp:LinkButton>
</asp:Content>
