﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Employee.aspx.cs" Inherits="PssWebKintai.Employee" %>

<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="Search">
        <table border="1">
            <tr class="searchHeader">
                <th>
                    <asp:Label runat="server" Text="社員番号" /></th>
                <th>
                    <asp:Label runat="server" Text="氏名" /></th>
                <th>
                    <asp:Label runat="server" Text="所属" /></th>
                <th>
                    <asp:Label runat="server" Text="勤務形態" /></th>
                <th>
                    <asp:Label runat="server" Text="上長" /></th>
                <th>
                    <asp:Label runat="server" Text="権限" /></th>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtNo" runat="server" TextMode="Number" /></td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" /></td>
                <td>
                    <asp:DropDownList ID="drpPost" runat="server" /></td>
                <td>
                    <asp:DropDownList ID="drpWorkType" runat="server" /></td>
                <td>
                    <asp:DropDownList ID="drpSuperiorNo" runat="server" /></td>
                <td>
                    <asp:DropDownList ID="drpRole" runat="server" /></td>
            </tr>
        </table>

        <asp:Button ID="btnSearch" runat="server" Text="検索" OnClick="btnSearch_Click" />
    </div>

    <%--   ヘッダー   --%>
    <div id="GridHeader">
        <table border="1">
            <tr class="tableHeader">
                <th runat="server">
                    <asp:Label Width="93px" runat="server" Style="text-align: center" Text="登録/削除" /></th>
                <th runat="server">
                    <asp:Label Width="75px" runat="server" Style="text-align: center" Text="社員番号" /></th>
                <th runat="server">
                    <asp:Label Width="200px" runat="server" Style="text-align: center" Text="氏名" /></th>
                <th runat="server">
                    <asp:Label Width="193px" runat="server" Style="text-align: center" Text="所属" /></th>
                <th runat="server">
                    <asp:Label Width="48px" runat="server" Style="text-align: center" Text="等級" /></th>
                <th runat="server">
                    <asp:Label Width="180px" runat="server" Style="text-align: center" Text="勤務形態" /></th>
                <th runat="server">
                    <asp:Label Width="154px" runat="server" Style="text-align: center" Text="上長" /></th>
                <th runat="server">
                    <asp:Label Width="82px" runat="server" Style="text-align: center" Text="権限" /></th>
            </tr>
        </table>
    </div>

    <%--   ボディ   --%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="GridBody" style="max-height: 350px; width: 1052px">
                <asp:ListView ID="ListView1" runat="server" InsertItemPosition="LastItem">
                    <LayoutTemplate>
                        <table id="itemPlaceholderContainer" runat="server" border="1">
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class='<%# Container.DisplayIndex % 2 == 0 ? "even" : "odd" %>' >
                            <td runat="server">
                                <asp:Button ID="UpdateBtn" runat="server" Text="登録" OnClientClick='return confirm("登録しますか？");' OnClick="UpdateBtn_Click" />
                                <asp:Button ID="DeleteBtn" runat="server" Text="削除" OnClientClick='return confirm("削除しますか？");' OnClick="DeleteBtn_Click" /></td>
                            <td runat="server">
                                <asp:TextBox ID="NO" runat="server" Width="75px" Text='<%# int.Parse(Eval("NO").ToString()).ToString("000000") %>' Enabled="false" /></td>
                            <td runat="server">
                                <asp:TextBox ID="NAME" runat="server" Width="200px" Text='<%# Eval("NAME") %>' /></td>
                            <td runat="server">
                                <asp:DropDownList ID="POST" runat="server" SelectedValue='<%# Eval("POST") %>' OnInit="POST_Init" /></td>
                            <td runat="server">
                                <asp:TextBox ID="LANK" runat="server" Width="50px" Text='<%# Eval("LANK") %>' /></td>
                            <td runat="server">
                                <asp:DropDownList ID="WORK_TYPE" runat="server" SelectedValue='<%# Eval("WORK_TYPE") %>' OnInit="WORK_TYPE_Init" /></td>
                            <td runat="server">
                                <asp:DropDownList ID="SUPERIOR_NO" runat="server" SelectedValue='<%# Eval("SUPERIOR_NO") %>' OnInit="SUPERIOR_NO_Init" /></td>
                            <td runat="server">
                                <asp:DropDownList ID="KINTAI_KENGEN" runat="server" SelectedValue='<%# Eval("KINTAI_KENGEN") %>' OnInit="ROLE_Init" /></td>
                        </tr>
                    </ItemTemplate>
                    <InsertItemTemplate>
                        <tr class="odd">
                            <td runat="server">
                                <asp:Button ID="InsertBtn" runat="server" Text="登録" OnClientClick='return confirm("登録しますか？");' OnClick="InsertBtn_Click" />
                                <asp:Button ID="DeleteBtn" runat="server" Text="削除" OnClick="DeleteBtn_Click" Enabled="false" /></td>
                            <td runat="server">
                                <asp:TextBox ID="NO" runat="server" Width="75px" TextMode="Number" /></td>
                            <td runat="server">
                                <asp:TextBox ID="NAME" runat="server" Width="200px" /></td>
                            <td runat="server">
                                <asp:DropDownList ID="POST" runat="server" OnInit="POST_Init" /></td>
                            <td runat="server">
                                <asp:TextBox ID="LANK" runat="server" Width="50px" /></td>
                            <td runat="server">
                                <asp:DropDownList ID="WORK_TYPE" runat="server" OnInit="WORK_TYPE_Init" /></td>
                            <td runat="server">
                                <asp:DropDownList ID="SUPERIOR_NO" runat="server" OnInit="SUPERIOR_NO_Init" /></td>
                            <td runat="server">
                                <asp:DropDownList ID="KINTAI_KENGEN" runat="server" OnInit="ROLE_Init" /></td>
                        </tr>
                    </InsertItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
        <Triggers></Triggers>
    </asp:UpdatePanel>
    <asp:Button ID="btnBack" runat="server" Text="戻る" OnClick="btnBack_Click" />
</asp:Content>

