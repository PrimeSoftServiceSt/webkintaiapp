﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JitakuYakan.aspx.cs" Inherits="PssWebKintai.JitakuYakan" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script>
        $(document).ready(function () {

            //読込イベント処理を書く

            var div1 = $('#GridHeader');
            var div2 = $('#GridBody');
            var div3 = $('#GridSum');

            div2.scroll(function () {

                div1.scrollLeft(div2.scrollLeft())
                div3.scrollLeft(div2.scrollLeft())

            });
            div3.scroll(function () {

                div1.scrollLeft(div3.scrollLeft())
                div2.scrollLeft(div3.scrollLeft())

            });

        });
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <asp:Button ID="btnNewRow" runat="server" Text="新規" OnClick="btnNewRow_Click"/>
            <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click"/>
    
            <%--   ヘッダー   --%>
            <div id="GridHeader" >
                <table border="1">
                    <tr class="tableHeader">
                        <th>
                            <asp:Label Width="45px" runat="server" Style="text-align: center" Text="削除" /></th>
                        <th>
                            <asp:Label Width="140px" runat="server" Style="text-align: center" Text="日付" /></th>
                        <th>
                            <asp:Label Width="80px" runat="server" Style="text-align: center" Text="対応開始" /><br />
                            <asp:Label Width="80px" runat="server" Style="text-align: center" Text="時間" /></th>
                        <th>
                            <asp:Label Width="80px" runat="server" Style="text-align: center" Text="対応終了" /><br />
                            <asp:Label Width="80px" runat="server" Style="text-align: center" Text="時間" /></th>
                        <th>
                            <asp:Label Width="80px" runat="server" Style="text-align: center" Text="対応時間" /></th>
                        <th>
                            <asp:Label Width="80px" runat="server" Style="text-align: center" Text="深夜時間" /></th>
                        <th>
                            <asp:Label Width="187px" runat="server" Style="text-align: center" Text="インシデント番号" /></th>
                    </tr>
                </table>
            </div>

            <%--   ボディ   --%>
            <div id="GridBody" style="max-height: 300px; width:718px">
                <asp:ListView ID="ListView1" runat="server">
                    <LayoutTemplate>
                        <table id="itemPlaceholderContainer" runat="server" border="1">
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class='<%# Container.DisplayIndex % 2 == 0 ? "even" : "odd" %>' >
                            <td>
                                <asp:Button ID="DeleteBtn" runat="server" Text="削除" Width="45px" OnClick="DeleteBtn_Click"/></td>
                            <td>
                                <asp:TextBox ID="DAY" runat="server" TextMode="Date" Width="140px"
                                    Text='<%# DateTime.TryParse(string.Format("{0}/{1}/{2}", Eval("YEAR").ToString(), Eval("MONTH").ToString(), Eval("DAY").ToString()), out var dt) ? dt.ToString("yyyy-MM-dd") : string.Empty %>' /></td>
                            <td>
                                <asp:TextBox ID="START" runat="server" Text='<%# Bind("START") %>' TextMode="Time" Width="80px"/></td>
                            <td>
                                <asp:TextBox ID="END" runat="server" Text='<%# Bind("END") %>' TextMode="Time" Width="80px"/></td>
                            <td>
                                <asp:TextBox ID="TAIO_JIKAN" runat="server" Text='<%# Bind("TAIO_JIKAN") %>' TextMode="Time" Width="80px" /></td>
                            <td>
                                <asp:TextBox ID="SINYA_JIKAN" runat="server" Text='<%# Bind("SINYA_JIKAN") %>' TextMode="Time" Width="80px"/></td>
                            <td>
                                <asp:TextBox ID="INCIDENT_NO" runat="server" Text='<%# Bind("INCIDENT_NO") %>' MaxLength="45" Width="187px"/></td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>

            <%--   合計   --%>
            <div id="GridSum" style="width: 701px">
                <table border="1">
                    <tr>
                        <td>
                            <asp:Label Width="347px" runat="server" Style="text-align: center" Text="合計" /></td>
                        <td>
                            <asp:Label Width="80px" runat="server" ID="lblTaioJikan" Style="text-align: right" /></td>
                        <td>
                            <asp:Label Width="80px" runat="server" ID="lblShinyaJikan" Style="text-align: right" /></td>
                        <td>
                            <asp:Label Width="188px" runat="server" Style="text-align: center" /></td>
                    </tr>
                </table>
            </div>


        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="btnBack" runat="server" Text="戻る" OnClick="btnBack_Click"/>

</asp:Content>
