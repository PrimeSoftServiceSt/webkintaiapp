﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Password.aspx.cs" Inherits="PssWebKintai.Password" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Label ID="Label1" runat="server" Text="社員番号"/><br />
    <asp:TextBox ID="txtNo" runat="server"/>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="社員番号" ForeColor="Red" ControlToValidate="txtNo" /><br />

    <asp:Label ID="Label2" runat="server" Text="古いパスワード" /><br />
    <asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="古いパスワード" ForeColor="Red" ControlToValidate="txtOldPassword" /><br />

    <asp:Label ID="Label3" runat="server" Text="新しいパスワード" /><br />
    <asp:TextBox ID="txtNewPassword1" runat="server" TextMode="Password" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="新しいパスワード" ForeColor="Red" ControlToValidate="txtNewPassword1" /><br />

    <asp:Label ID="Label4" runat="server" Text="新しいパスワード(確認)" /><br />
    <asp:TextBox ID="txtNewPassword2" runat="server" TextMode="Password" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="新しいパスワード(確認)" ForeColor="Red" ControlToValidate="txtNewPassword2" /><br />

    <asp:Button ID="btnUpdate" runat="server" Text="更新" OnClick="btnUpdate_Click"/>
    <asp:Button ID="btnBack" runat="server" Text="戻る" OnClick="btnBack_Click" CausesValidation="false"/>

</asp:Content>
