﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.DBA;
using PssWebKintai.Entity;
using PssWebKintai.Logic;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI.WebControls;

namespace PssWebKintai
{
    public partial class JitakuYakan : System.Web.UI.Page
    {
        /// <summary> ページロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                    var yakan = Master.YakanInfo;
                    yakan = GetYakan(yakan.EMPLOYEE_NO, yakan.YEAR, yakan.MONTH);
                    SetDataSourece(yakan);
                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                    Master.ShowErrorAlart();
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                }
            }
        }

        /// <summary> 新規ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNewRow_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var yakan = GetYakanByControl();
                yakan.DETAILS.Add(new T_YAKAN_DETAIL
                {
                    EMPLOYEE_NO = yakan.EMPLOYEE_NO,
                    YEAR = yakan.YEAR,
                    MONTH = yakan.MONTH,
                    DAY = 1
                });
                SetDataSourece(yakan);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 保存ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                // データを画面から取得
                var yakan = GetYakanByControl();

                // 計算
                var calc = new KintaiCalcJitaku(yakan, Master.DispEmployeeInfo.WORK_TYPE);
                var res = calc.Calc_Jitaku();

                if (!res.Result)
                {
                    Master.ShowAlart(res.Message, UpdatePanel1);
                    return;
                }

                yakan = res.Value;

                // 保存
                var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);
                dba.RegistYakanInfo(yakan);
                Master.YakanInfo = yakan;

                Master.ShowAlart("保存しました。", UpdatePanel1);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 削除ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var yakan = GetYakanByControl(((Button)sender).ClientID);
                SetDataSourece(yakan);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> DBより夜間対応情報を取得する
        /// </summary>
        /// <param name="employeeNo"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        private T_YAKAN GetYakan(string employeeNo, int year, int month)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var startDate = new DateTime(year, month, 1);
                var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);

                // 夜間対応情報取得
                var yakan = dba.GetYakan(employeeNo, startDate.Year, startDate.Month);
                if (yakan == null)
                {
                    // 夜間対応情報を取得できなかった場合
                    // 夜間対応情報作成
                    yakan = new T_YAKAN
                    {
                        EMPLOYEE_NO = employeeNo,
                        YEAR = startDate.Year,
                        MONTH = startDate.Month,
                        DETAILS = new List<T_YAKAN_DETAIL>()
                    };
                }
                return yakan;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 各コントロールより自宅夜間対応情報を取得する
        /// </summary>
        /// <param name="deleteBtnID">削除ボタンID</param>
        /// <returns></returns>
        private T_YAKAN GetYakanByControl(string deleteBtnID = "")
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var yakan = Master.YakanInfo;
                yakan.SUM_TAIO_JIKAN = decimal.TryParse(lblTaioJikan.Text, out decimal dec) ? dec : 0;
                yakan.SUM_SINYA_JIKAN = decimal.TryParse(lblShinyaJikan.Text, out dec) ? dec : 0;
                if (yakan.DETAILS == null || yakan.DETAILS.Count > 0)
                {
                    yakan.DETAILS = new List<T_YAKAN_DETAIL>();
                }
                foreach (var row in ListView1.Items)
                {
                    if (deleteBtnID != string.Empty)
                    {
                        var btn = row.GetControl<Button>("DeleteBtn");
                        if (btn.ClientID == deleteBtnID)
                            continue;
                    }
                    var txtDay = row.GetControl<TextBox>("DAY");
                    if (txtDay.Text == string.Empty || DateTime.TryParse(txtDay.Text, out DateTime dt) == false)
                        continue;

                    var txtStart = row.GetControl<TextBox>("START");
                    var txtEnd = row.GetControl<TextBox>("END");

                    yakan.DETAILS.Add(new T_YAKAN_DETAIL
                    {
                        EMPLOYEE_NO = yakan.EMPLOYEE_NO,
                        YEAR = yakan.YEAR,
                        MONTH = yakan.MONTH,
                        DAY = dt.Day,
                        START = txtStart.Text,
                        END = txtEnd.Text,
                        INCIDENT_NO = row.GetControl<TextBox>("INCIDENT_NO").Text
                    });
                }
                return yakan;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> データを各項目に設定する
        /// </summary>
        /// <param name="yakan"></param>
        private void SetDataSourece(T_YAKAN yakan)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                ListView1.DataSource = yakan.DETAILS;
                ListView1.DataBind();

                lblTaioJikan.Text = yakan.SUM_TAIO_JIKAN.ToString();
                lblShinyaJikan.Text = yakan.SUM_SINYA_JIKAN.ToString();

                Master.YakanInfo = yakan;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                Response.Redirect("Kintai.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }
    }
}