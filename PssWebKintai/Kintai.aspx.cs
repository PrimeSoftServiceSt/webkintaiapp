﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.DBA;
using PssWebKintai.Entity;
using PssWebKintai.Logic;
using PssWebKintai.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace PssWebKintai
{
    public partial class Kintai : System.Web.UI.Page
    {

        #region メンバ変数

        /// <summary> 出欠区分データソース用リスト
        /// </summary>
        private List<M_KBN> lstSyukketsuKbn;

        /// <summary> 夜間待機データソース用リスト
        /// </summary>
        private List<M_KBN> lstYakanTaiki;

        /// <summary> 主副TRデータソース用リスト
        /// </summary>
        private List<M_KBN> lstSFTR;

        public static string[] AryCsvHead { get; } = { "社員番号", "社員指名", "平日出勤", "休日出勤", "出勤時間", "遅刻", "早退", "有給日数", "代休", "公休", "その他休日", "生理休暇", "休職日数",
                                                       "欠勤日数", "日額表用", "控除日数", "控除時間", "法定内", "平日", "平日深夜", "休日", "休日深夜", "法定深夜", "法定休日", "所定時間外",
                                                       "休日勤務割増", "法定休日割増", "深夜勤務割増", "遅刻/早退", "欠勤/代休", "法休深夜割増", "回数手当", "出張旅費", "時間外手当", "給与控除額" };

        #endregion

        #region 初期化

        /// <summary> ページロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);

                    M_EMPLOYEE loginEmployeeInfo = Master.LoginEmployeeInfo;
                    M_EMPLOYEE dispEmployeeInfo = null;
                    var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);
                    var start = DateTime.Today.GetFirstDateOfMonth();
                    var lstMachi = new List<T_KINTAI>();

                    var dispYear = start.Year;
                    var dispMonth = start.Month;

                    // 年月ドロップダウンのデータ作成
                    drpMonth.SetItem(CreateDropMonthDataSource(start));

                    if (loginEmployeeInfo.KINTAI_KENGEN == (int)Role.Normal)
                    {
                        dispEmployeeInfo = loginEmployeeInfo;
                        Master.DispEmployeeInfo = dispEmployeeInfo;
                    }
                    else
                    {
                        // CSV年月ドロップダウンのデータ作成,データバインド
                        drpCsvMonth.SetItem(CreateDropMonthDataSource(start));

                        // 承認待ち,確定待ちのリストを取得する
                        if (loginEmployeeInfo.KINTAI_KENGEN == (int)Role.Superior)
                        {
                            // 上長
                            lstMachi.AddRange(dba.GetSyoninIraiKintaiList(loginEmployeeInfo.NO));
                        }
                        else
                        {
                            // 事務
                            lstMachi.AddRange(dba.GetSyoninZumiKintaiList());
                        }

                        if (lstMachi.Count != 0)
                        {
                            var dbaEmployee = new DbaEmployee(string.Empty);
                            dispEmployeeInfo = dbaEmployee.GetEmployee(lstMachi[0].EMPLOYEE_NO);
                            Master.DispEmployeeInfo = dispEmployeeInfo;
                            dispYear = lstMachi[0].YEAR;
                            dispMonth = lstMachi[0].MONTH;
                        }
                    }

                    if (dispEmployeeInfo != null)
                    {
                        // 値の設定
                        SetDisplay(dispEmployeeInfo, dispYear, dispMonth);

                        if (loginEmployeeInfo.KINTAI_KENGEN == (int)Role.Normal)
                        {
                            // 一般の場合
                            btnPrint.Visible = false;
                            drpCsvMonth.Visible = false;
                            btnCsv.Visible = false;
                            drpMachi.Visible = false;
                            // 合計再計算(手動修正完了)ボタンはアテンドのみ使用可
                            btnCalcSum.Enabled = loginEmployeeInfo.WORK_TYPE == (int)WorkStyle.Attend;
                        }
                        else
                        {
                            // 管理者の場合
                            drpMonth.Enabled = false;
                            btnExcel.Visible = false;
                            var a = lstMachi.Select(x => new ListItem(string.Format("{0}/{1} {2}", x.YEAR, x.MONTH, x.NAME), string.Format("{0}-{1}-{2}", x.EMPLOYEE_NO, x.YEAR, x.MONTH)));
                            drpMachi.SetItem(a);
                            // 合計再計算(手動修正完了)ボタンは経理担当のみ使用可
                            btnCalcSum.Enabled = loginEmployeeInfo.KINTAI_KENGEN == (int)Role.Clerk;
                        }
                        drpMonth.Focus();
                    }
                    else
                    {
                        btnUpdateEmployeeInfo.Enabled = false;
                        btnYakan.Enabled = false;
                        btnAutoInput.Enabled = false;
                        btnCalcKintai.Enabled = false;
                        btnCalcSum.Enabled = false;
                        btnSave.Enabled = false;
                        btnIrai.Enabled = false;
                        btnSyonin.Enabled = false;
                        btnKakutei.Enabled = false;
                        btnSashimodoshi.Enabled = false;
                        btnPrint.Enabled = false;
                        drpMachi.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                }
            }
        }

        /// <summary> 年月ドロップダウンデータソース作成
        /// </summary>
        /// <param name="start"></param>
        /// <returns></returns>
        private List<ListItem> CreateDropMonthDataSource(DateTime start)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var lstMonth = new List<ListItem>();
                lstMonth.AddRange(Enumerable.Range(0, 24).Select(x => new ListItem(start.AddMonths(-x).ToString("yyyy年MM月"), start.AddMonths(-x).ToString("yyyyMM"))));
                return lstMonth;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        #endregion

        #region ボタンイベント

        protected void btnUpdateEmployeeInfo_Click(object sender, EventArgs e)
        {
            try
            {
                var kintai = Master.KintaiInfo;

                kintai.NAME = Master.DispEmployeeInfo.NAME;
                kintai.POST = Master.DispEmployeeInfo.POST;
                kintai.LANK = Master.DispEmployeeInfo.LANK;
                kintai.WORK_TYPE = Master.DispEmployeeInfo.WORK_TYPE;
                kintai.PRICE = Master.DispEmployeeInfo.PRICE;

                var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);
                dba.UpdateWorkType(kintai);

                var employee = Master.DispEmployeeInfo;
                var year = int.Parse(drpMonth.SelectedValue.Substring(0, 4));
                var month = int.Parse(drpMonth.SelectedValue.Substring(4, 2));
                // 値設定
                SetDisplay(employee, year, month);

                Master.ShowAlart("社員情報を更新しました。");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary>
        /// 自動入力ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAutoInput_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var kintai = GetKintaiByControl();
                var kintaiCalc = new KintaiAutoInput(kintai);
                kintai = kintaiCalc.AutoInputStartEnd();
                SetDataSource(kintai, Master.YakanInfo);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 自宅夜間対応ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnYakan_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var yakan = Master.YakanInfo;
                if (yakan.YEAR == 0)
                {
                    yakan.EMPLOYEE_NO = Master.DispEmployeeInfo.NO;
                    yakan.YEAR = int.Parse(drpMonth.SelectedValue.Substring(0, 4));
                    yakan.MONTH = int.Parse(drpMonth.SelectedValue.Substring(4, 2));
                }
                Master.YakanInfo = yakan;
                if (Master.LoginEmployeeInfo.NO == Master.DispEmployeeInfo.NO)
                {
                    var kintai = GetKintaiByControl();
                    SaveKintai(kintai);
                }

                Response.Redirect("JitakuYakan.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 勤務時間計算ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCalcKintai_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var kintai = GetKintaiByControl();
                var check = CheckKintai(kintai);
                if (!check.Result)
                {
                    Master.ShowAlart(check.Message);
                    return;
                }

                var kintaiCalc = new KintaiCalcMain(kintai, Master.YakanInfo);
                kintai = kintaiCalc.Calc();
                SetDataSource(kintai, Master.YakanInfo);
                SaveKintai(kintai);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 合計計算ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCalcSum_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var kintai = GetKintaiByControl();
                var check = CheckKintai(kintai);
                if (!check.Result)
                {
                    Master.ShowAlart(check.Message);
                    return;
                }

                var calcKintai = new KintaiCalcMain(kintai, Master.YakanInfo);
                kintai = calcKintai.CalcSum();
                SetDataSource(kintai, Master.YakanInfo);
                SaveKintai(kintai);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 勤怠の保存ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var kintai = GetKintaiByControl();
                var check = CheckKintai(kintai);
                if (!check.Result)
                {
                    Master.ShowAlart(check.Message);
                    return;
                }
                SaveKintai(kintai);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 依頼ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnIrai_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var kintai = GetKintaiByControl();
                var dbaEmployee = new DbaEmployee(Master.LoginEmployeeInfo.NO);
                var superior = dbaEmployee.GetEmployee(Master.DispEmployeeInfo.SUPERIOR_NO);

                var superiorNo = superior.KINTAI_KENGEN == (int)Role.Clerk ? "999999" : superior.NO;
                var status = superior.KINTAI_KENGEN == (int)Role.Clerk ? KintaiStatus.SyoninZumi : KintaiStatus.SyoninIrai;
                var dbaKintai = new DbaKintai(Master.LoginEmployeeInfo.NO);
                dbaKintai.UpdateStatus(kintai, superiorNo, status);

                btnAutoInput.Enabled = false;
                btnCalcKintai.Enabled = false;
                btnCalcSum.Enabled = false;
                btnSave.Enabled = false;
                btnIrai.Enabled = false;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 承認ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSyonin_Click(object sender, EventArgs e)
        {
            try
            {
                var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);
                var kintai = GetKintaiByControl();
                dba.UpdateStatus(kintai, "999999", KintaiStatus.SyoninZumi);

                drpMachi.Items.Remove(drpMachi.SelectedItem);
                var selectedValue = drpMachi.SelectedValue;
                // 値設定
                if (string.IsNullOrEmpty(selectedValue))
                {
                    SetDataSource(null, null);
                }
                else
                {
                    var key = selectedValue.Split('-').Select(x => int.Parse(x)).ToArray();
                    SetDisplay(key[0], key[1], key[2]);
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 確定ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnKakutei_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);
                var kintai = GetKintaiByControl();
                dba.UpdateStatus(kintai, kintai.EMPLOYEE_NO, KintaiStatus.Kakutei);

                drpMachi.Items.Remove(drpMachi.SelectedItem);
                var selectedValue = drpMachi.SelectedValue;
                // 値設定
                if (string.IsNullOrEmpty(selectedValue))
                {
                    SetDataSource(null, null);
                }
                else
                {
                    var key = selectedValue.Split('-').Select(x => int.Parse(x)).ToArray();
                    SetDisplay(key[0], key[1], key[2]);
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 差戻ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSashimodoshi_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);
                var kintai = GetKintaiByControl();
                dba.UpdateStatus(kintai, kintai.EMPLOYEE_NO, KintaiStatus.Sashimodoshi);

                drpMachi.Items.Remove(drpMachi.SelectedItem);
                var selectedValue = drpMachi.SelectedValue;
                // 値設定
                if (string.IsNullOrEmpty(selectedValue))
                {
                    SetDataSource(null, null);
                }
                else
                {
                    var key = selectedValue.Split('-').Select(x => int.Parse(x)).ToArray();
                    SetDisplay(key[0], key[1], key[2]);
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> EXCELボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExcel_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                DownloadKintaihyo(false);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 出力ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                DownloadKintaihyo(Master.LoginEmployeeInfo.KINTAI_KENGEN == (int)Role.Clerk);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> CSV出力ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCsv_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var selectedY = int.Parse(drpCsvMonth.SelectedValue.Substring(0, 4));
                var selectedM = int.Parse(drpCsvMonth.SelectedValue.Substring(4, 2));

                var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);
                var lstKintai = dba.GetKakuteiKintaiList(selectedY, selectedM);
                var dbaPss = new DbaPss();
                var lstSyukketsuKbn = dbaPss.GetSyukketsuKbnList();

                var lstCsv = new List<CsvInfo>();
                var lstYukyuUsedUsers = new List<List<string>>();

                foreach (var kintai in lstKintai)
                {
                    // CSVデータ作成
                    lstCsv.Add(this.CreateCsvData(kintai));
                    var lstYukyu = kintai.DETAILS.Where(dr => dr.SYUKKETSU_KBN == SyukketsuKbn.YUKYU || dr.SYUKKETSU_KBN == SyukketsuKbn.HANKYU);
                    if (lstYukyu.Any())
                    {
                        // 有給使用者の場合、使用日付,(有休or半休)を取得
                        List<string> lstYukyuUsedUser = new List<string>();
                        lstYukyuUsedUser.Add(kintai.EMPLOYEE_NO.ToString());
                        lstYukyuUsedUser.Add(kintai.NAME);
                        foreach (var drYukyu in lstYukyu)
                        {
                            lstYukyuUsedUser.Add(selectedM.ToString("00") + "/" + drYukyu.DAY.ToString("00"));
                            lstYukyuUsedUser.Add(lstSyukketsuKbn.Where(x => x.KEY1 == drYukyu.SYUKKETSU_KBN).First().VALUE1);
                        }
                        lstYukyuUsedUsers.Add(lstYukyuUsedUser);
                    }
                }
                // ファイル名取得
                var ym = string.Format("{0}-{1}", selectedY, selectedM);
                var fileName = string.Format("出勤簿_{0}{1}", ym, Extention.CSV);
                // csvファイル作成
                DownloadCsv(fileName, lstCsv.Select(x => x.ValueList).ToList());
                if (lstYukyuUsedUsers.Any())
                {
                    // 有給使用者一覧を出力する
                    fileName = string.Format("有給使用者一覧_{0}{1}", ym, Extention.CSV);
                    DownloadCsv(fileName, lstYukyuUsedUsers);
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        #endregion

        #region その他のイベント

        /// <summary> 年月変更時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void drpMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var drp = (DropDownList)sender;
                var employee = Master.LoginEmployeeInfo;
                var year = int.Parse(drp.SelectedValue.Substring(0, 4));
                var month = int.Parse(drp.SelectedValue.Substring(4, 2));
                // 値設定
                SetDisplay(employee, year, month);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 承認依頼リスト変更時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void drpMachi_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var drp = (DropDownList)sender;
                var key = drp.SelectedValue.Split('-').Select(x => int.Parse(x)).ToArray();
                // 値設定
                SetDisplay(key[0], key[1], key[2]);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        #endregion

        #region リストビュー内イベント

        /// <summary> 出欠区分ドロップダウンの初期化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SYUKKETSU_KBN_Init(object sender, EventArgs e)
        {
            try
            {
                if (lstSyukketsuKbn == null)
                {
                    var dbaPss = new DbaPss();
                    lstSyukketsuKbn = dbaPss.GetSyukketsuKbnList(Master.DispEmployeeInfo.NO);
                }

                var source = lstSyukketsuKbn.Select(x => new ListItem() { Value = x.KEY1, Text = string.IsNullOrEmpty(x.VALUE1) ? string.Empty : x.VALUE1 });
                var drp = (DropDownList)sender;
                drp.SetItem(source);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
        }

        /// <summary> 夜間待機ドロップダウンの初期化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void YAKAN_TAIKI_Init(object sender, EventArgs e)
        {
            try
            {
                if (lstYakanTaiki == null)
                {
                    var dba = new DbaPss();
                    lstYakanTaiki = dba.GetYakanTaikiList();
                }

                var source = lstYakanTaiki.Select(x => new ListItem() { Value = x.KEY1, Text = string.IsNullOrEmpty(x.VALUE1) ? string.Empty : x.VALUE1 });
                var drp = (DropDownList)sender;
                drp.SetItem(source);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
        }

        protected void TETUYA_AKEKIN_Init(object sender, EventArgs e)
        {
            try
            {
                if (lstSFTR == null)
                {
                    var dba = new DbaPss();
                    lstSFTR = dba.GetSFTRList();
                }

                var source = lstSFTR.Select(x => new ListItem() { Value = x.KEY1, Text = string.IsNullOrEmpty(x.VALUE1) ? string.Empty : x.VALUE1 });
                var drp = (DropDownList)sender;
                drp.SetItem(source);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
        }

        /// <summary> 時間テキストボックスバインド時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TimeTextBox_DataBinding(object sender, EventArgs e)
        {
            try
            {
                var txt = (TextBox)sender;
                if (TimeSpan.TryParse(txt.Text, out var ts) == false || ts == TimeSpan.Zero)
                {
                    txt.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
        }

        /// <summary> 休日変更時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void KYUJITSU_TextChanged(object sender, EventArgs e)
        {
            try
            {
                var txtKyujitsu = (TextBox)sender;
                var lblKinmuKbn = (Label)txtKyujitsu.Parent.FindControl("KINMU_KBN");

                lblKinmuKbn.Text = txtKyujitsu.Text == string.Empty ? "通常" : string.Empty;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
        }

        #endregion

        #region メソッド

        /// <summary> DBより勤怠情報を取得する
        /// </summary>
        /// <param name="employeeNo"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        private T_KINTAI GetKintai(M_EMPLOYEE employee, int year, int month)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var startDate = new DateTime(year, month, 1);
                var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);

                // 勤怠情報取得
                var kintai = dba.GetKintai(employee.NO, startDate.Year, startDate.Month);
                if (kintai == null)
                {
                    // 勤怠情報を取得できなかった場合
                    // 勤怠情報作成
                    kintai = new T_KINTAI();
                    kintai.EMPLOYEE_NO = employee.NO;
                    kintai.YEAR = startDate.Year;
                    kintai.MONTH = startDate.Month;
                    kintai.NAME = employee.NAME;
                    kintai.POST = employee.POST;
                    kintai.LANK = employee.LANK;
                    kintai.WORK_TYPE = employee.WORK_TYPE;
                    kintai.PRICE = employee.PRICE;
                    kintai.TANTO_NO = employee.NO;
                    kintai.SCHEDULED_WORKING_HOURS = 0;
                    kintai.RYOHI = 0;
                    kintai.SHIKAKU_HOJO = 0;
                    kintai.SHIKAKU_HOSYO = 0;

                    // 休日取得
                    var dbaPss = new DbaPss();
                    var lstHoliday = dbaPss.GetCalendar(startDate.Year, startDate.Month).Select(x => x.HOLIDAY);

                    // 勤怠情報明細作成
                    foreach (int i in Enumerable.Range(1, startDate.GetLastDateOfMonth().Day))
                    {
                        var day = new DateTime(year, month, i);
                        var detail = new T_KINTAI_DETAIL();
                        detail.EMPLOYEE_NO = employee.NO;
                        detail.YEAR = day.Year;
                        detail.MONTH = day.Month;
                        detail.DAY = day.Day;
                        switch (day.DayOfWeek)
                        {
                            case DayOfWeek.Sunday:
                                detail.KYUJITSU = "日休";
                                break;
                            case DayOfWeek.Saturday:
                                detail.KYUJITSU = "土休";
                                break;
                            default:
                                if (lstHoliday.Contains(day))
                                    detail.KYUJITSU = "休日";
                                else
                                    detail.KYUJITSU = string.Empty;
                                break;
                        }
                        //detail.KINMU_KBN = detail.KYUJITSU == string.Empty ? "通常" : string.Empty;
                        detail.SYUKKETSU_KBN = SyukketsuKbn.EMPTY;
                        detail.YAKAN_TAIKI = "0";
                        detail.TETUYA_AKEKIN = "0";
                        detail.DENKO = false;
                        detail.BIKO = "";
                        kintai.DETAILS.Add(detail);
                    }
                }
                return kintai;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 社員情報,年月よりデータを取得,各項目にデータを設定する
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        private void SetDisplay(M_EMPLOYEE employee, int year, int month)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                // 勤怠情報取得,設定
                var kintai = GetKintai(employee, year, month);
                var yakan = new T_YAKAN();
                if (employee.WORK_TYPE == (int)WorkStyle.NR)
                {
                    var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);
                    yakan = dba.GetYakan(employee.NO, year, month);
                }
                SetDataSource(kintai, yakan);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 社員番号,年月よりデータを取得,各項目にデータを設定する
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        private void SetDisplay(int employeeNo, int year, int month)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                // 勤怠情報取得,設定
                var dba = new DbaEmployee(Master.LoginEmployeeInfo.NO);
                var employee = dba.GetEmployee(employeeNo.ToString("000000"));
                SetDisplay(employee, year, month);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> リストビュー,各項目にデータを設定する
        /// </summary>
        /// <param name="kintai"></param>
        private void SetDataSource(T_KINTAI kintai, T_YAKAN yakan)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);

                if (kintai == null)
                {
                    txtNo.Text = string.Empty;
                    txtName.Text = string.Empty;
                    txtPost.Text = string.Empty;
                    txtWorkType.Text = string.Empty;

                    lblSyoteiWorkTime.Visible = false;
                    txtSyoteiWorkTime.Visible = false;

                    txtRyohi.Text = string.Empty;
                    txtShikakuHojo.Text = string.Empty;
                    txtShikakuHosyo.Text = string.Empty;

                    lblHeadJimusyonaiJitsudo1.Text = "通常実働";
                    lblHeadJimusyonaiJitsudo2.Text = "時間";
                    lblHeadYakanTaio1.Text = "夜間対応";
                    lblHeadYakanTaio2.Text = "時間";
                    lblHeadJunkekkin.Text = "準欠勤";

                    ListView1.DataSource = null;
                    ListView1.DataBind();

                    ListView2.DataSource = null;
                    ListView2.DataBind();

                    lblSumJunkekkinKai.Text = "0.0";
                    lblSumJunkekkinJikan.Text = "0.00";
                    lblSumJimusyonaiJitsudo.Text = "0.0";
                    lblSumYakanTaio.Text = "0.0";
                    lblSumGoukeiJitsudo.Text = "0.0";
                    lblSumZanHutsu.Text = "0.0";
                    lblSumZanSinya.Text = "0.0";
                    lblSumKyujitsuKinmu.Text = "0.0";
                    lblSumYakanTaiki.Text = "0.0";
                    //lblSumGaisyutsuKai.Text = kintai.SUM_GAISYUTSU_KAI.ToString();
                    //lblSumGaisyutsuJikan.Text = kintai.SUM_GAISYUTSU_JIKAN.ToString();
                    lblSumDenko.Text = "0";
                    lblBiko.Text = string.Empty;

                    btnYakan.Visible = false;

                    btnAutoInput.Enabled = false;
                    btnCalcKintai.Enabled = false;
                    btnCalcSum.Enabled = false;
                    btnSave.Enabled = false;
                    btnIrai.Enabled = false;
                    btnSyonin.Enabled = false;
                    btnKakutei.Enabled = false;
                    btnSashimodoshi.Enabled = false;

                    Master.KintaiInfo = null;
                    Master.YakanInfo = null;
                    return;
                }

                var dba = new DbaPss();

                // 勤怠情報をリストビュー,各項目に設定
                txtNo.Text = kintai.EMPLOYEE_NO;
                txtName.Text = kintai.NAME;
                txtPost.Text = dba.GetPostName(kintai.POST);
                txtWorkType.Text = dba.GetWorkTypeName(kintai.WORK_TYPE);

                if (kintai.WORK_TYPE == (int)WorkStyle.Attend)
                {
                    lblSyoteiWorkTime.Visible = true;
                    txtSyoteiWorkTime.Visible = true;
                    txtSyoteiWorkTime.Text = kintai.SCHEDULED_WORKING_HOURS.ToString("0.0");

                    lblHeadJimusyonaiJitsudo1.Text = "休憩前";
                    lblHeadJimusyonaiJitsudo2.Text = "労働時間";
                    lblHeadYakanTaio1.Text = "休憩";
                    lblHeadYakanTaio2.Text = "時間";
                    lblHeadJunkekkin.Text = "有休/特休";
                    lblSumZanHutsu.BackColor = Color.SkyBlue;
                    lblBiko.BackColor = Color.SkyBlue;
                }
                else
                {
                    lblSyoteiWorkTime.Visible = false;
                    txtSyoteiWorkTime.Visible = false;

                    lblHeadJimusyonaiJitsudo1.Text = "通常実働";
                    lblHeadJimusyonaiJitsudo2.Text = "時間";
                    lblHeadYakanTaio1.Text = "夜間対応";
                    lblHeadYakanTaio2.Text = "時間";
                    lblHeadJunkekkin.Text = "準欠勤";
                }

                ListView1.DataSource = kintai.DETAILS;
                ListView1.DataBind();

                ListView2.DataSource = kintai.DETAILS;
                ListView2.DataBind();

                drpMonth.SelectedValue = kintai.YEAR.ToString("0000") + kintai.MONTH.ToString("00");
                txtRyohi.Text = kintai.RYOHI.ToString();
                txtShikakuHojo.Text = kintai.SHIKAKU_HOJO.ToString();
                txtShikakuHosyo.Text = kintai.SHIKAKU_HOSYO.ToString();

                lblSumJunkekkinKai.Text = kintai.SUM_JUNKEKKIN_KAI.ToString();
                lblSumJunkekkinJikan.Text = kintai.SUM_JUNKEKKIN_JIKAN.ToString();
                lblSumJimusyonaiJitsudo.Text = kintai.SUM_JIMUSYONAI_JITSUDO.ToString();
                lblSumYakanTaio.Text = kintai.SUM_YAKAN_TAIO.ToString();
                lblSumGoukeiJitsudo.Text = kintai.SUM_GOKEI_JITSUDO.ToString();
                lblSumZanHutsu.Text = kintai.SUM_ZAN_HUTU.ToString();
                lblSumZanSinya.Text = kintai.SUM_ZAN_SINYA.ToString();
                lblSumKyujitsuKinmu.Text = kintai.SUM_KYUJITSU_KINMU.ToString();
                lblSumYakanTaiki.Text = kintai.SUM_YAKAN_TAIKI.ToString();
                //lblSumGaisyutsuKai.Text = kintai.SUM_GAISYUTSU_KAI.ToString();
                //lblSumGaisyutsuJikan.Text = kintai.SUM_GAISYUTSU_JIKAN.ToString();
                lblSumDenko.Text = kintai.SUM_DENKO.ToString();
                lblBiko.Text = kintai.BIKO;

                btnYakan.Visible = kintai.WORK_TYPE == (int)WorkStyle.NR;

                var isEnabledUser = kintai.TANTO_NO == Master.LoginEmployeeInfo.NO;

                switch ((KintaiStatus)kintai.STATUS)
                {
                    case KintaiStatus.None:
                    case KintaiStatus.Sashimodoshi:
                        btnUpdateEmployeeInfo.Enabled = isEnabledUser;
                        btnAutoInput.Enabled = isEnabledUser && kintai.WORK_TYPE != (int)WorkStyle.Attend;
                        btnCalcKintai.Enabled = isEnabledUser;
                        btnCalcSum.Enabled = isEnabledUser && kintai.WORK_TYPE == (int)WorkStyle.Attend;
                        btnSave.Enabled = isEnabledUser;
                        btnIrai.Enabled = isEnabledUser;
                        btnSyonin.Enabled = false;
                        btnKakutei.Enabled = false;
                        btnSashimodoshi.Enabled = false;
                        break;
                    case KintaiStatus.SyoninIrai:
                        btnUpdateEmployeeInfo.Enabled = false;
                        btnAutoInput.Enabled = false;
                        btnCalcKintai.Enabled = isEnabledUser && Master.LoginEmployeeInfo.IsSuperior;
                        btnCalcSum.Enabled = isEnabledUser && Master.LoginEmployeeInfo.IsSuperior;
                        btnSave.Enabled = isEnabledUser && Master.LoginEmployeeInfo.IsSuperior;
                        btnIrai.Enabled = false;
                        btnSyonin.Enabled = isEnabledUser && Master.LoginEmployeeInfo.IsSuperior;
                        btnKakutei.Enabled = false;
                        btnSashimodoshi.Enabled = isEnabledUser && Master.LoginEmployeeInfo.IsSuperior;
                        break;
                    case KintaiStatus.SyoninZumi:
                        btnUpdateEmployeeInfo.Enabled = Master.LoginEmployeeInfo.IsClerk;
                        btnAutoInput.Enabled = false;
                        btnCalcKintai.Enabled = Master.LoginEmployeeInfo.IsClerk;
                        btnCalcSum.Enabled = Master.LoginEmployeeInfo.IsClerk;
                        btnSave.Enabled = Master.LoginEmployeeInfo.IsClerk;
                        btnIrai.Enabled = false;
                        btnSyonin.Enabled = false;
                        btnKakutei.Enabled = Master.LoginEmployeeInfo.IsClerk;
                        btnSashimodoshi.Enabled = Master.LoginEmployeeInfo.IsClerk;
                        break;
                    case KintaiStatus.Kakutei:
                        btnUpdateEmployeeInfo.Enabled = false;
                        btnAutoInput.Enabled = false;
                        btnCalcKintai.Enabled = false;
                        btnCalcSum.Enabled = false;
                        btnSave.Enabled = false;
                        btnIrai.Enabled = false;
                        btnSyonin.Enabled = false;
                        btnKakutei.Enabled = false;
                        btnSashimodoshi.Enabled = false;
                        break;
                }

                // セッションに保存
                Master.KintaiInfo = kintai;
                if (kintai.WORK_TYPE != (int)WorkStyle.NR)
                {
                    Master.YakanInfo = null;
                }
                else if (yakan == null || yakan.YEAR == 0)
                {
                    Master.YakanInfo = new T_YAKAN() { EMPLOYEE_NO = kintai.EMPLOYEE_NO, YEAR = kintai.YEAR, MONTH = kintai.MONTH };
                }
                else
                {
                    Master.YakanInfo = yakan;
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 各コントロールより勤怠情報を取得する
        /// </summary>
        /// <returns></returns>
        private T_KINTAI GetKintaiByControl()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var kintai = Master.KintaiInfo;
                decimal decValue;
                int intValue;

                // リストビューから値を取得
                kintai.DETAILS.Clear();
                foreach (var row in ListView1.Items)
                {
                    //int intValue = 0;
                    TimeSpan tsValue = TimeSpan.Zero;

                    var detail = new T_KINTAI_DETAIL();

                    detail.EMPLOYEE_NO = kintai.EMPLOYEE_NO;
                    detail.YEAR = kintai.YEAR;
                    detail.MONTH = kintai.MONTH;
                    detail.DAY = int.Parse(row.GetControl<HiddenField>("DAY").Value);
                    detail.KYUJITSU = row.GetControl<TextBox>("KYUJITSU").Text;
                    //detail.KINMU_KBN = row.GetControl<Label>("KINMU_KBN").Text;
                    detail.SYUKKETSU_KBN = row.GetControl<DropDownList>("SYUKKETSU_KBN").SelectedValue;
                    detail.START = row.GetControl<TextBox>("START").Text;
                    detail.END = row.GetControl<TextBox>("END").Text;
                    detail.JUNKEKKIN_KAI = decimal.TryParse(row.GetControl<TextBox>("JUNKEKKIN_KAI").Text, out decValue) ? decValue : 0;
                    detail.JUNKEKKIN_JIKAN = row.GetControl<TextBox>("JUNKEKKIN_JIKAN").Text;
                    detail.JIMUSYONAI_JITSUDO = row.GetControl<TextBox>("JIMUSYONAI_JITSUDO").Text;
                    detail.YAKAN_TAIO = row.GetControl<TextBox>("YAKAN_TAIO").Text;
                    detail.GOKEI_JITSUDO = row.GetControl<TextBox>("GOKEI_JITSUDO").Text;
                    detail.ZAN_HUTSU = row.GetControl<TextBox>("ZAN_HUTSU").Text;
                    detail.ZAN_SINYA = row.GetControl<TextBox>("ZAN_SINYA").Text;
                    detail.KYUJITSU_KINMU = row.GetControl<TextBox>("KYUJITSU_KINMU").Text;
                    detail.YAKAN_TAIKI = row.GetControl<DropDownList>("YAKAN_TAIKI").SelectedValue;
                    detail.TETUYA_AKEKIN = row.GetControl<DropDownList>("TETUYA_AKEKIN").SelectedValue;
                    detail.SINYA_JIKAN = row.GetControl<HiddenField>("SINYA_JIKAN").Value;
                    //detail.GAISYUTSU_KAI = int.TryParse(row.GetControl<TextBox>("GAISYUTSU_KAI").Text, out intValue) ? intValue : 0;
                    //detail.GAISYUTSU_JIKAN = row.GetControl<TextBox>("GAISYUTSU_JIKAN").Text;
                    detail.DENKO = row.GetControl<CheckBox>("DENKO").Checked;
                    //detail.IDO_JIKAN = row.GetControl<TextBox>("IDO_JIKAN").Text;
                    detail.BIKO = row.GetControl<TextBox>("BIKO").Text;
                    
                    kintai.DETAILS.Add(detail);
                }

                // 各項目から値を取得
                kintai.RYOHI = int.TryParse(txtRyohi.Text, out intValue) ? intValue : 0;
                kintai.SHIKAKU_HOJO = int.TryParse(txtShikakuHojo.Text, out intValue) ? intValue : 0;
                kintai.SHIKAKU_HOSYO = int.TryParse(txtShikakuHosyo.Text, out intValue) ? intValue : 0;
                kintai.SCHEDULED_WORKING_HOURS = decimal.TryParse(txtSyoteiWorkTime.Text, out decValue) ? decValue : 0;
                kintai.SUM_JUNKEKKIN_KAI = decimal.TryParse(lblSumJunkekkinKai.Text, out decValue) ? decValue : 0;
                kintai.SUM_JUNKEKKIN_JIKAN = decimal.TryParse(lblSumJunkekkinJikan.Text, out decValue) ? decValue : 0;
                kintai.SUM_JIMUSYONAI_JITSUDO = decimal.TryParse(lblSumJimusyonaiJitsudo.Text, out decValue) ? decValue : 0;
                kintai.SUM_YAKAN_TAIO = decimal.TryParse(lblSumYakanTaio.Text, out decValue) ? decValue : 0;
                kintai.SUM_GOKEI_JITSUDO = decimal.TryParse(lblSumGoukeiJitsudo.Text, out decValue) ? decValue : 0;
                kintai.SUM_ZAN_HUTU = decimal.TryParse(lblSumZanHutsu.Text, out decValue) ? decValue : 0;
                kintai.SUM_ZAN_SINYA = decimal.TryParse(lblSumZanSinya.Text, out decValue) ? decValue : 0;
                kintai.SUM_KYUJITSU_KINMU = decimal.TryParse(lblSumKyujitsuKinmu.Text, out decValue) ? decValue : 0;
                kintai.SUM_YAKAN_TAIKI = int.TryParse(lblSumYakanTaiki.Text, out intValue) ? intValue : 0;
                //kintai.SUM_GAISYUTSU_KAI = decimal.TryParse(lblSumGaisyutsuKai.Text, out decValue) ? decValue : 0;
                //kintai.SUM_GAISYUTSU_JIKAN = decimal.TryParse(lblSumGaisyutsuJikan.Text, out decValue) ? decValue : 0;
                kintai.BIKO = lblBiko.Text;

                return kintai;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 勤務表(EXCEL)ファイルダウンロード
        /// </summary>
        /// <param name="IsNeedMoneyInfo"></param>
        private void DownloadKintaihyo(bool IsNeedMoneyInfo)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var kintai = GetKintaiByControl();
                var filePath = Server.MapPath(ConfigurationManager.AppSettings["KintaihyoExcelFilePath"]);
                using (var A = new PssExcelKintai(filePath, kintai, IsNeedMoneyInfo))
                using (var stream = A.PrintExcel())
                {
                    // ダウンロード
                    Response.Clear();
                    Response.Buffer = true;
                    //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Response.ContentType = "application/msexcel";
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Length", stream.Length.ToString());
                    var fileName = string.Format("出勤簿_PSS_{0}-{1}_{2}{3}", kintai.YEAR, kintai.MONTH, kintai.NAME, Extention.XLSX);
                    if (Request.Browser.Browser == "IE" || Request.Browser.Browser == "InternetExplorer")
                    {
                        // IEの場合、ファイル名をURLエンコード
                        fileName = HttpUtility.UrlEncode(fileName);
                    }
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                    Response.BinaryWrite(stream.ToArray());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> csv出力用データ作成
        /// </summary>
        /// <returns></returns>
        private CsvInfo CreateCsvData(T_KINTAI kintai)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var csv = new CsvInfo();
                // アテンド？
                bool IsAttend = kintai.WORK_TYPE == (int)WorkStyle.Attend;

                // 等級取得
                string strGtade = kintai.LANK.Split('-')[0];

                // 幹部？
                bool IsCadres = strGtade == "G" || strGtade == "M";

                // 管理職？
                bool IsManager = IsCadres == false
                                 && int.TryParse(strGtade, out int iTokyu) == true
                                 && iTokyu >= 5;

                // 代休
                int iDaikyu = kintai.DETAILS.Where(dr => dr.SYUKKETSU_KBN == SyukketsuKbn.DAIKYU).Count();

                // 欠勤
                int iKekkin = kintai.DETAILS.Where(dr => dr.SYUKKETSU_KBN == SyukketsuKbn.KEKKIN).Count();

                // 準欠勤
                var tsHourSum = TimeSpan.Zero;
                var intCountSum = 0m;
                int iKekkinKuriage = 0;
                if (IsAttend == false)
                {
                    // アテンドは準欠勤を有給/特休として使用しているためカウントしない
                    foreach (var dr in kintai.DETAILS.Where(dr => dr.JUNKEKKIN_KAI != 0 || dr.JUNKEKKIN_JIKAN != string.Empty))
                    {
                        tsHourSum += dr.JUNKEKKIN_JIKAN.ToTimeSpan();
                        intCountSum += dr.JUNKEKKIN_KAI;
                        if (tsHourSum.TotalHours >= 5)
                        {
                            iKekkinKuriage++;
                            intCountSum = 0;
                            tsHourSum -= new TimeSpan(5L * TimeSpan.TicksPerHour);
                        }
                        else if (intCountSum >= 4)
                        {
                            iKekkinKuriage++;
                            intCountSum = 0;
                            tsHourSum = TimeSpan.Zero;
                        }
                    }
                }

                // 出力用データをテーブルに保持
                // 社員番号 A列
                csv.SYAINBANGOU = kintai.EMPLOYEE_NO;
                // 社員氏名 B列
                csv.SIMEI = kintai.NAME;
                // 平日出勤 C列
                decimal heijitsuSyukkin = 0;
                heijitsuSyukkin += kintai.DETAILS.Where(dr => string.IsNullOrEmpty(dr.KYUJITSU) == true && SyukketsuKbn.ListHeijitsuNormal.Contains(dr.SYUKKETSU_KBN))
                                                 .Select(x => 1m).Sum();
                heijitsuSyukkin += kintai.DETAILS.Where(dr => string.IsNullOrEmpty(dr.KYUJITSU) == true && SyukketsuKbn.ListHeijitsuHalf.Contains(dr.SYUKKETSU_KBN))
                                                 .Select(x => 0.5m).Sum();
                csv.HEIJITUSYKKIN = heijitsuSyukkin.ToString("0.0");
                // 休日出勤 D列
                csv.KYUJITISYUKKIN = kintai.DETAILS.Where(dr => string.IsNullOrEmpty(dr.KYUJITSU) == false && SyukketsuKbn.ListKyujitsu.Contains(dr.SYUKKETSU_KBN))
                                                   .Count().ToString();
                // 出勤時間 E列
                csv.SYUKKINJIKAN = kintai.SUM_GOKEI_JITSUDO.ToString();
                // 遅刻 F列
                csv.CHIKOKU = IsAttend ? string.Empty : kintai.SUM_JUNKEKKIN_KAI.ToString();
                // 早退 G列
                csv.SOUTAI = string.Empty;
                // 有給日数 H列
                csv.YUKYU = kintai.DETAILS.Where(dr => SyukketsuKbn.ListYukyu.Contains(dr.SYUKKETSU_KBN))
                                          .Select(dr => dr.SYUKKETSU_KBN == SyukketsuKbn.YUKYU ? 1m : 0.5m)
                                          .Sum().ToString("0.0");
                // 代休 I列
                csv.DAIKYU = iDaikyu.ToString();
                // 公休 J列
                csv.KOUKYU = string.Empty;
                // その他休日 K列
                csv.SONOHOKA = kintai.DETAILS.Where(dr => SyukketsuKbn.ListTokukyu.Contains(dr.SYUKKETSU_KBN))
                                             .Select(dr => dr.SYUKKETSU_KBN == SyukketsuKbn.TOKUKYU ? 1m : 0.5m)
                                             .Sum().ToString("0.0");
                // 生理休暇 L列
                csv.SEIRIKYUKA = string.Empty;
                // 休職日数 M列
                csv.KYUSYOKU = kintai.DETAILS.Where(dr=>dr.SYUKKETSU_KBN == SyukketsuKbn.KYUGYO)
                                             .Count().ToString();
                // 欠勤日数 N列
                csv.KEKKIN = iKekkin.ToString();
                // 日額表用 O列
                csv.NITIGAKU = string.Empty;
                // 控除日数 P列
                csv.KOUJYONISU = (iDaikyu + iKekkin).ToString();
                // 控除時間 Q列
                csv.KOUJYOJIKAN = kintai.SUM_JUNKEKKIN_JIKAN.ToString();
                // 法定内 R列
                csv.HOUTEINAI = string.Empty;
                // 平日 S列
                csv.HEIJITU = IsAttend ? string.Empty : kintai.SUM_ZAN_HUTU.ToString();
                // 平日深夜 T列
                csv.HEIJITISINYA = kintai.DETAILS.Where(dr => string.IsNullOrEmpty(dr.KYUJITSU) == true && string.IsNullOrEmpty(dr.ZAN_SINYA) == false)
                                                 .Select(dr => dr.ZAN_SINYA.ToTimeSpan())
                                                 .Sum().GetDecimalTotalHours().RoundDown().ToString("0.0");
                // 休日 U列
                csv.KYUJITI = kintai.SUM_KYUJITSU_KINMU.ToString();
                // 休日深夜 V列
                csv.KYUJITISINYA = kintai.DETAILS.Where(dr => string.IsNullOrEmpty(dr.KYUJITSU) == false && string.IsNullOrEmpty(dr.ZAN_SINYA) == false)
                                                 .Select(dr => dr.ZAN_SINYA.ToTimeSpan())
                                                 .Sum().GetDecimalTotalHours().RoundDown().ToString("0.0");
                // 俸給深夜 W列
                csv.HOUKYUSINYA = string.Empty;
                // 法定休日 X列
                csv.HOUTEIKYUJITU = string.Empty;

                // ↓お金関係
                // 所定時間外 Y列
                csv.SYOTEIJIKAN = (IsAttend || IsCadres ? 0 : kintai.SUM_ZAN_HUTU).ToString();
                // 休日勤務割増 Z列
                csv.KYUJITUKINMU = (IsCadres ? 0 : kintai.SUM_KYUJITSU_KINMU).ToString();
                // 法定休日割増 AA列
                csv.HOUTEIKYUJITUWARI = "0";
                // 深夜勤務割増 AB列
                csv.SINNYAKINNMU = kintai.SUM_ZAN_SINYA.ToString();
                // 遅刻/早退 AC列
                csv.CHIKOKU_SOUTAI = (IsAttend || IsCadres || IsManager ? 0 : tsHourSum.GetDecimalTotalHours().RoundOff(2)).ToString("0.00");
                // 欠勤/代休 AD列
                int temp = 0;
                temp += IsCadres ? 0 : iDaikyu;
                temp += IsCadres || IsManager ? 0 : iKekkin + iKekkinKuriage;
                csv.KEKKIN_DAIKYU = temp.ToString();
                // ↑お金関係

                // 法休深夜割増 AE列
                csv.HOUKYUSINYAWARI = "0";
                // 回数手当 AF列
                int kaisuTeate = 0;
                kaisuTeate += kintai.DETAILS.Where(dr => dr.YAKAN_TAIKI == "1").Count() * 2000;
                kaisuTeate += kintai.DETAILS.Where(dr => dr.YAKAN_TAIKI == "2").Count() * 3000;
                csv.KAISUTEATE = kaisuTeate.ToString();
                // 旅費 AG列
                csv.RYOHI = kintai.RYOHI.ToString();
                // 時間外手当 AH列
                csv.JIKANGAI_TEATE = string.Empty;
                // 給与控除額 AI列
                csv.KYUYOKUJYO = string.Empty;
                // テレワーク AJ列
                csv.TELEWORK = kintai.DETAILS.Where(dr => dr.DENKO == true).Count().ToString();

                return csv;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> CSVファイルをダウンロードする
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="outputData"></param>
        private void DownloadCsv(string fileName, List<List<string>> outputData)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                // Contentをクリア
                Response.ClearContent();

                // Contentを設定
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("Shift-JIS");
                Response.ContentType = "text/csv";

                // 表示ファイル名を指定
                if (Request.Browser.Browser == "IE" || Request.Browser.Browser == "InternetExplorer")
                {
                    // IEの場合、ファイル名をURLエンコード
                    fileName = HttpUtility.UrlEncode(fileName);
                }
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);

                // CSVデータを書き込み
                var sb = new StringBuilder();
                foreach (var record in outputData)
                {
                    sb.AppendLine(string.Join(",", record));
                }
                Response.Write(sb.ToString());

                // ダウンロード実行
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 入力チェック
        /// </summary>
        /// <param name="kintai"></param>
        /// <returns></returns>
        private ProcResult<T_KINTAI> CheckKintai(T_KINTAI kintai)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var res = new ProcResult<T_KINTAI>();

                if (kintai.WORK_TYPE == (int)WorkStyle.Attend && kintai.SCHEDULED_WORKING_HOURS < 0)
                {
                    res.SetResult(false, "所定労働時間を0以上で入力してください。", kintai);
                    return res;
                }

                if (kintai.RYOHI < 0)
                {
                    res.SetResult(false, "旅費を0以上で入力してください。", kintai);
                    return res;
                }

                foreach (var detail in kintai.DETAILS)
                {
                    ProcResult<TimeSpan?> checkTime;

                    checkTime = detail.START.ToTimeSpanTry("出勤");
                    if (!checkTime.Result)
                    {
                        res.SetResult(false, checkTime.Message, kintai);
                        break;
                    }

                    checkTime = detail.END.ToTimeSpanTry("退勤");
                    if (!checkTime.Result)
                    {
                        res.SetResult(false, checkTime.Message, kintai);
                        break;
                    }

                    checkTime = detail.JUNKEKKIN_JIKAN.ToTimeSpanTry("準欠勤時間");
                    if (!checkTime.Result)
                    {
                        res.SetResult(false, checkTime.Message, kintai);
                        break;
                    }

                    checkTime = detail.JIMUSYONAI_JITSUDO.ToTimeSpanTry("通常実働時間");
                    if (!checkTime.Result)
                    {
                        res.SetResult(false, checkTime.Message, kintai);
                        break;
                    }

                    checkTime = detail.YAKAN_TAIO.ToTimeSpanTry("夜間対応時間");
                    if (!checkTime.Result)
                    {
                        res.SetResult(false, checkTime.Message, kintai);
                        break;
                    }

                    checkTime = detail.GOKEI_JITSUDO.ToTimeSpanTry("合計実働時間");
                    if (!checkTime.Result)
                    {
                        res.SetResult(false, checkTime.Message, kintai);
                        break;
                    }

                    checkTime = detail.ZAN_HUTSU.ToTimeSpanTry("普通残業");
                    if (!checkTime.Result)
                    {
                        res.SetResult(false, checkTime.Message, kintai);
                        break;
                    }

                    checkTime = detail.ZAN_SINYA.ToTimeSpanTry("深夜残業");
                    if (!checkTime.Result)
                    {
                        res.SetResult(false, checkTime.Message, kintai);
                        break;
                    }

                    checkTime = detail.KYUJITSU_KINMU.ToTimeSpanTry("休日勤務");
                    if (!checkTime.Result)
                    {
                        res.SetResult(false, checkTime.Message, kintai);
                        break;
                    }

                    //checkTime = detail.SINYA_JIKAN.ToTimeSpanTry("深夜時間");
                    //if (!checkTime.Result)
                    //{
                    //    res.SetResult(false, checkTime.Message, kintai);
                    //    break;
                    //}

                    //checkTime = detail.GAISYUTSU_JIKAN.ToTimeSpanTry("私用外出時間");
                    //if (!checkTime.Result)
                    //{
                    //    res.SetResult(false, checkTime.Message, kintai);
                    //    break;
                    //}

                    //checkTime = detail.IDO_JIKAN.ToTimeSpanTry("移動時間");
                    //if (!checkTime.Result)
                    //{
                    //    res.SetResult(false, checkTime.Message, kintai);
                    //    break;
                    //}
                }

                return res;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        private void SaveKintai(T_KINTAI kintai)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var dba = new DbaKintai(Master.LoginEmployeeInfo.NO);
                dba.RegistKintaiInfo(kintai);
                Master.KintaiInfo = kintai;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        #endregion

    }
}