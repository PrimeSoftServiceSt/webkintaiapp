﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.DBA;
using PssWebKintai.Entity;
using System;
using System.Reflection;
using System.Web.UI.WebControls;

namespace PssWebKintai
{
    public partial class WorkType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                    var dba = new DbaWorkShift(Master.LoginEmployeeInfo.NO);
                    ListView1.DataSource = dba.GetWorkTypeList();
                    ListView1.DataBind();
                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                    Master.ShowErrorAlart();
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                }
            }
        }

        protected void UpdateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var btn = (Button)sender;
                var item = btn.GetParentItem();
                var workType = new M_WORK_TYPE
                {
                    WORK_TYPE = int.Parse(item.GetControl<HiddenField>("WORK_TYPE").Value),
                    NAME = item.GetControl<TextBox>("NAME").Text
                };

                var dba = new DbaWorkShift(Master.LoginEmployeeInfo.NO);
                dba.UpdateWorkType(workType);

                ListView1.DataSource = dba.GetWorkTypeList();
                ListView1.DataBind();

                Master.ShowAlart("勤務形態を登録しました。", UpdatePanel1);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        protected void DetailBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var btn = (Button)sender;
                var item = btn.GetParentItem();

                Session[SessionKey.SELECTED_WORK_TYPE] = item.GetControl<HiddenField>("WORK_TYPE").Value;
                Response.Redirect("WorkShift.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                Response.Redirect("Menu.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        protected void InsertBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var btn = (Button)sender;
                var item = btn.GetParentItem();
                var name = item.GetControl<TextBox>("NAME").Text;

                var dba = new DbaWorkShift(Master.LoginEmployeeInfo.NO);
                dba.InsertWorkType(name);

                ListView1.DataSource = dba.GetWorkTypeList();
                ListView1.DataBind();

                Master.ShowAlart("勤務形態を登録しました。", UpdatePanel1);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }
    }
}