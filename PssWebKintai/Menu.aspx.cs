﻿using PssCommonLib;
using System;
using System.Reflection;

namespace PssWebKintai
{
    public partial class Menu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                    Master.ShowErrorAlart();
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                }
            }
        }
    }
}