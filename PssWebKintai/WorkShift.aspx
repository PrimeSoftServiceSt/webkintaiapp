﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WorkShift.aspx.cs" Inherits="PssWebKintai.WorkShift" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:Label ID="lblWorkTypeName" runat="server" />

    <%--   ヘッダー   --%>
    <div id="GridHeader">
        <table border="1">
            <tr class="tableHeader">
                <th>
                    <asp:Label Width="93px" runat="server" Style="text-align: center" Text="登録/削除" /></th>
                <th>
                    <asp:Label Width="87px" runat="server" Style="text-align: center" Text="出欠区分" /></th>
                <th>
                    <asp:Label Width="80px" runat="server" Style="text-align: center" Text="始業" /></th>
                <th>
                    <asp:Label Width="80px" runat="server" Style="text-align: center" Text="終業" /></th>
                <th>
                    <asp:Label Width="400px" runat="server" Style="text-align: center" Text="休憩" /></th>
                <%--<th>
                    <asp:Label Width="400px" runat="server" Style="text-align: center" Text="残業" /></th>--%>
            </tr>
        </table>
    </div>

    <%--   ボディ   --%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="GridBody" style="max-height: 300px; width:765px">
                <asp:ListView ID="ListView1" runat="server" InsertItemPosition="LastItem">
                    <LayoutTemplate>
                        <table id="itemPlaceholderContainer" runat="server" border="1">
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class='<%# Container.DisplayIndex % 2 == 0 ? "even" : "odd" %>' >
                            <td>
                                <asp:Button ID="UpdateBtn" runat="server" Text="更新" OnClientClick='return confirm("更新しますか？");' OnClick="UpdateBtn_Click" />
                                <asp:Button ID="DeleteBtn" runat="server" Text="削除" OnClientClick='return confirm("削除しますか？");' OnClick="DeleteBtn_Click" /></td>
                            <td>
                                <asp:DropDownList ID="SYUKKETSU_KBN" runat="server" SelectedValue='<%# Eval("SYUKKETSU_KBN") %>' OnInit="SYUKKETSU_KBN_Init" Enabled="false" /></td>
                            <td>
                                <asp:TextBox Width="80px" ID="START" runat="server" Text='<%# Eval("START") %>' TextMode="Time" /></td>
                            <td>
                                <asp:TextBox Width="80px" ID="END" runat="server" Text='<%# Eval("END") %>' TextMode="Time" /></td>
                            <td>
                                <asp:TextBox Width="400px" ID="REST" runat="server" Text='<%# Eval("REST") %>' Style="max-width:400px" /></td>
                            <%--<td>
                                <asp:TextBox Width="400px" ID="ZAN" runat="server" Text='<%# Eval("ZAN") %>' /></td>--%>
                        </tr>
                    </ItemTemplate>
                    <InsertItemTemplate>
                        <tr class="odd">
                            <td>
                                <asp:Button ID="InsertBtn" runat="server" Text="登録" OnClientClick='return confirm("登録しますか？");' OnClick="InsertBtn_Click" />
                                <asp:Button ID="DeleteBtn" runat="server" Text="削除" Enabled="false"/></td>
                            <td>
                                <asp:DropDownList ID="SYUKKETSU_KBN" runat="server" OnInit="SYUKKETSU_KBN_Init" /></td>
                            <td>
                                <asp:TextBox Width="80px" ID="START" runat="server" TextMode="Time" /></td>
                            <td>
                                <asp:TextBox Width="80px" ID="END" runat="server" TextMode="Time" /></td>
                            <td>
                                <asp:TextBox Width="400px" ID="REST" runat="server" Style="max-width:400px" /></td>
                            <%--<td>
                                <asp:TextBox Width="400px" ID="ZAN" runat="server" /></td>--%>
                        </tr>
                    </InsertItemTemplate>
                </asp:ListView>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="btnBack" runat="server" Text="戻る" OnClick="btnBack_Click" />
</asp:Content>
