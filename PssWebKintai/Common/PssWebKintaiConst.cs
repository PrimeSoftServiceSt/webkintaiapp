﻿using System.Collections.Generic;

namespace PssWebKintai.Common
{
    /// <summary> 丸
    /// </summary>
    public struct Circle
    {
        /// <summary> 〇
        /// </summary>
        public const string WHITE = "○";
        /// <summary> ●
        /// </summary>
        public const string BLACK = "●";
    }

    /// <summary> セッションキー
    /// </summary>
    public struct SessionKey
    {
        /// <summary> ログイン中の社員情報
        /// </summary>
        public const string LOGIN_EMPLOYEE_INFO = "loginemployeeInfo";

        /// <summary> 表示中の社員情報
        /// </summary>
        public const string DISPLAY_EMPLOYEE_INFO = "displayemployeeInfo";

        /// <summary> 勤怠情報
        /// </summary>
        public const string KINTAI_INFO = "kintaiInfo";

        /// <summary> 自宅夜間対応情報
        /// </summary>
        public const string YAKAN_INFO = "yakanInfo";

        /// <summary> 選択中の勤務形態
        /// </summary>
        public const string SELECTED_WORK_TYPE = "selectedworktype";

        /// <summary> 出欠区分リスト
        /// </summary>
        public const string SYUKKETSU_KBN_LIST = "syukketsukbnlist";
    }

    /// <summary> 出欠区分
    /// </summary>
    public struct SyukketsuKbn
    {
        /// <summary> なし
        /// </summary>
        public const string EMPTY = "000";
        /// <summary> 出勤
        /// </summary>
        public const string SYUKKIN = "001";
        /// <summary> 有給
        /// </summary>
        public const string YUKYU = "002";
        /// <summary> 半休
        /// </summary>
        public const string HANKYU = "003";
        /// <summary> 代休
        /// </summary>
        public const string DAIKYU = "004";
        /// <summary> 特休
        /// </summary>
        public const string TOKUKYU = "005";
        /// <summary> 半特
        /// </summary>
        public const string HANTOKU = "006";
        /// <summary> 休出
        /// </summary>
        public const string KYUSYUTSU = "007";
        /// <summary> 欠勤
        /// </summary>
        public const string KEKKIN = "008";
        /// <summary> 早出
        /// </summary>
        public const string HAYADE = "009";
        /// <summary> 遅番
        /// </summary>
        public const string OSOBAN = "010";
        /// <summary> トラ相
        /// </summary>
        public const string TORASO = "011";
        /// <summary> フル
        /// </summary>
        public const string FULL = "012";
        /// <summary> 夜勤
        /// </summary>
        public const string YAKIN = "013";
        /// <summary> 休日夜間
        /// </summary>
        public const string KYUJITUYAKIN = "014";
        /// <summary> 早番
        /// </summary>
        public const string HAYABAN = "015";
        /// <summary> 休業
        /// </summary>
        public const string KYUGYO = "016";

        /// <summary> 平日出勤(1日分)リスト
        /// </summary>
        public static readonly List<string> ListHeijitsuNormal = new List<string>()
        {
            SYUKKIN,
            HAYADE,
            OSOBAN,
            TORASO,
            FULL,
            YAKIN,
            HAYABAN
        };

        /// <summary> 平日出勤(0.5日分)リスト
        /// </summary>
        public static readonly List<string> ListHeijitsuHalf = new List<string>()
        {
            HANKYU,
            HANTOKU
        };

        /// <summary> 休日出勤リスト
        /// </summary>
        public static readonly List<string> ListKyujitsu = new List<string>()
        {
            KYUSYUTSU,
            KYUJITUYAKIN
        };

        /// <summary> 有給リスト
        /// </summary>
        public static readonly List<string> ListYukyu = new List<string>()
        {
            YUKYU,
            HANKYU
        };

        /// <summary> 特休リスト
        /// </summary>
        public static readonly List<string> ListTokukyu = new List<string>()
        {
            TOKUKYU,
            HANTOKU
        };
    }

    /// <summary> 勤務形態
    /// </summary>
    public enum WorkStyle
    {
        Attend = 1,
        NR = 2,
        LCM = 16
    }

    /// <summary> 勤怠ステータス
    /// </summary>
    public enum KintaiStatus
    {
        None = 0,
        SyoninIrai = 1,
        SyoninZumi = 2,
        Kakutei = 3,
        Sashimodoshi = 4
    }

    /// <summary> 区分
    /// </summary>
    public struct KBN
    {
        public const string SyukketsuKbn = "A0001";
        public const string Role = "A0002";
        public const string YakanTaiki = "A0003";
        public const string SFTR = "A0004";
    }

    /// <summary> 権限
    /// </summary>
    public enum Role
    {
        Normal = 0,
        Superior = 1,
        Clerk = 2
    }
}
