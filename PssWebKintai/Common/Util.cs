﻿using PssWebKintai.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PssWebKintai.Common
{
    /// <summary> 共通機能
    /// </summary>
    public static class Util
    {
        /// <summary> 先頭に空行を追加したドロップダウンのデータソースを作成する
        /// </summary>
        /// <param name="items"></param>
        /// <param name="isNeedEmpty"></param>
        /// <returns></returns>
        public static List<ListItem> CreateDrpDataSource(IEnumerable<ListItem> items, bool isNeedEmpty = true, string emptyValue = "")
        {
            var source = new List<ListItem>();
            if (isNeedEmpty)
            {
                source.Add(new ListItem() { Value = emptyValue, Text = "---------" });
            }
            source.AddRange(items);
            return source;
        }
    }

    /// <summary> 拡張メソッド
    /// </summary>
    public static class ExpandLogic
    {
        /// <summary> 名前でコントロールを取得する(型指定)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        public static T GetControl<T>(this ListViewItem item, string controlName) where T : class
        {
            return item.FindControl(controlName) as T;
        }

        /// <summary> ドロップダウンリストにListItemを設定する
        /// </summary>
        /// <param name="drp"></param>
        /// <param name="items"></param>
        public static void SetItem(this DropDownList drp, IEnumerable<ListItem> items)
        {
            drp.DataValueField = "Value";
            drp.DataTextField = "Text";
            foreach (var item in items)
            {
                var temp = new ListItem() { Value = item.Value, Text = string.IsNullOrEmpty(item.Text) ? "----------" : item.Text };
                drp.Items.Add(temp);
            }
        }

        /// <summary> コントロールの親ListViewItemを取得する
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public static ListViewItem GetParentItem(this Control control)
        {
            try
            {
                var parent = control.Parent;
                if (parent is ListViewItem)
                {
                    return parent as ListViewItem;
                }
                else
                {
                    return parent.GetParentItem();
                }
            }
            catch
            {
                return null;
            }
        }

        public static string GetValue1(this List<M_KBN> lst, string key1, string key2 = "", string key3 = "")
        {
            var res1 = lst.Where(x => x.KEY1 == key1);
            if (key2 != string.Empty)
            {
                res1 = res1.Where(x => x.KEY2 == key2);
                if (key3 != string.Empty)
                {
                    res1 = res1.Where(x => x.KEY3 == key3);
                }
            }
            var res = res1.FirstOrDefault();
            return res != null ? res.VALUE1 : string.Empty;
        }

    }
}