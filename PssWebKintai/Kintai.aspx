﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Kintai.aspx.cs" Inherits="PssWebKintai.Kintai" %>

<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%--   スクリプト   --%>
    <script src="<%= ResolveUrl("~/Scripts/PssJs.js") %>" type="text/javascript" charset="utf-8">
        num_check(str, allowMinus = false, allowPoint = false)
    </script>
    <script src="<%= ResolveUrl("~/Scripts/jquery.numeric.js") %>" type="text/javascript" charset="utf-8">
    </script>
    <script>
        $(document).ready(function () {

            //読込イベント処理を書く

            var div1 = $('#GridHeader');
            var div2 = $('#GridBody');
            var div3 = $('#GridSum');
            var div4 = $('#GridBody_fix');

            div2.scroll(function () {

                div1.scrollLeft(div2.scrollLeft())
                div3.scrollLeft(div2.scrollLeft())
                div4.scrollTop(div2.scrollTop())
            });
            div3.scroll(function () {

                div1.scrollLeft(div3.scrollLeft())
                div2.scrollLeft(div3.scrollLeft())
            });
            //div4.scroll(function () {
            //    div2.scroll(div4.scrollTop())
            //});

            //// 数値
            //$(".numeric").numeric();
            //// 整数
            //$(".integer").numeric({ decimal: false });
            //// 正の数値
            //$(".positive").numeric({ negative: false });
            //// 正の整数
            //$(".positive-integer").numeric({ decimal: false, negative: false });

            // *「右クリック貼り付け」対応
            $(".antirc").change(function () {
                $(this).keyup();
            });
        });
    </script>

    <%--   社員情報   --%>
    <div>
        <div style="float: left; padding-right: 10px">
            <asp:Label runat="server" Text="社員番号：" />
            <asp:TextBox Width="80px" ID="txtNo" runat="server" Enabled="false" />
        </div>
        <div style="float: left; padding-right: 10px">
            <asp:Label runat="server" Text="社員名：" />
            <asp:TextBox Width="100px" ID="txtName" runat="server" Enabled="false" />
        </div>
        <div style="float: left; padding-right: 10px">
            <asp:Label runat="server" Text="所属：" />
            <asp:TextBox Width="200px" ID="txtPost" runat="server" Enabled="false" />
        </div>
        <div style="float: left; padding-right: 10px">
            <asp:Label runat="server" Text="勤務形態：" />
            <asp:TextBox Width="150px" ID="txtWorkType" runat="server" Enabled="false" />
            <asp:Button ID="btnUpdateEmployeeInfo" runat="server" Text="社員情報更新" OnClick="btnUpdateEmployeeInfo_Click"/>
        </div>
    </div>
    <br />
    <hr />

    
    <%--   操作ボタンなど①   --%>
    <asp:Panel ID="pnlInput" runat="server">

        <asp:DropDownList ID="drpMonth" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpMonth_SelectedIndexChanged" />
        
        <asp:Label runat="server" Text="旅費(円)：" ID="lblRyohi"/>
        <asp:TextBox runat="server" Width="100px" Style="text-align: right" ID="txtRyohi" TextMode="Number" />
        <asp:Label runat="server" Text="資格補助金(円)：" ID="Label2"/>
        <asp:TextBox runat="server" Width="100px" Style="text-align: right" ID="txtShikakuHojo" TextMode="Number" />
        <asp:Label runat="server" Text="資格報奨金(円)：" ID="Label3"/>
        <asp:TextBox runat="server" Width="100px" Style="text-align: right" ID="txtShikakuHosyo" TextMode="Number" />
        <asp:Label runat="server" Text="所定労働時間(h)：" ID="lblSyoteiWorkTime" />
        <asp:TextBox runat="server" Width="100px" Style="text-align: right" ID="txtSyoteiWorkTime" />
                
    </asp:Panel>
    <br />

    <asp:Panel ID="pnlButton" runat="server" style="margin: 0 auto 20px;">
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnAutoInput" runat="server" Text="自動入力" OnClick="btnAutoInput_Click" />
                    <asp:Button ID="btnYakan" runat="server" Text="自宅夜間対応" OnClick="btnYakan_Click" />
                    <asp:Button ID="btnCalcKintai" runat="server" Text="勤務時間計算" OnClick="btnCalcKintai_Click" />
                    <asp:Button ID="btnCalcSum" runat="server" Text="手動修正完了" OnClick="btnCalcSum_Click"
                        data-toggle="tooltip" title="合計行が再計算されます。手動修正時のみ押すようにしてください。"/>
                </td>
                <td style="width:20px"/>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="勤怠の保存" OnClick="btnSave_Click" />
                    <asp:Button ID="btnIrai" runat="server" Text="承認依頼" OnClientClick="return ShowConfirm('承認依頼を行いますか？')" OnClick="btnIrai_Click" />
                    <asp:Button ID="btnSyonin" runat="server" Text="承認" OnClientClick="return ShowConfirm('承認を行いますか？')" OnClick="btnSyonin_Click" />
                    <asp:Button ID="btnKakutei" runat="server" Text="確定" OnClientClick="return ShowConfirm('確定を行いますか？')" OnClick="btnKakutei_Click" />
                    <asp:Button ID="btnSashimodoshi" runat="server" Text="差戻" OnClientClick="return ShowConfirm('差戻を行いますか？')" OnClick="btnSashimodoshi_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <hr />

    <table>
        <tr>
            <td>
                <%--左上--%>
                <div id="GridHeader_fix" >
                    <table border="1">
                        <tr class="tableHeader">
                            <th runat="server">
                                <div style="height:42px;display:table">
                                    <asp:Label Width="60px" runat="server" style="display:table-cell;text-align:center;vertical-align:middle"
                                        Text="日付(曜)" ID="lblHeadDay" />
                                </div>
                            </th>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
                <%--右上--%>
                <div id="GridHeader" style="width: 1117px">
                    <table border="1">
                        <tr class="tableHeader">
                            <th runat="server" rowspan="2">
                                <asp:Label Width="60px" runat="server" Style="text-align: center"
                                    Text="休日" ID="lblHeadKyujitsu"/></th>
                            <th runat="server" rowspan="2">
                                <asp:Label Width="60px" runat="server" Style="text-align: center"
                                    Text="出欠区分" ID="lblHeadSyukketsuKbn"/></th>
                            <th runat="server" rowspan="2">
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="出勤" ID="lblHeadStart"/></th>
                            <th runat="server" rowspan="2">
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="退勤" ID="lblHeadEnd"/></th>
                            <th runat="server" colspan="2" style="text-align: center">
                                <asp:Label runat="server"
                                    Text="準欠勤" ID="lblHeadJunkekkin"/></th>
                            <th runat="server" rowspan="2">
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="通常実働" ID="lblHeadJimusyonaiJitsudo1"/><br>
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="時間" ID="lblHeadJimusyonaiJitsudo2"/></th>
                            <th runat="server" rowspan="2">
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="夜間対応" ID="lblHeadYakanTaio1"/><br>
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="時間" ID="lblHeadYakanTaio2"/></th>
                            <th runat="server" rowspan="2">
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="合計実働" ID="lblHeadGokeiJitsudo1"/><br>
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="時間" ID="lblHeadGkeiJitsudo2"/></th>
                            <th runat="server" colspan="2" style="text-align: center">
                                <asp:Label runat="server"
                                    Text="時間外時間" ID="lblHeadZan"/></th>
                            <th runat="server" rowspan="2">
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="休日勤務" ID="lblHeadKyujitsuKinmu"/></th>
                            <th runat="server" rowspan="2">
                                <!-- テレワーク欄として使用 -->
                                <asp:Label Width="30px" runat="server" Style="text-align: center"
                                    Text="テ" ID="lblHeadDenko1"/><br>
                                <asp:Label Width="30px" runat="server" Style="text-align: center"
                                    Text="レ" ID="lblHeadDenko2"/></th>
                            <th runat="server" rowspan="2">
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="夜間" ID="lblHeadYakanTaiki1"/></th>
                            <th runat="server" rowspan="2">
                                <asp:Label Width="200px" runat="server" Style="text-align: center"
                                    Text="備考" ID="lblHeadBiko"/></th>
                        </tr>
                        <tr class="tableHeader">
                            <th runat="server">
                                <asp:Label Width="35px" runat="server" Style="text-align: center"
                                    Text="回" ID="lblHeadJunkekkinKai"/></th>
                            <th runat="server">
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="時間" ID="lblHeadJunkekkinJikan"/></th>
                            <th runat="server">
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="普通" ID="lblHeadZanHutsu"/></th>
                            <th runat="server">
                                <asp:Label Width="80px" runat="server" Style="text-align: center"
                                    Text="深夜" ID="lblHeadZanSinya"/></th>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <%--左まんなか--%>
                <div id="GridBody_fix" style="height: 350px;">
                    <asp:ListView ID="ListView2" runat="server">
                        <LayoutTemplate>
                            <table id="itemPlaceholderContainer" runat="server" border="1">
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr class='<%# Container.DisplayIndex % 2 == 0 ? "even" : "odd" %>' >
                                <td>
                                    <asp:TextBox Width="60px" ID="DAY" runat="server" Text='<%# DateTime.Parse(Eval("YEAR") + "/" + Eval("MONTH") + "/" + Eval("DAY")).ToString("dd(ddd)") %>' Enabled="false"/></td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </td>
            <td>
                <%--右まんなか--%>
                <div id="GridBody" style="height: 350px; width: 1134px">
                    <asp:ListView ID="ListView1" runat="server">
                        <LayoutTemplate>
                            <table id="itemPlaceholderContainer" runat="server" border="1">
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr class='<%# Container.DisplayIndex % 2 == 0 ? "even" : "odd" %>' >
                                <td>
                                    <asp:HiddenField ID="DAY" runat="server" Value='<%# Eval("DAY") %>'/>
                                    <asp:HiddenField ID="SINYA_JIKAN" runat="server" Value='<%# Bind("SINYA_JIKAN") %>'/>
                                    <asp:TextBox Width="60px" ID="KYUJITSU" runat="server" Style="text-align: center" Text='<%# Bind("KYUJITSU") %>' /></td>
                                <td>
                                    <asp:DropDownList Width="60px" ID="SYUKKETSU_KBN" runat="server" SelectedValue='<%# Eval("SYUKKETSU_KBN") %>' OnInit="SYUKKETSU_KBN_Init" /></td>
                                <td>
                                    <asp:TextBox Width="80px" ID="START" runat="server" Text='<%# Bind("START") %>' TextMode="Time" OnDataBinding="TimeTextBox_DataBinding" /></td>
                                <td>
                                    <asp:TextBox Width="80px" ID="END" runat="server" Text='<%# Bind("END") %>' TextMode="Time" OnDataBinding="TimeTextBox_DataBinding" /></td>
                                <td>
                                    <asp:TextBox Width="35px" ID="JUNKEKKIN_KAI" runat="server" Style="text-align: right" Text='<%# Bind("JUNKEKKIN_KAI") %>' class="positive" /></td>
                                <td>
                                    <asp:TextBox Width="80px" ID="JUNKEKKIN_JIKAN" runat="server" Text='<%# Bind("JUNKEKKIN_JIKAN") %>' TextMode="Time" OnDataBinding="TimeTextBox_DataBinding" /></td>
                                <td>
                                    <asp:TextBox Width="80px" ID="JIMUSYONAI_JITSUDO" runat="server" Text='<%# Bind("JIMUSYONAI_JITSUDO") %>' TextMode="Time" OnDataBinding="TimeTextBox_DataBinding" /></td>
                                <td>
                                    <asp:TextBox Width="80px" ID="YAKAN_TAIO" runat="server" Text='<%# Bind("YAKAN_TAIO") %>' TextMode="Time" OnDataBinding="TimeTextBox_DataBinding" /></td>
                                <td>
                                    <asp:TextBox Width="80px" ID="GOKEI_JITSUDO" runat="server" Text='<%# Bind("GOKEI_JITSUDO") %>' TextMode="Time" OnDataBinding="TimeTextBox_DataBinding" /></td>
                                <td>
                                    <asp:TextBox Width="80px" ID="ZAN_HUTSU" runat="server" Text='<%# Bind("ZAN_HUTSU") %>' TextMode="Time" OnDataBinding="TimeTextBox_DataBinding" /></td>
                                <td>
                                    <asp:TextBox Width="80px" ID="ZAN_SINYA" runat="server" Text='<%# Bind("ZAN_SINYA") %>' TextMode="Time" OnDataBinding="TimeTextBox_DataBinding" /></td>
                                <td>
                                    <asp:TextBox Width="80px" ID="KYUJITSU_KINMU" runat="server" Text='<%# Bind("KYUJITSU_KINMU") %>' TextMode="Time" OnDataBinding="TimeTextBox_DataBinding" /></td>
                                <td align="center">
                                    <asp:CheckBox Width="30px" ID="DENKO" runat="server" Checked='<%# Bind("DENKO") %>' /></td>
                                <td>
                                    <asp:DropDownList Width="35px" ID="YAKAN_TAIKI" runat="server" SelectedValue='<%# Eval("YAKAN_TAIKI") %>' OnInit="YAKAN_TAIKI_Init" /></td>
                                <td>
                                    <asp:DropDownList Width="45px" ID="TETUYA_AKEKIN" runat="server" SelectedValue='<%# Eval("TETUYA_AKEKIN") %>' OnInit="TETUYA_AKEKIN_Init"/></td>
                                <td>
                                    <asp:TextBox Width="200px" ID="BIKO" runat="server" Text='<%# Bind("BIKO") %>' /></td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <%--左下--%>
                <div id="GridSum_fix">
                    <table border="1">
                        <tr>
                            <td>
                                <asp:label Width="60px" runat="server" Style="text-align: center" Text="合計"/></td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
                <%--右下--%>
                <div id="GridSum" style="width: 1117px">
                    <table border="1">
                        <tr>
                            <td>
                                <asp:Label Width="284px" ID="Label1" runat="server"/></td>
                            <td>
                                <asp:Label Width="35px" ID="lblSumJunkekkinKai" runat="server" Style="text-align: right" /></td>
                            <td>
                                <asp:Label Width="80px" ID="lblSumJunkekkinJikan" runat="server" Style="text-align: right" /></td>
                            <td>
                                <asp:Label Width="80px" ID="lblSumJimusyonaiJitsudo" runat="server" Style="text-align: right" /></td>
                            <td>
                                <asp:Label Width="80px" ID="lblSumYakanTaio" runat="server" Style="text-align: right" /></td>
                            <td>
                                <asp:Label Width="80px" ID="lblSumGoukeiJitsudo" runat="server" Style="text-align: right" /></td>
                            <td>
                                <asp:Label Width="80px" ID="lblSumZanHutsu" runat="server" Style="text-align: right" /></td>
                            <td>
                                <asp:Label Width="80px" ID="lblSumZanSinya" runat="server" Style="text-align: right" /></td>
                            <td>
                                <asp:Label Width="80px" ID="lblSumKyujitsuKinmu" runat="server" Style="text-align: right" /></td>
                            <td>
                                <asp:Label Width="30px" ID="lblSumDenko" runat="server" Style="text-align: right" /></td>
                            <td>
                                <asp:Label Width="80px" ID="lblSumYakanTaiki" runat="server" Style="text-align: right" /></td>
                            <td>
                                <asp:Label Width="199px" ID="lblBiko" runat="server" /></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>

    <%--   操作ボタンなど②   --%>
    <div>
        <table>
            <tr>
                <td style="width: 33%">
                    <asp:Button ID="btnExcel" runat="server" Text="EXCEL" OnClick="btnExcel_Click" />
                    <asp:Button ID="btnPrint" runat="server" Text="出力" OnClick="btnPrint_Click" />
                </td>
                <td style="width: 33%" align="center">
                    <asp:DropDownList ID="drpMachi" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpMachi_SelectedIndexChanged" />
                </td>
                <td style="width: 33%" align="right">
                    <asp:DropDownList ID="drpCsvMonth" runat="server" />
                    <asp:Button ID="btnCsv" runat="server" Text="CSV出力" OnClick="btnCsv_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
