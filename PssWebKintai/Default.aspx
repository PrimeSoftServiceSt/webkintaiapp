﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PssWebKintai._Default" %>

<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script src="<%= ResolveUrl("~/Scripts/PssJs.js") %>" type="text/javascript">
        num_check(str, allowMinus = false, allowPoint = false);
    </script>
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" BackColor="LightYellow" />
    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="社員番号：" />
            </td>
            <td>
                <asp:TextBox ID="txtEmployeeNo" runat="server" MaxLength="6" />
                <asp:RequiredFieldValidator ID="reqEmployeeNo" runat="server" ControlToValidate="txtEmployeeNo" ErrorMessage="社員番号" ForeColor="Red" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="パスワード：" />
            </td>
            <td>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" />
                <asp:RequiredFieldValidator ID="reqPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="パスワード" ForeColor="Red" />
            </td>
        </tr>
    </table>
    <asp:CheckBox ID="ckbKanrisya" runat="server" Text="管理者" />
    <asp:Button ID="btnLogin" runat="server" Text="ログイン" OnClick="btnLogin_Click" />
    <asp:Button ID="btnChangePassword" runat="server" Text="パスワード変更" OnClick="btnChangePassword_Click" CausesValidation="false" />
</asp:Content>
