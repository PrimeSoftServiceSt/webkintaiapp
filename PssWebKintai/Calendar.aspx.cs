﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.DBA;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI.WebControls;

namespace PssWebKintai
{
    public partial class Calendar : System.Web.UI.Page
    {
        /// <summary> ページロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                    var year = DateTime.Today.Year;
                    var lstItem = new List<ListItem>
                    {
                        new ListItem() { Value = (year + 1).ToString(), Text = (year + 1).ToString() },
                        new ListItem() { Value = year.ToString(), Text = year.ToString() },
                        new ListItem() { Value = (year - 1).ToString(), Text = (year - 1).ToString() }
                    };
                    drpYear.SetItem(lstItem);
                    drpYear.SelectedValue = year.ToString();

                    var lstCalendar = GetCalendar(year);
                    SetDataSource(lstCalendar);
                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                    Master.ShowErrorAlart();
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
                }
            }
        }

        /// <summary> 年ドロップ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void drpYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                var drp = (DropDownList)sender;

                var lstCalendar = GetCalendar(int.Parse(drp.SelectedValue));
                SetDataSource(lstCalendar);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
            }
        }

        protected void btnNewRow_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                var lstCalendar = GetCalendarByControl();
                int year = int.Parse(drpYear.SelectedValue);
                lstCalendar.Add(new M_CALENDAR() { HOLIDAY = new DateTime(year, 1, 1) });
                SetDataSource(lstCalendar);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                int year = int.Parse(drpYear.SelectedValue);
                var lstCalendar = GetCalendarByControl();
                var dba = new DbaCalendar(Master.LoginEmployeeInfo.NO);
                dba.RegistCalendar(year, lstCalendar);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
            }
        }

        protected void DeleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                var btn = (Button)sender;
                var lstCalendar = GetCalendarByControl(btn.ClientID);
                SetDataSource(lstCalendar);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
            }
        }

        private List<M_CALENDAR> GetCalendar(int year)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                var dba = new DbaPss();
                return dba.GetCalendar(year);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
            }
        }

        private List<M_CALENDAR> GetCalendarByControl(string deleteBtnID = "")
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                var lstCalendar = new List<M_CALENDAR>();
                foreach (var item in ListView1.Items)
                {
                    if (deleteBtnID != string.Empty)
                    {
                        var btn = item.GetControl<Button>("DeleteBtn");
                        if (btn.ClientID == deleteBtnID)
                        {
                            continue;
                        }
                    }
                    lstCalendar.Add(new M_CALENDAR() { HOLIDAY = item.GetControl<TextBox>("HOLIDAY").Text.ToDateTime() });
                }

                return lstCalendar;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
            }
        }

        private void SetDataSource(List<M_CALENDAR> lstCalendar)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                ListView1.DataSource = lstCalendar;
                ListView1.DataBind();
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), "");
                Response.Redirect("Menu.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), "");
            }
        }
    }
}