﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.DBA;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;

namespace PssWebKintai
{
    public partial class WorkShift : System.Web.UI.Page
    {
        /// <summary> 勤務形態区分値
        /// </summary>
        private int? WorkType
        {
            get { return int.Parse(Session[SessionKey.SELECTED_WORK_TYPE].ToString()); }
            set
            {
                if (value.HasValue)
                    Session[SessionKey.SELECTED_WORK_TYPE] = value.Value.ToString();
                else
                    Session[SessionKey.SELECTED_WORK_TYPE] = null;
            }
        }

        /// <summary> 出欠区分リスト
        /// </summary>
        private List<ListItem> lstSyukketsuKbn;

        /// <summary> ページロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);

                    var dbaPss = new DbaPss();
                    lblWorkTypeName.Text = dbaPss.GetWorkTypeName(WorkType.Value);

                    var dba = new DbaWorkShift(Master.LoginEmployeeInfo.NO);
                    ListView1.DataSource = dba.GetWorkShift(WorkType.Value);
                    ListView1.DataBind();
                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                    Master.ShowErrorAlart();
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                }
            }
        }

        /// <summary> 出欠区分ドロップダウン初期化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SYUKKETSU_KBN_Init(object sender, EventArgs e)
        {
            try
            {
                if (lstSyukketsuKbn == null || lstSyukketsuKbn.Count == 0)
                {
                    var dba = new DbaPss();
                    lstSyukketsuKbn = dba.GetSyukketsuKbnList(true).Select(x => new ListItem() { Value = x.KEY1, Text = string.IsNullOrEmpty(x.VALUE1) ? string.Empty : x.VALUE1 }).ToList();
                }
                var drp = (DropDownList)sender;
                drp.SetItem(lstSyukketsuKbn);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
        }

        /// <summary> 戻るボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                WorkType = null;
                Response.Redirect("WorkType.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 更新ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var btn = (Button)sender;
                var item = btn.GetParentItem();
                var shift = new M_WORK_SHIFT()
                {
                    WORK_TYPE = WorkType.Value,
                    SYUKKETSU_KBN = item.GetControl<DropDownList>("SYUKKETSU_KBN").SelectedValue,
                    START = item.GetControl<TextBox>("START").Text,
                    END = item.GetControl<TextBox>("END").Text,
                    REST = item.GetControl<TextBox>("REST").Text
                };
                var check = DataCheck(shift);
                if (!check.Result)
                {
                    Master.ShowAlart(check.Message, UpdatePanel1);
                    return;
                }
                var dba = new DbaWorkShift(Master.LoginEmployeeInfo.NO);
                dba.UpdateWorkShift(shift);

                ListView1.DataSource = dba.GetWorkShift(WorkType.Value);
                ListView1.DataBind();

                Master.ShowAlart("更新しました。", UpdatePanel1);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 削除ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var btn = (Button)sender;
                var item = btn.GetParentItem();
                var dba = new DbaWorkShift(Master.LoginEmployeeInfo.NO);
                dba.DeleteWorkShift(WorkType.Value, item.GetControl<DropDownList>("SYUKKETSU_KBN").SelectedValue);

                ListView1.DataSource = dba.GetWorkShift(WorkType.Value);
                ListView1.DataBind();

                Master.ShowAlart("削除しました。", UpdatePanel1);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 登録ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InsertBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var btn = (Button)sender;
                var item = btn.GetParentItem();
                var shift = new M_WORK_SHIFT()
                {
                    WORK_TYPE = WorkType.Value,
                    SYUKKETSU_KBN = item.GetControl<DropDownList>("SYUKKETSU_KBN").SelectedValue,
                    START = item.GetControl<TextBox>("START").Text,
                    END = item.GetControl<TextBox>("END").Text,
                    REST = item.GetControl<TextBox>("REST").Text
                };
                var check = DataCheck(shift);
                if (!check.Result)
                {
                    Master.ShowAlart(check.Message, UpdatePanel1);
                    return;
                }
                var dba = new DbaWorkShift(Master.LoginEmployeeInfo.NO);
                dba.InsertWorkShift(shift);

                ListView1.DataSource = dba.GetWorkShift(WorkType.Value);
                ListView1.DataBind();

                Master.ShowAlart("登録しました。", UpdatePanel1);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> データチェック & 整形
        /// </summary>
        /// <param name="shift"></param>
        /// <returns></returns>
        private ProcResult<M_WORK_SHIFT> DataCheck(M_WORK_SHIFT shift)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var res = new ProcResult<M_WORK_SHIFT>();
                var rest = new List<TimeSpan>();
                var zan = new List<TimeSpan>();

                // 出欠区分チェック
                if (shift.SYUKKETSU_KBN == SyukketsuKbn.EMPTY)
                {
                    res.SetResult(false, "出欠区分が選択されていません。", null);
                    return res;
                }
                // 開始チェック
                if (TimeSpan.TryParse(shift.START, out var start) == false)
                {
                    res.SetResult(false, "開始が正しい時刻形式ではありません。", null);
                    return res;
                }
                // 終了チェック
                if (TimeSpan.TryParse(shift.END, out var end) == false)
                {
                    res.SetResult(false, "終了が正しい時刻形式ではありません。", null);
                    return res;
                }
                // 休憩チェック
                foreach (var tempRest in shift.REST.Split('-'))
                {
                    if (TimeSpan.TryParse(tempRest, out var ts) == false)
                    {
                        res.SetResult(false, "休憩の一部に正しくない時刻形式があります。", null);
                        return res;
                    }
                    else
                    {
                        rest.Add(ts);
                    }
                }
                if (rest.Count % 2 != 0)
                {
                    res.SetResult(false, "休憩の一部に開始,終了の組が成立していない箇所があります。", null);
                    return res;
                }

                shift.START = start.ToStringEx("hh:mm");
                shift.END = end.ToStringEx("hh:mm");
                shift.REST = string.Join("-", rest.Select(x => x.ToStringEx("hh:mm")));

                res.Value = shift;
                return res;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }
    }
}