﻿using MySql.Data.MySqlClient;
using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PssWebKintai.DBA
{
    public class DbaKintai : DbaBase
    {
        public DbaKintai(string employeeNo) : base(employeeNo) { }
        #region 取得

        /// <summary> 勤怠情報取得
        /// </summary>
        /// <param name="employeeNo"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public T_KINTAI GetKintai(string employeeNo, int year, int month)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var ret = new T_KINTAI();
                var sql = new StringBuilder();
                var prm = new List<KeyValuePair<string, object>>();
                sql.AppendLine("SELECT");
                sql.AppendLine("  *");
                sql.AppendLine("FROM");
                sql.AppendLine("  {0}.T_KINTAI");
                sql.AppendLine("WHERE");
                sql.AppendLine("  EMPLOYEE_NO = ?pEMPLOYEE_NO");
                sql.AppendLine("  AND YEAR = ?pYEAR");
                sql.AppendLine("  AND MONTH = ?pMONTH");

                prm.Add(new KeyValuePair<string, object>("?pEMPLOYEE_NO", employeeNo));
                prm.Add(new KeyValuePair<string, object>("?pYEAR", year));
                prm.Add(new KeyValuePair<string, object>("?pMONTH", month));

                var res1 = GetTable<T_KINTAI>(sql, prm);
                if (res1.Count == 0)
                {
                    return null;
                }

                ret = res1[0];
                sql.Clear();
                prm.Clear();
                sql.AppendLine("SELECT");
                sql.AppendLine("  *");
                sql.AppendLine("FROM");
                sql.AppendLine("  {0}.T_KINTAI_DETAIL");
                sql.AppendLine("WHERE");
                sql.AppendLine("  EMPLOYEE_NO = ?pEMPLOYEE_NO");
                sql.AppendLine("  AND YEAR = ?pYEAR");
                sql.AppendLine("  AND MONTH = ?pMONTH");

                prm.Add(new KeyValuePair<string, object>("?pEMPLOYEE_NO", employeeNo));
                prm.Add(new KeyValuePair<string, object>("?pYEAR", year));
                prm.Add(new KeyValuePair<string, object>("?pMONTH", month));

                var res2 = GetTable<T_KINTAI_DETAIL>(sql, prm);
                if (res2.Count == 0)
                {
                    ret.DETAILS = new List<T_KINTAI_DETAIL>();
                }
                else
                {
                    ret.DETAILS = res2;
                }
                return ret;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        ///// <summary> 担当者,ステータスで該当する勤怠情報を取得する
        ///// </summary>
        ///// <param name="tantoNo"></param>
        ///// <param name="status"></param>
        ///// <returns></returns>
        //public List<T_KINTAI> GetKintaiList(string tantoNo, KintaiStatus status)
        //{
        //    try
        //    {
        //        PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
        //        var lstWhere = new List<Where>()
        //        {
        //            new Where("TANTO_NO", tantoNo),
        //            new Where("STATUS", (int)status)
        //        };
        //        return GetTable<T_KINTAI>(lstWhere);
        //    }
        //    catch (Exception ex)
        //    {
        //        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
        //    }
        //}

        public List<T_KINTAI> GetKintaiList(int year, int month)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var lstWhere = new List<Where>()
                {
                    new Where("YEAR", year),
                    new Where("MONTH", month)
                };
                return GetTable<T_KINTAI>(lstWhere).OrderBy(x => x.EMPLOYEE_NO).ToList();
                
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary>
        /// 承認依頼の勤怠リストを取得する
        /// </summary>
        /// <param name="tantoNo"></param>
        /// <returns></returns>
        public List<T_KINTAI> GetSyoninIraiKintaiList(string tantoNo)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var lstWhere = new List<Where>()
                {
                    new Where("TANTO_NO", tantoNo),
                    new Where("STATUS", KintaiStatus.SyoninIrai)
                };
                return GetTable<T_KINTAI>(lstWhere);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary>
        /// 承認済みの勤怠リストを取得する
        /// </summary>
        /// <returns></returns>
        public List<T_KINTAI> GetSyoninZumiKintaiList()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var lstWhere = new List<Where>()
                {
                    new Where("STATUS", KintaiStatus.SyoninZumi)
                };
                return GetTable<T_KINTAI>(lstWhere);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 確定済みの勤怠情報一覧を取得する
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public List<T_KINTAI> GetKakuteiKintaiList(int year, int month)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var lstWhere = new List<Where>()
                {
                    new Where("YEAR", year),
                    new Where("MONTH", month),
                    new Where("STATUS", (int)KintaiStatus.Kakutei)
                };
                var lstKintai = GetTable<T_KINTAI>(lstWhere);
                if (lstKintai.Count == 0)
                {
                    return lstKintai;
                }


                var lstPrmEmployee = lstKintai.Select(x => new KeyValuePair<string, object>("?E_NO_" + x.EMPLOYEE_NO, x.EMPLOYEE_NO));

                var sql = new StringBuilder();
                sql.AppendLine("SELECT");
                sql.AppendLine("    *");
                sql.AppendLine("FROM");
                sql.AppendLine("    {0}.T_KINTAI_DETAIL");
                sql.AppendLine("WHERE");
                sql.AppendLine("    YEAR = ?YEAR");
                sql.AppendLine("    AND MONTH = ?MONTH");
                sql.AppendLine("    AND EMPLOYEE_NO IN(");
                sql.AppendLine("        " + string.Join(",", lstPrmEmployee.Select(x => x.Key)));
                sql.AppendLine("    )");

                int prmNo = 0;
                var lstPrm = new List<KeyValuePair<string, object>>();
                lstPrm.Add(new KeyValuePair<string, object>("?YEAR", year));
                lstPrm.Add(new KeyValuePair<string, object>("?MONTH", month));
                lstPrm.AddRange(lstPrmEmployee);
                var lstKintaiDetail = GetTable<T_KINTAI_DETAIL>(sql, lstPrm);
                lstKintai.ForEach(kintai =>
                {
                    kintai.DETAILS.AddRange(lstKintaiDetail.Where(detail => kintai.EMPLOYEE_NO == detail.EMPLOYEE_NO &&
                                                                            kintai.YEAR == detail.YEAR &&
                                                                            kintai.MONTH == detail.MONTH)
                                                           .Select(detail => detail));
                });
                return lstKintai;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 夜間対応情報取得
        /// </summary>
        /// <param name="employeeNo"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public T_YAKAN GetYakan(string employeeNo, int year, int month)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                T_YAKAN ret = new T_YAKAN();
                var sql = new StringBuilder();
                var prm = new List<KeyValuePair<string, object>>();
                sql.AppendLine("SELECT");
                sql.AppendLine("  *");
                sql.AppendLine("FROM");
                sql.AppendLine("  {0}.T_YAKAN");
                sql.AppendLine("WHERE");
                sql.AppendLine("  EMPLOYEE_NO = ?pEMPLOYEE_NO");
                sql.AppendLine("  AND YEAR = ?pYEAR");
                sql.AppendLine("  AND MONTH = ?pMONTH");

                prm.Add(new KeyValuePair<string, object>("?pEMPLOYEE_NO", employeeNo));
                prm.Add(new KeyValuePair<string, object>("?pYEAR", year));
                prm.Add(new KeyValuePair<string, object>("?pMONTH", month));

                var res1 = GetTable<T_YAKAN>(sql, prm);
                if (res1.Count == 0)
                {
                    return null;
                }

                ret = res1[0];
                sql.Clear();
                prm.Clear();
                sql.AppendLine("SELECT");
                sql.AppendLine("  *");
                sql.AppendLine("FROM");
                sql.AppendLine("  {0}.T_YAKAN_DETAIL");
                sql.AppendLine("WHERE");
                sql.AppendLine("  EMPLOYEE_NO = ?pEMPLOYEE_NO");
                sql.AppendLine("  AND YEAR = ?pYEAR");
                sql.AppendLine("  AND MONTH = ?pMONTH");

                prm.Add(new KeyValuePair<string, object>("?pEMPLOYEE_NO", employeeNo));
                prm.Add(new KeyValuePair<string, object>("?pYEAR", year));
                prm.Add(new KeyValuePair<string, object>("?pMONTH", month));

                var res2 = GetTable<T_YAKAN_DETAIL>(sql, prm);
                if (res2.Count == 0)
                {
                    ret.DETAILS = new List<T_YAKAN_DETAIL>();
                }
                else
                {
                    ret.DETAILS = res2;
                }
                return ret;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }
        #endregion

        #region 勤怠情報登録更新

        /// <summary> 勤怠情報登録更新
        /// </summary>
        /// <param name="kintai"></param>
        /// <returns></returns>
        public bool RegistKintaiInfo(T_KINTAI kintai)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var connection = GetConnection())
                using (var transaction = connection.BeginTransaction())
                using (var com = GetCommand(connection, transaction))
                {
                    try
                    {
                        if (!RegistKintai(com, kintai))
                        {
                            transaction.Rollback();
                            return false;
                        }
                        if (!RegistKintaiDetail(com, kintai))
                        {
                            transaction.Rollback();
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                        transaction.Rollback();
                        return false;
                    }
                    transaction.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> t_kintai登録更新
        /// </summary>
        /// <param name="com"></param>
        /// <param name="kintai"></param>
        /// <returns></returns>
        private bool RegistKintai(MySqlCommand com, T_KINTAI kintai)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var sql = new StringBuilder();
                var lstPrm = new List<KeyValuePair<string, object>>();

                #region 登録SQL
                sql.AppendLine("INSERT INTO {0}.`T_KINTAI`(");
                sql.AppendLine("    `EMPLOYEE_NO`,");
                sql.AppendLine("    `YEAR`,");
                sql.AppendLine("    `MONTH`,");
                sql.AppendLine("    `NAME`,");
                sql.AppendLine("    `POST`,");
                sql.AppendLine("    `LANK`,");
                sql.AppendLine("    `WORK_TYPE`,");
                sql.AppendLine("    `PRICE`,");
                sql.AppendLine("    `RYOHI`,");
                sql.AppendLine("    `SHIKAKU_HOJO`,");
                sql.AppendLine("    `SHIKAKU_HOSYO`,");
                sql.AppendLine("    `SCHEDULED_WORKING_HOURS`,");
                sql.AppendLine("    `STATUS`,");
                sql.AppendLine("    `TANTO_NO`,");
                sql.AppendLine("    `SUM_JUNKEKKIN_KAI`,");
                sql.AppendLine("    `SUM_JUNKEKKIN_JIKAN`,");
                sql.AppendLine("    `SUM_JIMUSYONAI_JITSUDO`,");
                sql.AppendLine("    `SUM_YAKAN_TAIO`,");
                sql.AppendLine("    `SUM_GOKEI_JITSUDO`,");
                sql.AppendLine("    `SUM_ZAN_HUTU`,");
                sql.AppendLine("    `SUM_ZAN_SINYA`,");
                sql.AppendLine("    `SUM_KYUJITSU_KINMU`,");
                sql.AppendLine("    `SUM_YAKAN_TAIKI`,");
                sql.AppendLine("    `SUM_TETUYA_AKEKIN`,");
                sql.AppendLine("    `SUM_GAISYUTSU_KAI`,");
                sql.AppendLine("    `SUM_GAISYUTSU_JIKAN`,");
                sql.AppendLine("    `SUM_SINYA_JIKAN`,");
                sql.AppendLine("    `SUM_DENKO`,");
                sql.AppendLine("    `BIKO`");
                sql.AppendLine(")VALUES(");
                sql.AppendLine("    ?EMPLOYEE_NO,");
                sql.AppendLine("    ?YEAR,");
                sql.AppendLine("    ?MONTH,");
                sql.AppendLine("    ?NAME,");
                sql.AppendLine("    ?POST,");
                sql.AppendLine("    ?LANK,");
                sql.AppendLine("    ?WORK_TYPE,");
                sql.AppendLine("    ?PRICE,");
                sql.AppendLine("    ?RYOHI,");
                sql.AppendLine("    ?SHIKAKU_HOJO,");
                sql.AppendLine("    ?SHIKAKU_HOSYO,");
                sql.AppendLine("    ?SCHEDULED_WORKING_HOURS,");
                sql.AppendLine("    ?STATUS,");
                sql.AppendLine("    ?TANTO_NO,");
                sql.AppendLine("    ?SUM_JUNKEKKIN_KAI,");
                sql.AppendLine("    ?SUM_JUNKEKKIN_JIKAN,");
                sql.AppendLine("    ?SUM_JIMUSYONAI_JITSUDO,");
                sql.AppendLine("    ?SUM_YAKAN_TAIO,");
                sql.AppendLine("    ?SUM_GOKEI_JITSUDO,");
                sql.AppendLine("    ?SUM_ZAN_HUTU,");
                sql.AppendLine("    ?SUM_ZAN_SINYA,");
                sql.AppendLine("    ?SUM_KYUJITSU_KINMU,");
                sql.AppendLine("    ?SUM_YAKAN_TAIKI,");
                sql.AppendLine("    ?SUM_TETUYA_AKEKIN,");
                sql.AppendLine("    ?SUM_GAISYUTSU_KAI,");
                sql.AppendLine("    ?SUM_GAISYUTSU_JIKAN,");
                sql.AppendLine("    ?SUM_SINYA_JIKAN,");
                sql.AppendLine("    ?SUM_DENKO,");
                sql.AppendLine("    ?BIKO");
                sql.AppendLine(")");
                sql.AppendLine("ON DUPLICATE KEY UPDATE");
                sql.AppendLine("    `NAME` = ?NAME,");
                sql.AppendLine("    `POST` = ?POST,");
                sql.AppendLine("    `LANK` = ?LANK,");
                sql.AppendLine("    `WORK_TYPE` = ?WORK_TYPE,");
                sql.AppendLine("    `PRICE` = ?PRICE,");
                sql.AppendLine("    `RYOHI` = ?RYOHI,");
                sql.AppendLine("    `SHIKAKU_HOJO` = ?SHIKAKU_HOJO,");
                sql.AppendLine("    `SHIKAKU_HOSYO` = ?SHIKAKU_HOSYO,");
                sql.AppendLine("    `SCHEDULED_WORKING_HOURS` = ?SCHEDULED_WORKING_HOURS,");
                sql.AppendLine("    `STATUS` = ?STATUS,");
                sql.AppendLine("    `TANTO_NO` = ?TANTO_NO,");
                sql.AppendLine("    `SUM_JUNKEKKIN_KAI` = ?SUM_JUNKEKKIN_KAI,");
                sql.AppendLine("    `SUM_JUNKEKKIN_JIKAN` = ?SUM_JUNKEKKIN_JIKAN,");
                sql.AppendLine("    `SUM_JIMUSYONAI_JITSUDO` = ?SUM_JIMUSYONAI_JITSUDO,");
                sql.AppendLine("    `SUM_YAKAN_TAIO` = ?SUM_YAKAN_TAIO,");
                sql.AppendLine("    `SUM_GOKEI_JITSUDO` = ?SUM_GOKEI_JITSUDO,");
                sql.AppendLine("    `SUM_ZAN_HUTU` = ?SUM_ZAN_HUTU,");
                sql.AppendLine("    `SUM_ZAN_SINYA` = ?SUM_ZAN_SINYA,");
                sql.AppendLine("    `SUM_KYUJITSU_KINMU` = ?SUM_KYUJITSU_KINMU,");
                sql.AppendLine("    `SUM_YAKAN_TAIKI` = ?SUM_YAKAN_TAIKI,");
                sql.AppendLine("    `SUM_TETUYA_AKEKIN` = ?SUM_TETUYA_AKEKIN,");
                sql.AppendLine("    `SUM_GAISYUTSU_KAI` = ?SUM_GAISYUTSU_KAI,");
                sql.AppendLine("    `SUM_GAISYUTSU_JIKAN` = ?SUM_GAISYUTSU_JIKAN,");
                sql.AppendLine("    `SUM_SINYA_JIKAN` = ?SUM_SINYA_JIKAN,");
                sql.AppendLine("    `SUM_DENKO` = ?SUM_DENKO,");
                sql.AppendLine("    `BIKO` = ?BIKO");
                com.CommandText = string.Format(sql.ToString(), Schema);
                #endregion

                #region 登録パラメータ
                lstPrm.Add(new KeyValuePair<string, object>("?EMPLOYEE_NO", kintai.EMPLOYEE_NO));
                lstPrm.Add(new KeyValuePair<string, object>("?YEAR", kintai.YEAR));
                lstPrm.Add(new KeyValuePair<string, object>("?MONTH", kintai.MONTH));
                lstPrm.Add(new KeyValuePair<string, object>("?NAME", kintai.NAME));
                lstPrm.Add(new KeyValuePair<string, object>("?POST", kintai.POST));
                lstPrm.Add(new KeyValuePair<string, object>("?LANK", kintai.LANK));
                lstPrm.Add(new KeyValuePair<string, object>("?WORK_TYPE", kintai.WORK_TYPE));
                lstPrm.Add(new KeyValuePair<string, object>("?PRICE", kintai.PRICE));
                lstPrm.Add(new KeyValuePair<string, object>("?RYOHI", kintai.RYOHI));
                lstPrm.Add(new KeyValuePair<string, object>("?SHIKAKU_HOJO", kintai.SHIKAKU_HOJO));
                lstPrm.Add(new KeyValuePair<string, object>("?SHIKAKU_HOSYO", kintai.SHIKAKU_HOSYO));
                lstPrm.Add(new KeyValuePair<string, object>("?SCHEDULED_WORKING_HOURS", kintai.SCHEDULED_WORKING_HOURS));
                lstPrm.Add(new KeyValuePair<string, object>("?STATUS", kintai.STATUS));
                lstPrm.Add(new KeyValuePair<string, object>("?TANTO_NO", kintai.TANTO_NO));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_JUNKEKKIN_KAI", kintai.SUM_JUNKEKKIN_KAI));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_JUNKEKKIN_JIKAN", kintai.SUM_JUNKEKKIN_JIKAN));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_JIMUSYONAI_JITSUDO", kintai.SUM_JIMUSYONAI_JITSUDO));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_YAKAN_TAIO", kintai.SUM_YAKAN_TAIO));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_GOKEI_JITSUDO", kintai.SUM_GOKEI_JITSUDO));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_ZAN_HUTU", kintai.SUM_ZAN_HUTU));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_ZAN_SINYA", kintai.SUM_ZAN_SINYA));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_KYUJITSU_KINMU", kintai.SUM_KYUJITSU_KINMU));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_YAKAN_TAIKI", kintai.SUM_YAKAN_TAIKI));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_TETUYA_AKEKIN", kintai.SUM_TETUYA_AKEKIN));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_GAISYUTSU_KAI", kintai.SUM_GAISYUTSU_KAI));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_GAISYUTSU_JIKAN", kintai.SUM_GAISYUTSU_JIKAN));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_SINYA_JIKAN", kintai.SUM_SINYA_JIKAN));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_DENKO", kintai.SUM_DENKO));
                lstPrm.Add(new KeyValuePair<string, object>("?BIKO", kintai.BIKO));
                lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                #endregion

                com.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> t_kintai_detail登録更新
        /// </summary>
        /// <param name="com"></param>
        /// <param name="kintai"></param>
        /// <returns></returns>
        private bool RegistKintaiDetail(MySqlCommand com, T_KINTAI kintai)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var sql = new StringBuilder();
                var lstPrm = new List<KeyValuePair<string, object>>();
                var resCount = 0;

                #region 登録SQL
                sql.AppendLine("INSERT INTO {0}.`T_KINTAI_DETAIL`(");
                sql.AppendLine("    `EMPLOYEE_NO`,");
                sql.AppendLine("    `YEAR`,");
                sql.AppendLine("    `MONTH`,");
                sql.AppendLine("    `DAY`,");
                sql.AppendLine("    `KYUJITSU`,");
                sql.AppendLine("    `KINMU_KBN`,");
                sql.AppendLine("    `SYUKKETSU_KBN`,");
                sql.AppendLine("    `START`,");
                sql.AppendLine("    `END`,");
                sql.AppendLine("    `JUNKEKKIN_KAI`,");
                sql.AppendLine("    `JUNKEKKIN_JIKAN`,");
                sql.AppendLine("    `JIMUSYONAI_JITSUDO`,");
                sql.AppendLine("    `YAKAN_TAIO`,");
                sql.AppendLine("    `GOKEI_JITSUDO`,");
                sql.AppendLine("    `ZAN_HUTSU`,");
                sql.AppendLine("    `ZAN_SINYA`,");
                sql.AppendLine("    `KYUJITSU_KINMU`,");
                sql.AppendLine("    `YAKAN_TAIKI`,");
                sql.AppendLine("    `TETUYA_AKEKIN`,");
                sql.AppendLine("    `GAISYUTSU_KAI`,");
                sql.AppendLine("    `GAISYUTSU_JIKAN`,");
                sql.AppendLine("    `SINYA_JIKAN`,");
                sql.AppendLine("    `DENKO`,");
                sql.AppendLine("    `BIKO`,");
                sql.AppendLine("    `ERROR_TEXT`");
                sql.AppendLine(")VALUES(");
                sql.AppendLine("    ?EMPLOYEE_NO,");
                sql.AppendLine("    ?YEAR,");
                sql.AppendLine("    ?MONTH,");
                sql.AppendLine("    ?DAY,");
                sql.AppendLine("    ?KYUJITSU,");
                sql.AppendLine("    ?KINMU_KBN,");
                sql.AppendLine("    ?SYUKKETSU_KBN,");
                sql.AppendLine("    ?START,");
                sql.AppendLine("    ?END,");
                sql.AppendLine("    ?JUNKEKKIN_KAI,");
                sql.AppendLine("    ?JUNKEKKIN_JIKAN,");
                sql.AppendLine("    ?JIMUSYONAI_JITSUDO,");
                sql.AppendLine("    ?YAKAN_TAIO,");
                sql.AppendLine("    ?GOKEI_JITSUDO,");
                sql.AppendLine("    ?ZAN_HUTSU,");
                sql.AppendLine("    ?ZAN_SINYA,");
                sql.AppendLine("    ?KYUJITSU_KINMU,");
                sql.AppendLine("    ?YAKAN_TAIKI,");
                sql.AppendLine("    ?TETUYA_AKEKIN,");
                sql.AppendLine("    ?GAISYUTSU_KAI,");
                sql.AppendLine("    ?GAISYUTSU_JIKAN,");
                sql.AppendLine("    ?SINYA_JIKAN,");
                sql.AppendLine("    ?DENKO,");
                sql.AppendLine("    ?BIKO,");
                sql.AppendLine("    ?ERROR_TEXT)");
                sql.AppendLine("ON DUPLICATE KEY UPDATE");
                sql.AppendLine("    `KYUJITSU` = ?KYUJITSU,");
                sql.AppendLine("    `KINMU_KBN` = ?KINMU_KBN,");
                sql.AppendLine("    `SYUKKETSU_KBN` = ?SYUKKETSU_KBN,");
                sql.AppendLine("    `START` = ?START,");
                sql.AppendLine("    `END` = ?END,");
                sql.AppendLine("    `JUNKEKKIN_KAI` = ?JUNKEKKIN_KAI,");
                sql.AppendLine("    `JUNKEKKIN_JIKAN` = ?JUNKEKKIN_JIKAN,");
                sql.AppendLine("    `JIMUSYONAI_JITSUDO` = ?JIMUSYONAI_JITSUDO,");
                sql.AppendLine("    `YAKAN_TAIO` = ?YAKAN_TAIO,");
                sql.AppendLine("    `GOKEI_JITSUDO` = ?GOKEI_JITSUDO,");
                sql.AppendLine("    `ZAN_HUTSU` = ?ZAN_HUTSU,");
                sql.AppendLine("    `ZAN_SINYA` = ?ZAN_SINYA,");
                sql.AppendLine("    `KYUJITSU_KINMU` = ?KYUJITSU_KINMU,");
                sql.AppendLine("    `YAKAN_TAIKI` = ?YAKAN_TAIKI,");
                sql.AppendLine("    `TETUYA_AKEKIN` = ?TETUYA_AKEKIN,");
                sql.AppendLine("    `GAISYUTSU_KAI` = ?GAISYUTSU_KAI,");
                sql.AppendLine("    `GAISYUTSU_JIKAN` = ?GAISYUTSU_JIKAN,");
                sql.AppendLine("    `SINYA_JIKAN` = ?SINYA_JIKAN,");
                sql.AppendLine("    `DENKO` = ?DENKO,");
                sql.AppendLine("    `BIKO` = ?BIKO,");
                sql.AppendLine("    `ERROR_TEXT` = ?ERROR_TEXT");
                com.CommandText = string.Format(sql.ToString(), Schema);
                #endregion

                foreach (var detail in kintai.DETAILS)
                {
                    #region 登録パラメータ
                    com.Parameters.Clear();
                    lstPrm.Clear();
                    lstPrm.Add(new KeyValuePair<string, object>("?EMPLOYEE_NO", detail.EMPLOYEE_NO));
                    lstPrm.Add(new KeyValuePair<string, object>("?YEAR", detail.YEAR));
                    lstPrm.Add(new KeyValuePair<string, object>("?MONTH", detail.MONTH));
                    lstPrm.Add(new KeyValuePair<string, object>("?DAY", detail.DAY));
                    lstPrm.Add(new KeyValuePair<string, object>("?KYUJITSU", detail.KYUJITSU));
                    lstPrm.Add(new KeyValuePair<string, object>("?KINMU_KBN", detail.KINMU_KBN));
                    lstPrm.Add(new KeyValuePair<string, object>("?SYUKKETSU_KBN", detail.SYUKKETSU_KBN));
                    lstPrm.Add(new KeyValuePair<string, object>("?START", detail.START));
                    lstPrm.Add(new KeyValuePair<string, object>("?END", detail.END));
                    lstPrm.Add(new KeyValuePair<string, object>("?JUNKEKKIN_KAI", detail.JUNKEKKIN_KAI));
                    lstPrm.Add(new KeyValuePair<string, object>("?JUNKEKKIN_JIKAN", detail.JUNKEKKIN_JIKAN));
                    lstPrm.Add(new KeyValuePair<string, object>("?JIMUSYONAI_JITSUDO", detail.JIMUSYONAI_JITSUDO));
                    lstPrm.Add(new KeyValuePair<string, object>("?YAKAN_TAIO", detail.YAKAN_TAIO));
                    lstPrm.Add(new KeyValuePair<string, object>("?GOKEI_JITSUDO", detail.GOKEI_JITSUDO));
                    lstPrm.Add(new KeyValuePair<string, object>("?ZAN_HUTSU", detail.ZAN_HUTSU));
                    lstPrm.Add(new KeyValuePair<string, object>("?ZAN_SINYA", detail.ZAN_SINYA));
                    lstPrm.Add(new KeyValuePair<string, object>("?KYUJITSU_KINMU", detail.KYUJITSU_KINMU));
                    lstPrm.Add(new KeyValuePair<string, object>("?YAKAN_TAIKI", detail.YAKAN_TAIKI));
                    lstPrm.Add(new KeyValuePair<string, object>("?TETUYA_AKEKIN", detail.TETUYA_AKEKIN));
                    lstPrm.Add(new KeyValuePair<string, object>("?GAISYUTSU_KAI", detail.GAISYUTSU_KAI));
                    lstPrm.Add(new KeyValuePair<string, object>("?GAISYUTSU_JIKAN", detail.GAISYUTSU_JIKAN));
                    lstPrm.Add(new KeyValuePair<string, object>("?SINYA_JIKAN", detail.SINYA_JIKAN));
                    lstPrm.Add(new KeyValuePair<string, object>("?DENKO", detail.DENKO));
                    lstPrm.Add(new KeyValuePair<string, object>("?BIKO", detail.BIKO));
                    lstPrm.Add(new KeyValuePair<string, object>("?ERROR_TEXT", ""));
                    lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                    #endregion

                    com.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 勤怠情報のステータスと担当者の更新
        /// </summary>
        /// <param name="kintai"></param>
        /// <param name="tantoNo"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool UpdateStatus(T_KINTAI kintai, string tantoNo, KintaiStatus status)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();
                        var resCount = 0;

                        #region 登録SQL
                        sql.AppendLine("UPDATE {0}.`T_KINTAI` SET");
                        sql.AppendLine("    `STATUS` = ?STATUS,");
                        sql.AppendLine("    `TANTO_NO` = ?TANTO_NO");
                        sql.AppendLine("WHERE");
                        sql.AppendLine("    `EMPLOYEE_NO` = ?EMPLOYEE_NO");
                        sql.AppendLine("    AND `YEAR` = ?YEAR");
                        sql.AppendLine("    AND `MONTH` = ?MONTH");
                        com.CommandText = string.Format(sql.ToString(), Schema);
                        #endregion

                        #region 登録パラメータ
                        lstPrm.Add(new KeyValuePair<string, object>("?STATUS", (int)status));
                        lstPrm.Add(new KeyValuePair<string, object>("?TANTO_NO", tantoNo));
                        lstPrm.Add(new KeyValuePair<string, object>("?EMPLOYEE_NO", kintai.EMPLOYEE_NO));
                        lstPrm.Add(new KeyValuePair<string, object>("?YEAR", kintai.YEAR));
                        lstPrm.Add(new KeyValuePair<string, object>("?MONTH", kintai.MONTH));
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                        #endregion
                        resCount = com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                        tra.Rollback();
                        return false;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        public bool UpdateWorkType(T_KINTAI kintai)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();
                        var resCount = 0;

                        #region 登録SQL
                        sql.AppendLine("UPDATE {0}.`T_KINTAI` SET");
                        sql.AppendLine("    `NAME` = ?NAME");
                        sql.AppendLine("    , `POST` = ?POST");
                        sql.AppendLine("    , `LANK` = ?LANK");
                        sql.AppendLine("    , `WORK_TYPE` = ?WORK_TYPE");
                        sql.AppendLine("    , `PRICE` = ?PRICE");
                        sql.AppendLine("WHERE");
                        sql.AppendLine("    `EMPLOYEE_NO` = ?EMPLOYEE_NO");
                        sql.AppendLine("    AND `YEAR` = ?YEAR");
                        sql.AppendLine("    AND `MONTH` = ?MONTH");
                        com.CommandText = string.Format(sql.ToString(), Schema);
                        #endregion

                        #region 登録パラメータ
                        lstPrm.Add(new KeyValuePair<string, object>("?NAME", kintai.NAME));
                        lstPrm.Add(new KeyValuePair<string, object>("?POST", kintai.POST));
                        lstPrm.Add(new KeyValuePair<string, object>("?LANK", kintai.LANK));
                        lstPrm.Add(new KeyValuePair<string, object>("?WORK_TYPE", kintai.WORK_TYPE));
                        lstPrm.Add(new KeyValuePair<string, object>("?PRICE", kintai.PRICE));
                        lstPrm.Add(new KeyValuePair<string, object>("?EMPLOYEE_NO", kintai.EMPLOYEE_NO));
                        lstPrm.Add(new KeyValuePair<string, object>("?YEAR", kintai.YEAR));
                        lstPrm.Add(new KeyValuePair<string, object>("?MONTH", kintai.MONTH));
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                        #endregion
                        resCount = com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                        tra.Rollback();
                        return false;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        #endregion

        #region 自宅夜間対応情報登録更新

        /// <summary> 自宅夜間対応情報登録更新
        /// </summary>
        /// <param name="yakan"></param>
        /// <returns></returns>
        public bool RegistYakanInfo(T_YAKAN yakan)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var connection = GetConnection())
                using (var transaction = connection.BeginTransaction())
                using (var com = GetCommand(connection, transaction))
                {
                    try
                    {
                        if (!RegistYakan(com, yakan))
                        {
                            transaction.Rollback();
                            return false;
                        }
                        if (!RegistYakanDetail(com, yakan))
                        {
                            transaction.Rollback();
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                        transaction.Rollback();
                        return false;
                    }
                    transaction.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> t_yakan登録更新
        /// </summary>
        /// <param name="com"></param>
        /// <param name="yakan"></param>
        /// <returns></returns>
        private bool RegistYakan(MySqlCommand com, T_YAKAN yakan)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var sql = new StringBuilder();
                var lstPrm = new List<KeyValuePair<string, object>>();

                #region 登録SQL

                sql.AppendLine("INSERT INTO {0}.`T_YAKAN` (");
                sql.AppendLine("    `EMPLOYEE_NO`,");
                sql.AppendLine("    `YEAR`,");
                sql.AppendLine("    `MONTH`,");
                sql.AppendLine("    `SUM_TAIO_JIKAN`,");
                sql.AppendLine("    `SUM_SINYA_JIKAN`");
                sql.AppendLine(")VALUES(");
                sql.AppendLine("    ?EMPLOYEE_NO,");
                sql.AppendLine("    ?YEAR,");
                sql.AppendLine("    ?MONTH,");
                sql.AppendLine("    ?SUM_TAIO_JIKAN,");
                sql.AppendLine("    ?SUM_SINYA_JIKAN");
                sql.AppendLine(")");
                sql.AppendLine("ON DUPLICATE KEY UPDATE");
                sql.AppendLine("    `SUM_TAIO_JIKAN` = ?SUM_TAIO_JIKAN,");
                sql.AppendLine("    `SUM_SINYA_JIKAN` = ?SUM_SINYA_JIKAN");

                com.CommandText = string.Format(sql.ToString(), Schema);
                #endregion

                #region 登録パラメータ
                lstPrm.Add(new KeyValuePair<string, object>("?EMPLOYEE_NO", yakan.EMPLOYEE_NO));
                lstPrm.Add(new KeyValuePair<string, object>("?YEAR", yakan.YEAR));
                lstPrm.Add(new KeyValuePair<string, object>("?MONTH", yakan.MONTH));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_TAIO_JIKAN", yakan.SUM_TAIO_JIKAN));
                lstPrm.Add(new KeyValuePair<string, object>("?SUM_SINYA_JIKAN", yakan.SUM_SINYA_JIKAN));
                lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                #endregion

                com.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> t_yakan_detail登録更新
        /// </summary>
        /// <param name="com"></param>
        /// <param name="yakan"></param>
        /// <returns></returns>
        private bool RegistYakanDetail(MySqlCommand com, T_YAKAN yakan)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var sql = new StringBuilder();
                var lstPrm = new List<KeyValuePair<string, object>>();

                #region 削除
                sql.AppendLine("DELETE FROM {0}.`T_YAKAN_DETAIL`");
                sql.AppendLine("WHERE");
                sql.AppendLine("    `EMPLOYEE_NO` = ?EMPLOYEE_NO");
                sql.AppendLine("    AND `YEAR` = ?YEAR");
                sql.AppendLine("    AND `MONTH` = ?MONTH");
                com.CommandText = string.Format(sql.ToString(), Schema);

                com.Parameters.Clear();
                lstPrm.Add(new KeyValuePair<string, object>("?EMPLOYEE_NO", yakan.EMPLOYEE_NO));
                lstPrm.Add(new KeyValuePair<string, object>("?YEAR", yakan.YEAR));
                lstPrm.Add(new KeyValuePair<string, object>("?MONTH", yakan.MONTH));
                lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                com.ExecuteNonQuery();

                #endregion


                #region 登録SQL
                sql.Clear();
                sql.AppendLine("INSERT INTO {0}.`T_YAKAN_DETAIL` (");
                sql.AppendLine("    `EMPLOYEE_NO`,");
                sql.AppendLine("    `YEAR`,");
                sql.AppendLine("    `MONTH`,");
                sql.AppendLine("    `DAY`,");
                sql.AppendLine("    `START`,");
                sql.AppendLine("    `END`,");
                sql.AppendLine("    `TAIO_JIKAN`,");
                sql.AppendLine("    `SINYA_JIKAN`,");
                sql.AppendLine("    `INCIDENT_NO`");
                sql.AppendLine(")VALUES(");
                sql.AppendLine("    ?EMPLOYEE_NO,");
                sql.AppendLine("    ?YEAR,");
                sql.AppendLine("    ?MONTH,");
                sql.AppendLine("    ?DAY,");
                sql.AppendLine("    ?START,");
                sql.AppendLine("    ?END,");
                sql.AppendLine("    ?TAIO_JIKAN,");
                sql.AppendLine("    ?SINYA_JIKAN,");
                sql.AppendLine("    ?INCIDENT_NO");
                sql.AppendLine(")");

                com.CommandText = string.Format(sql.ToString(), Schema);
                #endregion

                foreach (var detail in yakan.DETAILS)
                {
                    #region 登録パラメータ
                    com.Parameters.Clear();
                    lstPrm.Clear();
                    lstPrm.Add(new KeyValuePair<string, object>("?EMPLOYEE_NO", detail.EMPLOYEE_NO));
                    lstPrm.Add(new KeyValuePair<string, object>("?YEAR", detail.YEAR));
                    lstPrm.Add(new KeyValuePair<string, object>("?MONTH", detail.MONTH));
                    lstPrm.Add(new KeyValuePair<string, object>("?DAY", detail.DAY));
                    lstPrm.Add(new KeyValuePair<string, object>("?START", detail.START));
                    lstPrm.Add(new KeyValuePair<string, object>("?END", detail.END));
                    lstPrm.Add(new KeyValuePair<string, object>("?TAIO_JIKAN", detail.TAIO_JIKAN));
                    lstPrm.Add(new KeyValuePair<string, object>("?SINYA_JIKAN", detail.SINYA_JIKAN));
                    lstPrm.Add(new KeyValuePair<string, object>("?INCIDENT_NO", detail.INCIDENT_NO));
                    lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                    #endregion

                    com.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }
        #endregion

    }
}