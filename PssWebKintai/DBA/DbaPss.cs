﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PssWebKintai.DBA
{
    public class DbaPss : DbaBase
    {
        public DbaPss() : base(string.Empty) { }

        /// <summary> 所属名取得
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string GetPostName(int code)
        {
            try
            {
                var where = new Where("CODE", code);
                var a = GetTable<M_POST>(where);
                return a.Count != 0 ? a[0].NAME : string.Empty;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 勤務形態名取得
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string GetWorkTypeName(int code)
        {
            try
            {
                var where = new Where("WORK_TYPE", code);
                var a = GetTable<M_WORK_TYPE>(where);
                return a.Count != 0 ? a[0].NAME : string.Empty;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 出欠区分一覧取得
        /// </summary>
        /// <returns></returns>
        public List<M_KBN> GetSyukketsuKbnList(bool isNeedNotDisplay = false)
        {
            try
            {
                var lstWhere = new List<Where>();
                lstWhere.Add(new Where("CODE", KBN.SyukketsuKbn));
                if (isNeedNotDisplay == false)
                {
                    lstWhere.Add(new Where("DISPLAY", 0));
                }
                return GetTable<M_KBN>(lstWhere).OrderBy(x => x.ORDER).ToList();
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 出欠区分一覧取得
        /// </summary>
        /// <returns></returns>
        public List<M_KBN> GetSyukketsuKbnList(string employeeNo)
        {
            try
            {
                var sb = new StringBuilder();
                sb.AppendLine("SELECT");
                sb.AppendLine("    KBN.*");
                sb.AppendLine("FROM");
                sb.AppendLine("    {0}.M_KBN KBN");
                sb.AppendLine("WHERE");
                sb.AppendLine("    KBN.CODE = ?pKBNCODE1");
                sb.AppendLine("    AND KBN.KEY1 IN(?pKey1, ?pKey2, ?pKey3, ?pKey4, ?pKey5, ?pKey6, ?pKey7, ?pKey8, ?pKey9, ?pKey10, ?pKey11)");
                sb.AppendLine("UNION SELECT");
                sb.AppendLine("    KBN.*");
                sb.AppendLine("FROM");
                sb.AppendLine("    {0}.M_KBN KBN");
                sb.AppendLine("    JOIN {0}.M_WORK_SHIFT WS");
                sb.AppendLine("        ON KBN.KEY1 = WS.SYUKKETSU_KBN");
                sb.AppendLine("    JOIN {0}.M_EMPLOYEE EM");
                sb.AppendLine("        ON WS.WORK_TYPE = EM.WORK_TYPE");
                sb.AppendLine("WHERE");
                sb.AppendLine("    KBN.CODE = ?pKBNCODE1");
                sb.AppendLine("    AND em.NO = ?pEmployeeNo");

                var lstPrm = new List<KeyValuePair<string, object>>();
                lstPrm.Add(new KeyValuePair<string, object>("?pKBNCODE1", KBN.SyukketsuKbn));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey1", SyukketsuKbn.EMPTY));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey2", SyukketsuKbn.SYUKKIN));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey3", SyukketsuKbn.YUKYU));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey4", SyukketsuKbn.HANKYU));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey5", SyukketsuKbn.DAIKYU));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey6", SyukketsuKbn.TOKUKYU));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey7", SyukketsuKbn.HANTOKU));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey8", SyukketsuKbn.KYUSYUTSU));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey9", SyukketsuKbn.KEKKIN));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey10", SyukketsuKbn.HAYADE));
                lstPrm.Add(new KeyValuePair<string, object>("?pKey11", SyukketsuKbn.KYUGYO));
                lstPrm.Add(new KeyValuePair<string, object>("?pEmployeeNo", employeeNo));


                return GetTable<M_KBN>(sb, lstPrm).OrderBy(x => x.ORDER).ToList();
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 夜間待機一覧取得
        /// </summary>
        /// <returns></returns>
        public List<M_KBN> GetYakanTaikiList()
        {
            try
            {
                var lstWhere = new List<Where>
                {
                    new Where("CODE", KBN.YakanTaiki),
                    new Where("DISPLAY", 0)
                };
                return GetTable<M_KBN>(lstWhere);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 主副TR一覧取得
        /// </summary>
        /// <returns></returns>
        public List<M_KBN> GetSFTRList()
        {
            try
            {
                var lstWhere = new List<Where>
                {
                    new Where("CODE", KBN.SFTR),
                    new Where("DISPLAY", 0)
                };
                return GetTable<M_KBN>(lstWhere);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 休み一覧取得
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private List<M_CALENDAR> GetCalendar(DateTime start, DateTime end)
        {
            try
            {
                var lstWhere = new List<Where>
                {
                    new Where("HOLIDAY", start, COMPARE_TYPE.LARGER_EQUAL),
                    new Where("HOLIDAY", end, COMPARE_TYPE.LOWER_EQUAL)
                };
                return GetTable<M_CALENDAR>(lstWhere);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 休み一覧取得(１か月分)
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public List<M_CALENDAR> GetCalendar(int year, int month)
        {
            try
            {
                var start = new DateTime(year, month, 1);
                var end = start.AddMonths(1).AddDays(-1);
                return GetCalendar(start, end);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 休み一覧取得(1年分)
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public List<M_CALENDAR> GetCalendar(int year)
        {
            try
            {
                var start = new DateTime(year, 4, 1);
                var end = start.AddYears(1).AddDays(-1);
                return GetCalendar(start, end);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }
    }
}