﻿using PssCommonLib;
using System.Configuration;

namespace PssWebKintai.DBA
{
    public class DbaBase : PssDbaBase
    {
        private static string Server { get { return ConfigurationManager.AppSettings["Server"]; } }
        private static string Database { get { return ConfigurationManager.AppSettings["Database"]; } }
        private static string Uid { get { return ConfigurationManager.AppSettings["Uid"]; } }
        private static string Pwd { get { return ConfigurationManager.AppSettings["Pwd"]; } }
        protected static string Schema { get { return ConfigurationManager.AppSettings["Schema"]; } }
        protected string EmployeeNo { get; set; }

        public DbaBase(string employeeNo) : base(Server, Database, Uid, Pwd, Schema)
        {
            EmployeeNo = employeeNo;
        }
    }
}