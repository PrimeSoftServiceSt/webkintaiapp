﻿using MySql.Data.MySqlClient;
using PssCommonLib;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace PssWebKintai.DBA
{
    /// <summary> DBA カレンダー
    /// </summary>
    public class DbaCalendar : DbaBase
    {
        public DbaCalendar(string employeeNo) : base(employeeNo) { }

        /// <summary> カレンダー登録
        /// </summary>
        /// <param name="year"></param>
        /// <param name="lstCalendar"></param>
        /// <returns></returns>
        public bool RegistCalendar(int year, List<M_CALENDAR> lstCalendar)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var connection = GetConnection())
                using (var transaction = connection.BeginTransaction())
                using (var com = GetCommand(connection, transaction))
                {
                    try
                    {
                        if (DeleteCalendar(com, year) == false)
                        {
                            transaction.Rollback();
                            return false;
                        }
                        if (InsertCalendar(com, lstCalendar) == false)
                        {
                            transaction.Rollback();
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                        return false;
                    }
                    transaction.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                return false;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> カレンダー削除
        /// </summary>
        /// <param name="com"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        private bool DeleteCalendar(MySqlCommand com, int year)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var sql = new StringBuilder();
                var lstPrm = new List<KeyValuePair<string, object>>();

                sql.AppendLine("DELETE FROM {0}.M_CALENDAR");
                sql.AppendLine("WHERE");
                sql.AppendLine("    HOLIDAY >= ?FROM");
                sql.AppendLine("    AND HOLIDAY <= ?TO");
                com.CommandText = string.Format(sql.ToString(), Schema);

                lstPrm.Add(new KeyValuePair<string, object>("?FROM", new DateTime(year, 4, 1)));
                lstPrm.Add(new KeyValuePair<string, object>("?TO", new DateTime(year + 1, 3, 31)));
                lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));

                com.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> カレンダー登録
        /// </summary>
        /// <param name="com"></param>
        /// <param name="lstCalendar"></param>
        /// <returns></returns>
        private bool InsertCalendar(MySqlCommand com, List<M_CALENDAR> lstCalendar)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var sql = new StringBuilder();
                var lstPrm = new List<KeyValuePair<string, object>>();

                sql.AppendLine("INSERT INTO {0}.M_CALENDAR(");
                sql.AppendLine("    HOLIDAY");
                sql.AppendLine(")VALUES(");
                sql.AppendLine("    ?HOLIDAY");
                sql.AppendLine(")");
                com.CommandText = string.Format(sql.ToString(), Schema);
                foreach (var calendar in lstCalendar)
                {
                    lstPrm.Clear();
                    com.Parameters.Clear();

                    lstPrm.Add(new KeyValuePair<string, object>("?HOLIDAY", calendar.HOLIDAY));
                    lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));

                    com.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }
    }
}