﻿using PssCommonLib;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PssWebKintai.DBA
{
    /// <summary> DBA 勤務形態情報
    /// </summary>
    public class DbaWorkShift : DbaBase
    {
        public DbaWorkShift(string employeeNo) : base(employeeNo) { }
        /// <summary> 勤務形態リスト取得
        /// </summary>
        /// <returns></returns>
        public List<M_WORK_TYPE> GetWorkTypeList()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                return GetTable<M_WORK_TYPE>().OrderBy(x => x.NAME).ToList();
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 勤務形態詳細リスト取得
        /// </summary>
        /// <param name="workType"></param>
        /// <returns></returns>
        public List<M_WORK_SHIFT> GetWorkShift(int workType)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                return GetTable<M_WORK_SHIFT>(new Where("WORK_TYPE", workType)).OrderBy(x => x.SYUKKETSU_KBN).ToList();
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 勤務形態登録
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool InsertWorkType(string name)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();

                        sql.AppendLine("INSERT INTO {0}.M_WORK_TYPE(");
                        sql.AppendLine("    NAME");
                        sql.AppendLine(")VALUES(");
                        sql.AppendLine("    ?NAME");
                        sql.AppendLine(")");

                        lstPrm.Add(new KeyValuePair<string, object>("?NAME", name));

                        com.CommandText = string.Format(sql.ToString(), Schema);
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));

                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                        tra.Rollback();
                        return false;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 勤務形態更新
        /// </summary>
        /// <param name="workType"></param>
        /// <returns></returns>
        public bool UpdateWorkType(M_WORK_TYPE workType)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();

                        sql.AppendLine("UPDATE {0}.M_WORK_TYPE SET");
                        sql.AppendLine("    NAME = ?NAME");
                        sql.AppendLine(" WHERE");
                        sql.AppendLine("    WORK_TYPE = ?WORK_TYPE");

                        lstPrm.Add(new KeyValuePair<string, object>("?NAME", workType.NAME));
                        lstPrm.Add(new KeyValuePair<string, object>("?WORK_TYPE", workType.WORK_TYPE));

                        com.CommandText = string.Format(sql.ToString(), Schema);
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));

                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                        tra.Rollback();
                        return false;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 勤務形態詳細登録
        /// </summary>
        /// <param name="workShift"></param>
        /// <returns></returns>
        public bool InsertWorkShift(M_WORK_SHIFT workShift)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();

                        sql.AppendLine("INSERT INTO {0}.M_WORK_SHIFT(");
                        sql.AppendLine("    WORK_TYPE,");
                        sql.AppendLine("    SYUKKETSU_KBN,");
                        sql.AppendLine("    START,");
                        sql.AppendLine("    END,");
                        sql.AppendLine("    REST");
                        sql.AppendLine(")VALUES(");
                        sql.AppendLine("    ?WORK_TYPE,");
                        sql.AppendLine("    ?SYUKKETSU_KBN,");
                        sql.AppendLine("    ?START,");
                        sql.AppendLine("    ?END,");
                        sql.AppendLine("    ?REST");
                        sql.AppendLine(")");

                        lstPrm.Add(new KeyValuePair<string, object>("?WORK_TYPE", workShift.WORK_TYPE));
                        lstPrm.Add(new KeyValuePair<string, object>("?SYUKKETSU_KBN", workShift.SYUKKETSU_KBN));
                        lstPrm.Add(new KeyValuePair<string, object>("?START", workShift.START));
                        lstPrm.Add(new KeyValuePair<string, object>("?END", workShift.END));
                        lstPrm.Add(new KeyValuePair<string, object>("?REST", workShift.REST));

                        com.CommandText = string.Format(sql.ToString(), Schema);
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));

                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                        tra.Rollback();
                        return false;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 勤務形態詳細更新
        /// </summary>
        /// <param name="workShift"></param>
        /// <returns></returns>
        public bool UpdateWorkShift(M_WORK_SHIFT workShift)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();

                        sql.AppendLine("UPDATE {0}.M_WORK_SHIFT SET");
                        sql.AppendLine("    START = ?START,");
                        sql.AppendLine("    END = ?END,");
                        sql.AppendLine("    REST = ?REST");
                        sql.AppendLine("WHERE");
                        sql.AppendLine("    WORK_TYPE = ?WORK_TYPE");
                        sql.AppendLine("    AND SYUKKETSU_KBN = ?SYUKKETSU_KBN");

                        lstPrm.Add(new KeyValuePair<string, object>("?WORK_TYPE", workShift.WORK_TYPE));
                        lstPrm.Add(new KeyValuePair<string, object>("?SYUKKETSU_KBN", workShift.SYUKKETSU_KBN));
                        lstPrm.Add(new KeyValuePair<string, object>("?START", workShift.START));
                        lstPrm.Add(new KeyValuePair<string, object>("?END", workShift.END));
                        lstPrm.Add(new KeyValuePair<string, object>("?REST", workShift.REST));

                        com.CommandText = string.Format(sql.ToString(), Schema);
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));

                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                        tra.Rollback();
                        return false;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 勤務形態詳細削除
        /// </summary>
        /// <param name="workType"></param>
        /// <param name="syukketsuKbn"></param>
        /// <returns></returns>
        public bool DeleteWorkShift(int workType, string syukketsuKbn)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();

                        sql.AppendLine("DELETE FROM {0}.M_WORK_SHIFT");
                        sql.AppendLine("WHERE");
                        sql.AppendLine("    WORK_TYPE = ?WORK_TYPE");
                        sql.AppendLine("    AND SYUKKETSU_KBN = ?SYUKKETSU_KBN");

                        lstPrm.Add(new KeyValuePair<string, object>("?WORK_TYPE", workType));
                        lstPrm.Add(new KeyValuePair<string, object>("?SYUKKETSU_KBN", syukketsuKbn));

                        com.CommandText = string.Format(sql.ToString(), Schema);
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));

                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                        tra.Rollback();
                        return false;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }
    }
}