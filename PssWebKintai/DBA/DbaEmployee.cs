﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace PssWebKintai.DBA
{
    /// <summary> DBA 社員情報
    /// </summary>
    public class DbaEmployee : DbaBase
    {
        public DbaEmployee(string employeeNo) : base(employeeNo) { }

        /// <summary> 社員情報取得
        /// </summary>
        /// <param name="employeeNo"></param>
        /// <returns></returns>
        public M_EMPLOYEE GetEmployee(string employeeNo)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var where = new Where("NO", employeeNo);
                var a = GetTable<M_EMPLOYEE>(where);
                return a.Count != 0 ? a[0] : null;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 社員情報リスト取得
        /// </summary>
        /// <param name="no"></param>
        /// <param name="name"></param>
        /// <param name="post"></param>
        /// <param name="workType"></param>
        /// <param name="superiorNo"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public List<M_EMPLOYEE> GetEmployeeList(string no = "", string name = "", string post = "", string workType = "", string superiorNo = "", string role = "")
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var sql = new StringBuilder();
                var lstPrm = new List<KeyValuePair<string, object>>();
                sql.AppendLine("SELECT");
                sql.AppendLine("    *");
                sql.AppendLine("FROM");
                sql.AppendLine("    {0}.M_EMPLOYEE");
                sql.AppendLine("WHERE 1 = 1");
                if (no != string.Empty)
                {
                    sql.AppendLine("    AND NO LIKE ?NO");
                    lstPrm.Add(new KeyValuePair<string, object>("?NO", $"{no}%"));
                }
                if (name != string.Empty)
                {
                    sql.AppendLine("    AND NAME LIKE ?NAME");
                    lstPrm.Add(new KeyValuePair<string, object>("?NAME", $"%{name}%"));
                }
                if (post != string.Empty && int.TryParse(post, out var temp))
                {
                    sql.AppendLine("    AND POST = ?POST");
                    lstPrm.Add(new KeyValuePair<string, object>("?POST", temp));
                }
                if (workType != string.Empty && int.TryParse(workType, out temp))
                {
                    sql.AppendLine("    AND WORK_TYPE = ?WORK_TYPE");
                    lstPrm.Add(new KeyValuePair<string, object>("?WORK_TYPE", temp));
                }
                if (superiorNo != string.Empty && int.TryParse(superiorNo, out temp))
                {
                    sql.AppendLine("    AND SUPERIOR_NO = ?SUPERIOR_NO");
                    lstPrm.Add(new KeyValuePair<string, object>("?SUPERIOR_NO", temp));
                }
                if (role != string.Empty && int.TryParse(role, out temp))
                {
                    sql.AppendLine("    AND KINTAI_KENGEN = ?KINTAI_KENGEN");
                    lstPrm.Add(new KeyValuePair<string, object>("?KINTAI_KENGEN", temp));
                }
                sql.AppendLine("ORDER BY");
                sql.AppendLine("    NO");

                return GetTable<M_EMPLOYEE>(sql, lstPrm);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 所属リスト取得
        /// </summary>
        /// <returns></returns>
        public IEnumerable<M_POST> GetPostList()
        {
            try
            {
                return GetTable<M_POST>().OrderBy(x => x.NAME);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 勤務形態リスト取得
        /// </summary>
        /// <returns></returns>
        public IEnumerable<M_WORK_TYPE> GetWorkTypeList()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                return GetTable<M_WORK_TYPE>().OrderBy(x => x.NAME);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 上長リスト取得
        /// </summary>
        /// <returns></returns>
        public IEnumerable<M_EMPLOYEE> GetSuperiorNoList()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var where = new Where("KINTAI_KENGEN", 0, COMPARE_TYPE.NOT_EQUAL);
                return GetTable<M_EMPLOYEE>(where).OrderBy(x => x.NAME);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 権限リスト取得
        /// </summary>
        /// <returns></returns>
        public IEnumerable<M_KBN> GetRoleList()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                var where = new Where("Code", KBN.Role);
                return GetTable<M_KBN>(where).OrderBy(x => x.KEY1);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 社員情報登録
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public bool InsertEmployee(M_EMPLOYEE employee)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();

                        #region 登録SQL
                        sql.AppendLine("INSERT INTO {0}.M_EMPLOYEE(");
                        sql.AppendLine("    NO,");
                        sql.AppendLine("    NAME,");
                        sql.AppendLine("    POST,");
                        sql.AppendLine("    LANK,");
                        sql.AppendLine("    WORK_TYPE,");
                        sql.AppendLine("    SUPERIOR_NO,");
                        sql.AppendLine("    PASSWORD,");
                        sql.AppendLine("    KINTAI_KENGEN");
                        sql.AppendLine(")VALUES(");
                        sql.AppendLine("    ?NO,");
                        sql.AppendLine("    ?NAME,");
                        sql.AppendLine("    ?POST,");
                        sql.AppendLine("    ?LANK,");
                        sql.AppendLine("    ?WORK_TYPE,");
                        sql.AppendLine("    ?SUPERIOR_NO,");
                        sql.AppendLine("    ?PASSWORD,");
                        sql.AppendLine("    ?KINTAI_KENGEN");
                        sql.AppendLine(")");
                        com.CommandText = string.Format(sql.ToString(), Schema);
                        #endregion

                        #region 登録パラメータ
                        lstPrm.Add(new KeyValuePair<string, object>("?NO", employee.NO));
                        lstPrm.Add(new KeyValuePair<string, object>("?NAME", employee.NAME));
                        lstPrm.Add(new KeyValuePair<string, object>("?POST", employee.POST));
                        lstPrm.Add(new KeyValuePair<string, object>("?LANK", employee.LANK));
                        lstPrm.Add(new KeyValuePair<string, object>("?WORK_TYPE", employee.WORK_TYPE));
                        lstPrm.Add(new KeyValuePair<string, object>("?SUPERIOR_NO", employee.SUPERIOR_NO));
                        lstPrm.Add(new KeyValuePair<string, object>("?PASSWORD", employee.NO));
                        lstPrm.Add(new KeyValuePair<string, object>("?KINTAI_KENGEN", employee.KINTAI_KENGEN));
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                        #endregion

                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        throw ex;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 社員情報更新
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public bool UpdateEmployee(M_EMPLOYEE employee)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();

                        #region 登録SQL
                        sql.AppendLine("UPDATE {0}.M_EMPLOYEE SET");
                        sql.AppendLine("    NAME = ?NAME,");
                        sql.AppendLine("    POST = ?POST,");
                        sql.AppendLine("    LANK = ?LANK,");
                        sql.AppendLine("    WORK_TYPE = ?WORK_TYPE,");
                        sql.AppendLine("    SUPERIOR_NO = ?SUPERIOR_NO,");
                        sql.AppendLine("    KINTAI_KENGEN = ?KINTAI_KENGEN");
                        sql.AppendLine("WHERE");
                        sql.AppendLine("    NO = ?NO");
                        com.CommandText = string.Format(sql.ToString(), Schema);
                        #endregion

                        #region 登録パラメータ
                        lstPrm.Add(new KeyValuePair<string, object>("?NAME", employee.NAME));
                        lstPrm.Add(new KeyValuePair<string, object>("?POST", employee.POST));
                        lstPrm.Add(new KeyValuePair<string, object>("?LANK", employee.LANK));
                        lstPrm.Add(new KeyValuePair<string, object>("?WORK_TYPE", employee.WORK_TYPE));
                        lstPrm.Add(new KeyValuePair<string, object>("?SUPERIOR_NO", employee.SUPERIOR_NO));
                        lstPrm.Add(new KeyValuePair<string, object>("?KINTAI_KENGEN", employee.KINTAI_KENGEN));
                        lstPrm.Add(new KeyValuePair<string, object>("?NO", employee.NO));
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                        #endregion

                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        throw ex;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 社員情報削除
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public bool DeleteEmployee(M_EMPLOYEE employee)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();

                        #region 登録SQL
                        sql.AppendLine("DELETE FROM {0}.M_EMPLOYEE");
                        sql.AppendLine("WHERE");
                        sql.AppendLine("    NO = ?NO");
                        com.CommandText = string.Format(sql.ToString(), Schema);
                        #endregion

                        #region 登録パラメータ
                        lstPrm.Add(new KeyValuePair<string, object>("?NO", employee.NO));
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                        #endregion

                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        throw ex;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> 単金更新
        /// </summary>
        /// <param name="employees"></param>
        /// <returns></returns>
        public bool UpdateTankin(List<M_EMPLOYEE> employees)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();

                        #region 登録SQL
                        sql.AppendLine("UPDATE {0}.M_EMPLOYEE SET");
                        sql.AppendLine("    PRICE = ?PRICE");
                        sql.AppendLine("WHERE");
                        sql.AppendLine("    NO = ?NO");
                        com.CommandText = string.Format(sql.ToString(), Schema);
                        #endregion

                        foreach (var employee in employees)
                        {
                            #region 登録パラメータ
                            com.Parameters.Clear();
                            lstPrm.Clear();
                            lstPrm.Add(new KeyValuePair<string, object>("?PRICE", employee.PRICE));
                            lstPrm.Add(new KeyValuePair<string, object>("?NO", employee.NO));
                            lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                            #endregion

                            com.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        return false;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }

        /// <summary> パスワード更新
        /// </summary>
        /// <param name="employeeNo"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool UpdatePassword(int employeeNo, string password)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), EmployeeNo);
                using (var con = GetConnection())
                using (var tra = con.BeginTransaction())
                using (var com = GetCommand(con, tra))
                {
                    try
                    {
                        var sql = new StringBuilder();
                        var lstPrm = new List<KeyValuePair<string, object>>();

                        #region 登録SQL
                        sql.AppendLine("UPDATE {0}.M_EMPLOYEE SET");
                        sql.AppendLine("    PASSWORD = ?PASSWORD");
                        sql.AppendLine("WHERE");
                        sql.AppendLine("    NO = ?NO");
                        com.CommandText = string.Format(sql.ToString(), Schema);
                        #endregion

                        #region 登録パラメータ
                        lstPrm.Add(new KeyValuePair<string, object>("?PASSWORD", password));
                        lstPrm.Add(new KeyValuePair<string, object>("?NO", employeeNo));
                        lstPrm.ForEach(x => com.Parameters.AddWithValue(x.Key, x.Value));
                        #endregion

                        com.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        tra.Rollback();
                        return false;
                    }
                    tra.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), EmployeeNo);
            }
        }
    }
}