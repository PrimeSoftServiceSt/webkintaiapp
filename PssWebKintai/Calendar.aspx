﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Calendar.aspx.cs" Inherits="PssWebKintai.Calendar" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:DropDownList ID="drpYear" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpYear_SelectedIndexChanged" />
    <asp:Label runat="server" Text="年度" /><br />
    <asp:Button ID="btnNewRow" runat="server" Text="新規" OnClick="btnNewRow_Click" />
    <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" />

    <%--   ヘッダー   --%>
    <div id="GridHeader">
        <table border="1">
            <tr class="tableHeader">
                <th runat="server">
                    <asp:Label Width="45px" runat="server" Style="text-align: center" Text="削除" /></th>
                <th runat="server">
                    <asp:Label Width="140px" runat="server" Style="text-align: center" Text="日付" /></th>
            </tr>
        </table>
    </div>

    <%--   ボディ   --%>
    <div id="GridBody" style="max-height: 350px; width: 204px">
        <asp:ListView ID="ListView1" runat="server">
            <LayoutTemplate>
                <table id="itemPlaceholderContainer" runat="server" border="1">
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr class='<%# Container.DisplayIndex % 2 == 0 ? "even" : "odd" %>' >
                    <td>
                        <asp:Button ID="DeleteBtn" runat="server" Text="削除" OnClick="DeleteBtn_Click" /></td>
                    <td>
                        <asp:TextBox ID="HOLIDAY" runat="server" TextMode="Date" Width="140px"
                            Text='<%# DateTime.TryParse(Eval("HOLIDAY").ToString(), out var dt) ? dt.ToString("yyyy-MM-dd") : string.Empty %>' /></td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>

    <asp:Button ID="btnBack" runat="server" Text="戻る" OnClick="btnBack_Click" />
</asp:Content>
