﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WorkType.aspx.cs" Inherits="PssWebKintai.WorkType" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%--   ヘッダー   --%>
            <div id="GridHeader">
                <table border="1">
                    <tr class="tableHeader">
                        <th>
                            <asp:Label Width="93px" runat="server" Style="text-align: center" Text="登録/詳細" /></th>
                        <th>
                            <asp:Label Width="200px" runat="server" Style="text-align: center" Text="名前" /></th>
                    </tr>
                </table>
            </div>

            <%--   ボディ   --%>
            <div id="GridBody" style="max-height: 300px; width: 313px">
                <asp:ListView ID="ListView1" runat="server" InsertItemPosition="LastItem">
                    <LayoutTemplate>
                        <table id="itemPlaceholderContainer" runat="server" border="1">
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class='<%# Container.DisplayIndex % 2 == 0 ? "even" : "odd" %>' >
                            <td>
                                <asp:Button ID="UpdateBtn" runat="server" Text="登録" OnClick="UpdateBtn_Click"/>
                                <asp:Button ID="DetailBtn" runat="server" Text="詳細" OnClick="DetailBtn_Click"/></td>
                            <td>
                                <asp:HiddenField ID="WORK_TYPE" runat="server" Value='<%# Eval("WORK_TYPE") %>'/>
                                <asp:TextBox Width="200px" ID="NAME" runat="server" Text='<%# Eval("NAME") %>' /></td>
                        </tr>
                    </ItemTemplate>
                    <InsertItemTemplate>
                        <tr class="odd">
                            <td>
                                <asp:Button ID="InsertBtn" runat="server" Text="登録" OnClick="InsertBtn_Click" />
                                <asp:Button ID="DetailBtn" runat="server" Text="詳細" Enabled="false"/></td>
                            <td>
                                <asp:TextBox Width="200px" ID="NAME" runat="server" /></td>
                        </tr>
                    </InsertItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
        <Triggers></Triggers>
    </asp:UpdatePanel>
    <asp:Button ID="btnBack" runat="server" Text="戻る" OnClick="btnBack_Click" />
</asp:Content>
