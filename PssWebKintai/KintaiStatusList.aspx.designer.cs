﻿//------------------------------------------------------------------------------
// <自動生成>
//     このコードはツールによって生成されました。
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。 
// </自動生成>
//------------------------------------------------------------------------------

namespace PssWebKintai
{


    public partial class KintaiStatusList
    {

        /// <summary>
        /// drpNengetsu コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList drpNengetsu;

        /// <summary>
        /// btnSearch コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSearch;

        /// <summary>
        /// ListView1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView ListView1;

        /// <summary>
        /// Master プロパティ。
        /// </summary>
        /// <remarks>
        /// 自動生成されたプロパティ。
        /// </remarks>
        public new PssWebKintai.SiteMaster Master
        {
            get
            {
                return ((PssWebKintai.SiteMaster)(base.Master));
            }
        }
    }
}
