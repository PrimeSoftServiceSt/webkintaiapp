﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.Entity;
using System;
using System.Linq;
using System.Reflection;

namespace PssWebKintai.Logic
{
    /// <summary> 自宅夜間対応計算用クラス
    /// </summary>
    public class KintaiCalcJitaku : KintaiCalc
    {
        #region プロパティ

        /// <summary> 自宅夜間対応表
        /// </summary>
        private T_YAKAN Yakan { get; set; }

        private int WorkType { get; set; }
        #endregion

        #region コンストラクタ

        /// <summary> コンストラクタ
        /// </summary>
        /// <param name="dgv1"></param>
        /// <param name="yakan"></param>
        /// <param name="yakan"></param>
        /// <param name="txt1"></param>
        /// <param name="txt2"></param>
        public KintaiCalcJitaku(T_YAKAN yakan, int workType) : base()
        {
            Yakan = yakan;
            WorkType = workType;
        }

        #endregion

        #region public メソッド

        /// <summary>自宅夜間対応計算
        /// </summary>
        public ProcResult<T_YAKAN> Calc_Jitaku()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                // 勤務形態取得
                base.Init_Time(WorkType);
                var wh = GetWorkingHours(SyukketsuKbn.SYUKKIN);

                if (wh == null)
                {
                    return new ProcResult<T_YAKAN>(false, "勤務形態情報が不足しています。", Yakan);
                }

                string strStartTime = string.Empty;     // 出勤時間 
                string strEndTime = string.Empty;      // 退勤時間
                string strTempDay = string.Empty;      // 月/日保存用

                // 月/日セルに何も入力されなくなるまで繰返す
                foreach (var yakanRecord in Yakan.DETAILS)
                {
                    // 初期化処理
                    TimeSpan overTime = TimeSpan.Zero;  // 使用しない
                    TimeSpan overTime2 = TimeSpan.Zero;  // 深夜残業時間
                    TimeSpan workTime = TimeSpan.Zero;   // 実働時間

                    // 計算項目初期化
                    yakanRecord.TAIO_JIKAN = string.Empty;
                    yakanRecord.SINYA_JIKAN = string.Empty;

                    // 月/日の保存
                    strTempDay = yakanRecord.DAY.ToString();

                    // 入力値の取得
                    strStartTime = yakanRecord.START;   // 対応開始時間
                    strEndTime = yakanRecord.END;       // 対応終了時間

                    // 対応時間の入力チェック
                    if (strStartTime != string.Empty && strEndTime != string.Empty)
                    {
                        // 対応開始、終了時間の調整
                        // 開始より終了が小さいときは日付をまたいで勤務
                        if (strStartTime.ToDateTime() > strEndTime.ToDateTime())
                        {
                            // 次の日の終了
                            strEndTime = DAY_SECOND + strEndTime;
                        }
                        else
                        {
                            // 同一日の終了
                            strEndTime = DAY_FIRST + strEndTime;
                        }
                        strStartTime = DAY_FIRST + strStartTime;

                        // 残業時間の取得
                        overTime2 = CalcJikangaiShinya(strStartTime, strEndTime, wh, (int)WorkStyle.NR);
                        //// 日付またぎの場合
                        //if (strEndTime.ToDateTime().Day == 2)
                        //{
                        //    // 残業時間の計算(当日分)
                        //    // ※日付またぎの場合は23時59分～0時00分の間の1分を加算
                        //    CalcJikangaiHutsu(strStartTime, DAY_FIRST + "23:59", "00:01", wh, overTime, overTime2, DAY_FIRST)
                        //        .ToTuple()
                        //        .Deconstruct(out overTime, out overTime2);

                        //    // 残業時間の計算(翌日分)
                        //    CalcJikangaiHutsu(DAY_SECOND + "00:00", strEndTime, string.Empty, wh, overTime, overTime2, DAY_SECOND)
                        //        .ToTuple()
                        //        .Deconstruct(out overTime, out overTime2);
                        //}
                        //else if ((DAY_FIRST + " 00:00").ToDateTime() <= strStartTime.ToDateTime() && strEndTime.ToDateTime() <= (DAY_FIRST + wh.SigyoTime).ToDateTime())
                        //{
                        //    // 残業時間の計算(翌日分)
                        //    strStartTime = strStartTime.ToDateTime().AddDays(1).ToString("yyyy/MM/dd HH:mm");
                        //    strEndTime = strEndTime.ToDateTime().AddDays(1).ToString("yyyy/MM/dd HH:mm");
                        //    CalcJikangaiHutsu(strStartTime, strEndTime, string.Empty, wh, overTime, overTime2, DAY_SECOND)
                        //        .ToTuple()
                        //        .Deconstruct(out overTime, out overTime2);
                        //}
                        //else
                        //{
                        //    // 残業時間の計算
                        //    CalcJikangaiHutsu(strStartTime, strEndTime, string.Empty, wh, overTime, overTime2, DAY_FIRST)
                        //        .ToTuple()
                        //        .Deconstruct(out overTime, out overTime2);
                        //}

                        // 実働時間の計算
                        if (strEndTime.ToDateTime() > strStartTime.ToDateTime())
                        {
                            workTime = strEndTime.ToDateTime() - strStartTime.ToDateTime();
                            yakanRecord.TAIO_JIKAN = workTime.ToString(@"hh\:mm");
                        }
                        else
                        {
                            workTime = TimeSpan.Zero;
                        }

                        // 深夜残業時間の貼付け
                        if (overTime2 > TimeSpan.Zero)
                        {
                            yakanRecord.SINYA_JIKAN = overTime2.ToString(@"hh\:mm");
                        }
                    }
                }

                // 夜間対応時間合計取得
                Yakan.SUM_TAIO_JIKAN = Yakan.DETAILS.Select(x => x.TAIO_JIKAN.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();
                Yakan.SUM_SINYA_JIKAN = Yakan.DETAILS.Select(x => x.SINYA_JIKAN.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();

                return new ProcResult<T_YAKAN>(t: Yakan);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }
        #endregion
    }
}
