﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.DBA;
using PssWebKintai.Entity;
using PssWebKintai.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PssWebKintai.Logic
{
    /// <summary> 勤務時間計算クラス
    /// </summary>
    public class KintaiCalc
    {
        #region 定数・プロパティ・変数

        /// <summary> エラーテキスト
        /// </summary>
        protected const string ERROR_TEXT = "error!!";

        /// <summary> 1900/1/1
        /// </summary>
        protected const string DAY_FIRST = "1900/1/1 ";

        /// <summary> 1900/1/2
        /// </summary>
        protected const string DAY_SECOND = "1900/1/2 ";

        protected const string DAY_START = "00:00";
        protected const string DAY_END = "23:59";

        protected const string SINYA_START = "22:00";
        protected const string SINYA_START_TORASO = "22:30";
        protected const string SINYA_END = "05:00";

        /// <summary> 勤務時間基本情報リスト
        /// </summary>
        protected List<WorkingHoursBaseInfo> WorkingHoursList = new List<WorkingHoursBaseInfo>();

        #endregion

        #region コンストラクタ

        /// <summary> コンストラクタ
        /// </summary>
        /// <param name="kintai"></param>
        /// <param name="yakan"></param>
        public KintaiCalc()
        {

        }

        #endregion

        #region protected メソッド

        /// <summary> 勤務時間取得
        /// </summary>
        protected void Init_Time(int workType)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                var dba = new DbaPss();
                var where = new Where("WORK_TYPE", workType);
                var lstWorkShift = dba.GetTable<M_WORK_SHIFT>(where);
                foreach (var item in lstWorkShift)
                {
                    var workingHoursInfo = new WorkingHoursBaseInfo
                    {
                        ShukketuKbn = item.SYUKKETSU_KBN,
                        SigyoTime = item.START,
                        SyugyoTime = item.END
                    };

                    var s = workingHoursInfo.SigyoTime.ToTimeSpan();    // 始業
                    var e = workingHoursInfo.SyugyoTime.ToTimeSpan();    // 終業

                    // 休憩時間の計算
                    var lstRest = item.REST.Split('-').Select(x => x.ToTimeSpan()).ToList();
                    lstRest.Add(DAY_END.ToTimeSpan());
                    lstRest.Add(DAY_END.ToTimeSpan());
                    lstRest.Add(DAY_START.ToTimeSpan());
                    lstRest.Add(DAY_START.ToTimeSpan());
                    lstRest.Add(SINYA_END.ToTimeSpan());
                    lstRest.Add(SINYA_END.ToTimeSpan());
                    workingHoursInfo.RestTime = SortTimeFromSigyoTime(s, lstRest)
                                                    .Select(x => x.ToStringEx("hh:mm"))
                                                    .ToArray();

                    // 残業時間の計算
                    var lstNotZan = new List<TimeSpan> { s, DAY_END.ToTimeSpan(), DAY_START.ToTimeSpan(), e };  // ①
                    foreach (var strRest in workingHoursInfo.RestTime)
                    {
                        var rest = strRest.ToTimeSpan();
                        if (rest <= s || e <= rest)
                        {
                            // 定時外の休憩時間を①に追加
                            lstNotZan.Add(rest);
                        }
                    }
                    workingHoursInfo.ZanTime = SortTimeFromSigyoTime(s, lstNotZan)
                                                    .Select(x => x.ToStringEx("hh:mm"))
                                                    .ToArray();

                    WorkingHoursList.Add(workingHoursInfo);
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }

        /// <summary>
        /// 始業時間を起点とした時間のソートを行う
        /// </summary>
        /// <param name="sigyoTime"></param>
        /// <param name="times"></param>
        /// <returns></returns>
        private List<TimeSpan> SortTimeFromSigyoTime(TimeSpan sigyoTime, List<TimeSpan> times)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                var temp = times.OrderBy(x => x);                   // 昇順で並べ替え
                var after = temp.SkipWhile(x => sigyoTime >= x);    // 始業より後の時間
                var before = temp.TakeWhile(x => sigyoTime >= x);   // 始業以前の時間

                var ret = new List<TimeSpan>();
                ret.AddRange(after);
                ret.AddRange(before);
                return ret;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }

        /// <summary> 出欠区分から対応する勤務形態詳細を取得する
        /// </summary>
        /// <param name="syukketsuKbn"></param>
        /// <returns></returns>
        protected WorkingHoursBaseInfo GetWorkingHours(string syukketsuKbn)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                return WorkingHoursList.FirstOrDefault(x => x.ShukketuKbn == syukketsuKbn);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }

        /// <summary> 退勤時の日付を取得する
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        /// <remarks>出勤時の日付を1900/1/1とし、同一日の退勤か次の日の退勤か</remarks>
        protected string SetDateEnd(string start, string end)
        {
            try
            {
                // 24時間以上の勤務は無しとする
                // (出勤より退勤が小さい)ときは日付をまたいで勤務
                if (start.ToDateTime() > end.ToDateTime())
                {
                    // 次の日の終了
                    return DAY_SECOND;
                }
                else
                {
                    // 同一日の退勤
                    return DAY_FIRST;
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 残業時間計算
        /// </summary>
        /// <param name="strStartTime"></param>
        /// <param name="strEndTime"></param>
        /// <param name="stradjust"></param>
        /// <param name="wh"></param>
        /// <param name="overTime"></param>
        /// <param name="strTemp"></param>
        protected TimeSpan CalcJikangaiHutsu(string strStartTime, string strEndTime, string stradjust, WorkingHoursBaseInfo wh, TimeSpan overTime, string strTemp)
        {
            try
            {
                for (int i = 0; i + 1 <= wh.ZanTime.Length - 1; i = i + 2)
                {
                    string tempS = (wh.ZanTime[i].ToTimeSpan() <= wh.SigyoTime.ToTimeSpan() ? DAY_SECOND : strTemp) + wh.ZanTime[i];
                    string tempE = (wh.ZanTime[i + 1].ToTimeSpan() <= wh.SigyoTime.ToTimeSpan() ? DAY_SECOND : strTemp) + wh.ZanTime[i + 1];

                    // 休憩の開始,終了が同じ時刻の場合、計算しない
                    if (tempS == tempE) continue;

                    // 退勤が休憩開始より早い場合、計算しない
                    if (strEndTime.ToDateTime() <= tempS.ToDateTime()) continue;

                    // 休憩終了が出勤より早い場合、計算しない
                    if (tempE.ToDateTime() <= strStartTime.ToDateTime()) continue;

                    // 残業算出開始時間の設定
                    string strStartWork;
                    if (tempS.ToDateTime() < strStartTime.ToDateTime())
                    {
                        strStartWork = strStartTime;
                    }
                    else
                    {
                        strStartWork = tempS;
                    }

                    // 残業算出終了時間の設定
                    string strEndWork;
                    if (strEndTime.ToDateTime() <= tempE.ToDateTime())
                    {
                        strEndWork = strEndTime;
                    }
                    else
                    {
                        strEndWork = tempE;
                    }

                    // 残業時間計算
                    TimeSpan ts = strEndWork.ToDateTime() - strStartWork.ToDateTime();
                    overTime += ts;

                    if (tempE == strTemp + DAY_END && stradjust != string.Empty)
                    {
                        // 日付またぎの場合は23:59から00:00までの1分を加算
                        overTime += TimeSpan.Parse(stradjust);
                        strTemp = DAY_SECOND;
                    }
                }
                return overTime;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 時間外深夜を計算する
        /// </summary>
        /// <param name="strStartTime"></param>
        /// <param name="strEndTime"></param>
        /// <param name="wh"></param>
        /// <param name="workType"></param>
        /// <param name="yakanTaio"></param>
        /// <returns></returns>
        protected TimeSpan CalcJikangaiShinya(string strStartTime, string strEndTime, WorkingHoursBaseInfo wh, int workType)
        {
            try
            {
                var jikangaiSinya = TimeSpan.Zero;                          // 時間外深夜
                var sinyaStart = (DAY_FIRST + SINYA_START).ToDateTime();    // 深夜開始時間
                var sinyaEnd = (DAY_SECOND + SINYA_END).ToDateTime();       // 深夜終了時間
                var start = strStartTime.ToDateTime();                      // 勤務開始時間
                var end = strEndTime.ToDateTime();                          // 勤務終了時間

                if (workType != (int)WorkStyle.Attend && wh.ShukketuKbn == SyukketsuKbn.TORASO)
                {
                    // 勤務形態がアテンド以外 かつ 出欠区分がトラ相の場合
                    sinyaStart = (DAY_FIRST + SINYA_START_TORASO).ToDateTime(); // 深夜開始を上書き
                }

                if (start > sinyaEnd || end < sinyaStart)
                {
                    // 開始が深夜終了より遅い 又は 終了が深夜開始より早い場合
                    return TimeSpan.Zero;
                }

                if (start < sinyaStart)
                {
                    // 開始が深夜開始より早い場合
                    start = sinyaStart; // 深夜開始を開始とする
                }
                if (end > sinyaEnd)
                {
                    // 終了が深夜終了より遅い場合
                    end = sinyaEnd;     // 深夜終了を終了とする
                }

                jikangaiSinya = end - start;   // 終了 - 開始 を時間外深夜とする
                if (workType == (int)WorkStyle.Attend)
                {
                    // 勤務形態がアテンドの場合
                    return jikangaiSinya;
                }
                else
                {
                    // 勤務形態がその他の場合
                    for (int i = 0; i + 1 <= wh.RestTime.Length - 1; i = i + 2)
                    {
                        string tempS = (wh.RestTime[i].ToTimeSpan() <= wh.SigyoTime.ToTimeSpan() ? DAY_SECOND : DAY_FIRST) + wh.RestTime[i];
                        string tempE = (wh.RestTime[i + 1].ToTimeSpan() <= wh.SigyoTime.ToTimeSpan() ? DAY_SECOND : DAY_FIRST) + wh.RestTime[i + 1];
                        if (tempS == tempE) continue;

                        var restS = tempS.ToDateTime();
                        var restE = tempE.ToDateTime();

                        if (start <= restS && restE <= end)
                        {
                            // 時間外深夜中に休憩が含まれている場合
                            jikangaiSinya -= restE - restS; // 休憩時間を差し引く
                        }
                    }


                    if (workType == (int)WorkStyle.LCM && wh.ShukketuKbn == SyukketsuKbn.YAKIN)
                    {
                        // 勤務形態がLCM かつ 出欠区分が夜間対応の場合、固定で1.5h差し引く
                        jikangaiSinya -= new TimeSpan(1, 30, 0);

                    }

                    if (jikangaiSinya < TimeSpan.Zero)
                    {
                        jikangaiSinya = TimeSpan.Zero;
                    }
                    return jikangaiSinya;
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        #endregion
    }
}