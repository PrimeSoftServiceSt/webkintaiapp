﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.Entity;
using PssWebKintai.Model;
using System;
using System.Linq;
using System.Reflection;

namespace PssWebKintai.Logic
{
    public class KintaiCalcMain : KintaiCalc
    {
        /// <summary> 勤怠表
        /// </summary>
        protected T_KINTAI Kintai { get; set; }

        /// <summary> 自宅夜間対応テーブル
        /// </summary>
        protected T_YAKAN Yakan { get; set; }

        public KintaiCalcMain(T_KINTAI kintai, T_YAKAN yakan)
        {
            Kintai = kintai;
            Yakan = yakan;
        }


        /// <summary> 勤務時間計算
        /// </summary>
        public T_KINTAI Calc()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
                // グリッド計算項目初期化
                DeleteCalcResult();

                // 勤務時間取得
                Init_Time(Kintai.WORK_TYPE);

                if (Kintai.WORK_TYPE == (int)WorkStyle.Attend)
                {
                    // アテンドの場合
                    CalcAttend();
                }
                else
                {
                    // アテンド以外の場合
                    CalcMain();
                }

                return CalcSum();
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
            }
        }

        /// <summary> 計算項目削除
        /// </summary>
        private void DeleteCalcResult()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
                // グリッド計算項目初期化
                foreach (var record in Kintai.DETAILS)
                {
                    record.JIMUSYONAI_JITSUDO = string.Empty;
                    record.ERROR_TEXT = string.Empty;
                    record.GOKEI_JITSUDO = string.Empty;
                    record.ZAN_HUTSU = string.Empty;
                    record.KYUJITSU_KINMU = string.Empty;
                    if (Kintai.WORK_TYPE != (int)WorkStyle.Attend)
                    {
                        // アテンド以外の場合初期化
                        record.YAKAN_TAIO = string.Empty;
                        record.ZAN_SINYA = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
            }
        }

        /// <summary> アテンド勤務時間計算
        /// </summary>
        private void CalcAttend()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
                // 出勤・退勤が入力されているものを抽出
                foreach (var record in Kintai.DETAILS.Where(grow => grow.START != string.Empty && grow.END != string.Empty))
                {
                    // todo 出退勤にDAY_FIRSTをつける
                    DateTime dtStartTime = (DAY_FIRST + record.START).ToDateTime();   // 出勤時間
                    DateTime dtEndTime = (DAY_FIRST + record.END).ToDateTime();       // 退勤時間
                    TimeSpan restTime = record.YAKAN_TAIO.ToTimeSpan(); // 休憩時間
                    TimeSpan workTime = TimeSpan.Zero;                  // 実働時間

                    // 実働時間の計算
                    if (dtEndTime < dtStartTime)
                    {
                        dtEndTime = dtEndTime.AddDays(1);
                    }
                    workTime = dtEndTime - dtStartTime;

                    // 休憩前労働時間の貼付け
                    record.JIMUSYONAI_JITSUDO = workTime.ToString(@"hh\:mm");

                    // 休憩時間計算
                    if (restTime == TimeSpan.Zero)
                    {
                        var decWorkTime = workTime.GetDecimalTotalHours();
                        if (decWorkTime <= 4m)
                        {
                            //4ｈ以下 0ｈ
                            restTime = TimeSpan.Zero;
                        }
                        else if (decWorkTime <= 5m)
                        {
                            //4ｈを超え、5ｈまで 0～1ｈ
                            restTime = workTime - new TimeSpan(4, 0, 0);
                        }
                        else if (decWorkTime <= 9m)
                        {
                            //5ｈを越え9ｈまで 1ｈ
                            restTime = new TimeSpan(1, 0, 0);
                        }
                        else if (decWorkTime <= 9.5m)
                        {
                            //9ｈを超え、9.5ｈまで                  1～1.5ｈ
                            restTime = workTime - new TimeSpan(9, 0, 0) + new TimeSpan(1, 0, 0);
                        }
                        else if (decWorkTime <= 13m)
                        {
                            //9.5ｈを超え、13ｈまで                1.5ｈ
                            restTime = new TimeSpan(1, 30, 0);
                        }
                        else if (decWorkTime <= 13.5m)
                        {
                            //13ｈを超え、13.5ｈまで              1.5～2ｈ
                            restTime = workTime - new TimeSpan(13, 0, 0) + new TimeSpan(1, 30, 0);
                        }
                        else if (decWorkTime <= 16m)
                        {
                            //13.5ｈを超え、16ｈまで              2ｈ
                            restTime = new TimeSpan(2, 0, 0);
                        }
                    }
                    if (workTime > restTime)
                    {
                        // 休憩時間の除外
                        workTime -= restTime;
                    }
                    else
                    {
                        workTime = TimeSpan.Zero;
                    }

                    // 休憩時間
                    record.YAKAN_TAIO = restTime.ToString(@"hh\:mm");

                    // 合計実働時間の計算
                    record.GOKEI_JITSUDO = workTime.ToString(@"hh\:mm");

                    // 時間外深夜
                    record.ZAN_SINYA = CalcJikangaiShinya(dtStartTime.ToString("yyyy/MM/dd HH:mm"), dtEndTime.ToString("yyyy/MM/dd HH:mm"), null, (int)WorkStyle.Attend).ToString(@"hh\:mm");

                    if (record.KYUJITSU != string.Empty)
                    {
                        record.KYUJITSU_KINMU = record.GOKEI_JITSUDO;
                    }
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
            }
        }

        /// <summary>
        /// 勤務時間計算
        /// </summary>
        private void CalcMain()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
                if (Kintai.WORK_TYPE == (int)WorkStyle.NR)
                {
                    // NRの場合,自宅夜間対応計算
                    SetYakinToKintai();
                }
                // 出勤・退勤が入力されている or 夜間対応が入力されている
                foreach (var record in Kintai.DETAILS.Where(r => (r.START != string.Empty && r.END != string.Empty)
                                                               || r.YAKAN_TAIO != string.Empty))
                {
                    string strStartTime = string.Empty;     // 出勤時間
                    string strEndTime = string.Empty;       // 退勤時間
                    TimeSpan workTime = TimeSpan.Zero;      // 実働時間
                    TimeSpan overTime = TimeSpan.Zero;      // 残業時間(普通)
                    TimeSpan overTime2 = TimeSpan.Zero;     // 残業時間(深夜)

                    if (record.START != string.Empty && record.END != string.Empty)
                    {
                        // 出欠区分に対応する勤務時間基本情報を取得する
                        var kbn = string.Empty;
                        switch (record.SYUKKETSU_KBN)
                        {
                            case SyukketsuKbn.HANKYU:
                            case SyukketsuKbn.HANTOKU:
                            case SyukketsuKbn.KYUSYUTSU:
                                kbn = SyukketsuKbn.SYUKKIN;
                                break;
                            default:
                                kbn = record.SYUKKETSU_KBN;
                                break;
                        }
                        var wh = GetWorkingHours(kbn);
                        if (wh != null)
                        {
                            // 出勤時間,退勤時間の取得
                            GetStartEnd(record, wh).ToTuple().Deconstruct(out strStartTime, out strEndTime);

                            // 休憩時間の取得
                            TimeSpan restTime = GetRestTime(strStartTime, strEndTime, wh);

                            // 時間外普通の取得
                            //overTime = CalcJikangaiHutsu(strStartTime, strEndTime, wh);

                            // 時間外深夜の計算
                            overTime2 = CalcJikangaiShinya(strStartTime, strEndTime, wh, Kintai.WORK_TYPE);

                            // 実働時間の取得
                            workTime = GetJitsudoTime(strStartTime, strEndTime, restTime);

                            // 実働時間の貼付け
                            if (workTime > TimeSpan.Zero)
                            {
                                record.JIMUSYONAI_JITSUDO = workTime.ToString(@"hh\:mm");
                            }
                        }
                        else
                        {
                            record.ERROR_TEXT = string.Format("勤務形態ファイルの出欠区分({0})を正しく読み込めませんでした。", kbn);
                        }
                    }

                    // 合計実働時間を計算
                    CalcGoukeiJitudouTime(record, overTime2);
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
            }
        }


        /// <summary> 自宅夜間対応データを勤務表に反映する
        /// </summary>
        private void SetYakinToKintai()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
                if (Yakan.DETAILS == null)
                {
                    return;
                }

                foreach (var yakanRecord in Yakan.DETAILS)
                {
                    TimeSpan ts = TimeSpan.Zero;
                    DateTime strTempDay = new DateTime(yakanRecord.YEAR, yakanRecord.MONTH, yakanRecord.DAY);
                    TimeSpan workTime = TimeSpan.TryParse(yakanRecord.TAIO_JIKAN, out ts) ? ts : TimeSpan.Zero;
                    TimeSpan overTime2 = TimeSpan.TryParse(yakanRecord.SINYA_JIKAN, out ts) ? ts : TimeSpan.Zero;

                    // 出勤簿の同一日付を探索
                    var record = Kintai.DETAILS.FirstOrDefault(grow => grow.YEAR == yakanRecord.YEAR
                                                                    && grow.MONTH == yakanRecord.MONTH
                                                                    && grow.DAY == yakanRecord.DAY);
                    if (record != null)
                    {
                        // エラー時は何もしない
                        if (record.ERROR_TEXT != ERROR_TEXT)
                        {
                            var yakanTaio = TimeSpan.TryParse(record.YAKAN_TAIO, out ts) ? ts : TimeSpan.Zero;
                            if ((workTime + yakanTaio).TotalHours > 24.0)
                            {
                                // 合計が1より大きい(24時間以上)のときはエラーとする
                                record.ERROR_TEXT = ERROR_TEXT;
                            }
                            else
                            {
                                // その他は加算する
                                record.YAKAN_TAIO = (workTime + yakanTaio).ToString(@"hh\:mm");
                            }
                        }
                        // エラー時は何もしない
                        if (record.ERROR_TEXT != ERROR_TEXT)
                        {
                            var sinyaJikan = TimeSpan.TryParse(record.SINYA_JIKAN, out ts) ? ts : TimeSpan.Zero;
                            if ((overTime2 + sinyaJikan).TotalHours > 24d)
                            {
                                // 合計が1より大きい(24時間以上)のときはエラーとする
                                record.ERROR_TEXT = ERROR_TEXT;
                            }
                            else
                            {
                                // その他は加算する
                                record.SINYA_JIKAN = (overTime2 + sinyaJikan).ToString(@"hh\:mm");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
            }
        }

        /// <summary> 出勤時間,退勤時間を取得する(トラ相,夜勤も取得)
        /// </summary>
        /// <param name="grow"></param>
        /// <param name="wh"></param>
        /// <returns></returns>
        private (string, string) GetStartEnd(T_KINTAI_DETAIL grow, WorkingHoursBaseInfo wh)
        {
            try
            {
                string strStartTime = grow.START;
                string strEndTime = grow.END;

                // 退勤時間取得
                strEndTime = CalcTaikinTime(strStartTime, strEndTime, wh);

                // 出勤時間取得
                strStartTime = CalcSyukkinTime(strStartTime, wh);

                return (strStartTime, strEndTime);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 退勤時間計算
        /// </summary>
        /// <param name="strStartTime"></param>
        /// <param name="strEndTime"></param>
        /// <param name="wh"></param>
        /// <returns></returns>
        private string CalcTaikinTime(string strStartTime, string strEndTime, WorkingHoursBaseInfo wh)
        {
            string strTemp = string.Empty; // 年月日保存領域
            try
            {
                // 24時間以上の勤務は無しとする
                // (出勤より退勤が小さい)ときは日付をまたいで勤務
                if (strStartTime.ToDateTime() > strEndTime.ToDateTime())
                {
                    // 次の日の退勤
                    strTemp = DAY_SECOND;
                }
                else
                {
                    // 同一日の退勤
                    strTemp = DAY_FIRST;
                }

                for (int i = 0; i + 1 <= wh.RestTime.Length - 1; i = i + 2)
                {
                    if (wh.RestTime[i] == string.Empty)
                    {
                        break;
                    }
                    string tempS = wh.RestTime[i];
                    string tempE = wh.RestTime[i + 1];

                    // 休憩時間中の退勤だった場合、休憩開始時間を退勤時間とする
                    if (tempS.ToDateTime() <= strEndTime.ToDateTime() && strEndTime.ToDateTime() <= tempE.ToDateTime())
                    {
                        return strTemp + tempS;
                    }
                }
                return strTemp + strEndTime;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 出勤時間取得
        /// </summary>
        /// <param name="strStartTime"></param>
        /// <param name="wh"></param>
        /// <returns></returns>
        private string CalcSyukkinTime(string strStartTime, WorkingHoursBaseInfo wh)
        {
            string strTemp = DAY_FIRST; // 年月日保存領域
            try
            {
                string tempSigyo = wh.SigyoTime;
                if (wh.ShukketuKbn == SyukketsuKbn.HAYADE)
                {
                    return strTemp + strStartTime;
                }
                if (strStartTime.ToDateTime() > tempSigyo.ToDateTime())
                {
                    for (int i = 0; i + 1 <= wh.RestTime.Length - 1; i = i + 2)
                    {
                        if (wh.RestTime[i] == string.Empty)
                        {
                            break;
                        }
                        // 休憩時間中の出勤だった場合、休憩終了時間を出勤時間とする

                        string tempS = wh.RestTime[i];
                        string tempE = wh.RestTime[i + 1];
                        if (tempS.ToDateTime() <= strStartTime.ToDateTime() && strStartTime.ToDateTime() <= tempE.ToDateTime())
                        {
                            return strTemp + tempE;
                        }
                    }
                    return strTemp + strStartTime;
                }
                else
                {
                    return strTemp + tempSigyo;
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 残業時間取得
        /// </summary>
        /// <param name="strStartTime"></param>
        /// <param name="strEndTime"></param>
        /// <param name="wh"></param>
        /// <returns></returns>
        private TimeSpan CalcJikangaiHutsu(string strStartTime, string strEndTime, WorkingHoursBaseInfo wh)
        {
            try
            {
                var zanHutsu = TimeSpan.Zero;       // 残業時間(普通)
                // 日付またぎの場合
                if (strEndTime.ToDateTime().Day == 2)
                {
                    // 残業時間の計算(当日分)
                    // ※日付またぎの場合は23時59分～0時00分の間の1分を加算
                    zanHutsu = CalcJikangaiHutsu(strStartTime, DAY_FIRST + DAY_END, "00:01", wh, zanHutsu, DAY_FIRST);

                    // 残業時間の計算(翌日分)
                    zanHutsu = CalcJikangaiHutsu(DAY_SECOND + DAY_START, strEndTime, string.Empty, wh, zanHutsu, DAY_SECOND);
                }
                else
                {
                    // 残業時間の計算
                    zanHutsu = CalcJikangaiHutsu(strStartTime, strEndTime, string.Empty, wh, zanHutsu, DAY_FIRST);
                }
                return zanHutsu;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 休憩時間取得
        /// </summary>
        /// <param name="strStartTime"></param>
        /// <param name="strEndTime"></param>
        /// <param name="wh"></param>
        /// <returns></returns>
        private TimeSpan GetRestTime(string strStartTime, string strEndTime, WorkingHoursBaseInfo wh)
        {
            try
            {
                TimeSpan restTime = TimeSpan.Zero;

                if (Kintai.WORK_TYPE == (int)WorkStyle.LCM)
                {
                    // 勤務形態がLCMの場合、休憩時間固定
                    switch (wh.ShukketuKbn)
                    {
                        case SyukketsuKbn.SYUKKIN:
                            restTime = new TimeSpan(1, 0, 0);
                            break;
                        case SyukketsuKbn.FULL:
                        case SyukketsuKbn.YAKIN:
                            restTime = new TimeSpan(1, 30, 0);
                            break;
                        default:
                            break;
                    }

                    if (strEndTime.ToDateTime() - strStartTime.ToDateTime() - restTime < TimeSpan.Zero)
                    {
                        return TimeSpan.Zero;
                    }
                    else
                    {
                        return restTime;
                    }
                }

                // 日付またぎの場合
                if (strEndTime.ToDateTime().Day == 2)
                {
                    // 休憩時間の計算(当日分)
                    // ※日付またぎの場合は23時59分～0時00分の間の1分を加算
                    CalcRestTime(strStartTime, DAY_FIRST + DAY_END, "00:01", wh, ref restTime, DAY_FIRST);

                    // 休憩時間の計算(翌日分)
                    CalcRestTime(DAY_SECOND + DAY_START, strEndTime, string.Empty, wh, ref restTime, DAY_SECOND);
                }
                else
                {
                    // 休憩時間の計算
                    CalcRestTime(strStartTime, strEndTime, string.Empty, wh, ref restTime, DAY_FIRST);
                }
                return restTime;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 休憩時間計算
        /// </summary>
        /// <param name="strStartTime"></param>
        /// <param name="strEndTime"></param>
        /// <param name="stradjust"></param>
        /// <param name="wh"></param>
        /// <param name="restTime"></param>
        /// <param name="strTemp"></param>
        private void CalcRestTime(string strStartTime, string strEndTime, string stradjust, WorkingHoursBaseInfo wh, ref TimeSpan restTime, string strTemp)
        {
            string strStartWork = string.Empty; // 休憩開始時間用ワーク
            string strEndWork = string.Empty;   // 休憩終了時間用ワーク
            try
            {
                for (int i = 0; i + 1 <= wh.RestTime.Length - 1; i = i + 2)
                {
                    if (wh.RestTime[i] == string.Empty)
                    {
                        break;
                    }
                    string tempS = wh.RestTime[i];
                    string tempE = wh.RestTime[i + 1];
                    if (tempS == tempE) continue;
                    if (strTemp == DAY_SECOND && tempS == DAY_END && tempE == DAY_START) continue;
                    if ((strTemp + tempE).ToDateTime() <= strEndTime.ToDateTime())
                    {

                        // 休憩開始時間の設定
                        if ((strTemp + tempS).ToDateTime() <= strStartTime.ToDateTime() && strStartTime.ToDateTime() <= (strTemp + tempE).ToDateTime())
                        {
                            // 休憩中に出勤した場合はそのまま代入
                            strStartWork = strStartTime;
                        }
                        else
                        {
                            if (strStartTime.ToDateTime() < (strTemp + tempS).ToDateTime())
                            {
                                // 休憩前に出勤した場合は休憩開始時間を代入
                                strStartWork = strTemp + tempS;
                            }
                            else
                            {
                                // 該当時間に勤務していない場合は休憩終了時間を代入
                                strStartWork = strTemp + tempE;
                            }
                        }
                        // 休憩終了時間は固定
                        strEndWork = strTemp + tempE;

                        // 休憩時間計算
                        TimeSpan ts = strEndWork.ToDateTime() - strStartWork.ToDateTime();
                        restTime = restTime + ts;

                        if (tempE == DAY_END)
                        {
                            // 日付またぎの場合は23:59から00:00までの1分を加算
                            if (stradjust != string.Empty)
                            {
                                restTime = restTime + TimeSpan.Parse(stradjust);
                                strTemp = DAY_SECOND;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 実働時間計算
        /// </summary>
        /// <param name="strStartTime"></param>
        /// <param name="strEndTime"></param>
        /// <param name="restTime"></param>
        /// <returns></returns>
        private TimeSpan GetJitsudoTime(string strStartTime, string strEndTime, TimeSpan restTime)
        {
            try
            {
                // 実働時間 = 退勤 - 出勤
                TimeSpan worktime = strEndTime.ToDateTime() - strStartTime.ToDateTime();
                if (worktime < TimeSpan.Zero)
                {
                    // 実働時間がマイナスになる場合
                    return TimeSpan.Zero;
                }
                // 実働時間 = 実働時間 - 休憩時間
                worktime -= restTime;
                if (worktime < TimeSpan.Zero)
                {
                    return TimeSpan.Zero;
                }
                return worktime;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        /// <summary> 合計実働時間計算
        /// </summary>
        /// <param name="detail"></param>
        /// <param name="overTime2"></param>
        private void CalcGoukeiJitudouTime(T_KINTAI_DETAIL detail, TimeSpan overTime2)
        {
            try
            {
                var ts = TimeSpan.Zero;
                var gokeiJitsudo = detail.JIMUSYONAI_JITSUDO.ToTimeSpan();

                // 合計実働
                if (detail.YAKAN_TAIO != string.Empty)
                {
                    // 夜間対応がある場合、合計実働時間に加算
                    gokeiJitsudo += detail.YAKAN_TAIO.ToTimeSpan();
                }
                detail.GOKEI_JITSUDO = gokeiJitsudo.ToString(@"hh\:mm");

                if (detail.KYUJITSU != string.Empty)
                {
                    // 休日勤務
                    detail.KYUJITSU_KINMU = detail.GOKEI_JITSUDO;
                }
                else
                {
                    var shiftTime = new TimeSpan(8, 0, 0);  // この時間を超えた分を時間外普通とする
                    if (Kintai.WORK_TYPE == (int)WorkStyle.LCM && (detail.SYUKKETSU_KBN == SyukketsuKbn.FULL || detail.SYUKKETSU_KBN == SyukketsuKbn.YAKIN))
                    {
                        // 勤務形態がLCM かつ 出欠区分がフルor夜勤
                        var wh = GetWorkingHours(detail.SYUKKETSU_KBN);
                        var endDate = wh.SigyoTime.ToTimeSpan() > wh.SyugyoTime.ToTimeSpan() ? DAY_SECOND : DAY_FIRST;

                        shiftTime = (endDate + wh.SyugyoTime).ToDateTime() - (DAY_FIRST + wh.SigyoTime).ToDateTime() - new TimeSpan(1, 30, 0);
                    }
                    if (gokeiJitsudo.TotalHours > shiftTime.TotalHours)
                    {
                        // 実働時間が8時間以上だった場合、残業時間を計算する
                        detail.ZAN_HUTSU = (gokeiJitsudo - shiftTime).ToString(@"hh\:mm");
                    }
                }

                if (detail.SINYA_JIKAN != string.Empty)
                {
                    // 深夜対応している場合
                    overTime2 += detail.SINYA_JIKAN.ToTimeSpan();
                }

                if (overTime2 > TimeSpan.Zero)
                {
                    // 時間外深夜
                    detail.ZAN_SINYA = overTime2.ToString(@"hh\:mm");
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }

        public T_KINTAI CalcSum()
        {
            try
            {
                Kintai.SUM_JUNKEKKIN_KAI = Kintai.DETAILS.Select(r => r.JUNKEKKIN_KAI).Sum();
                Kintai.SUM_JUNKEKKIN_JIKAN = Kintai.DETAILS.Select(r => r.JUNKEKKIN_JIKAN.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown(2);
                Kintai.SUM_JIMUSYONAI_JITSUDO = Kintai.DETAILS.Select(r => r.JIMUSYONAI_JITSUDO.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();
                Kintai.SUM_YAKAN_TAIO = Kintai.DETAILS.Select(r => r.YAKAN_TAIO.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();
                Kintai.SUM_GOKEI_JITSUDO = Kintai.DETAILS.Select(r => r.GOKEI_JITSUDO.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();
                Kintai.SUM_ZAN_SINYA = Kintai.DETAILS.Select(r => r.ZAN_SINYA.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();
                Kintai.SUM_KYUJITSU_KINMU = Kintai.DETAILS.Select(r => r.KYUJITSU_KINMU.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();
                Kintai.SUM_YAKAN_TAIKI = Kintai.DETAILS.Where(r => r.YAKAN_TAIKI != "0").Count();
                Kintai.SUM_TETUYA_AKEKIN = Kintai.DETAILS.Where(r => r.TETUYA_AKEKIN != string.Empty).Count();
                Kintai.SUM_GAISYUTSU_KAI = Kintai.DETAILS.Select(r => r.GAISYUTSU_KAI).Sum();
                Kintai.SUM_GAISYUTSU_JIKAN = Kintai.DETAILS.Select(r => r.GAISYUTSU_JIKAN.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();
                Kintai.SUM_DENKO = Kintai.DETAILS.Where(r => r.DENKO == true).Count();

                if (Kintai.WORK_TYPE == (int)WorkStyle.Attend)
                {
                    // 繰り越し計算を行う(合計実働+有給-所定労働時間)
                    var kurikoshi = Kintai.DETAILS.Select(r => r.GOKEI_JITSUDO.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();
                    kurikoshi += Kintai.DETAILS.Select(r => r.JUNKEKKIN_JIKAN.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();
                    kurikoshi -= Kintai.SCHEDULED_WORKING_HOURS;
                    Kintai.SUM_ZAN_HUTU = kurikoshi.RoundDown();
                    Kintai.BIKO = "次月繰越し";
                }
                else
                {
                    Kintai.SUM_ZAN_HUTU = Kintai.DETAILS.Select(r => r.ZAN_HUTSU.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundDown();
                }

                return Kintai;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
        }
    }
}
