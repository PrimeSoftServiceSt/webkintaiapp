﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace PssWebKintai.Logic
{
    public class KintaiAutoInput : KintaiCalc
    {
        /// <summary> 勤怠表
        /// </summary>
        protected T_KINTAI Kintai { get; set; }

        public KintaiAutoInput(T_KINTAI kintai)
        {
            Kintai = kintai;
        }

        public T_KINTAI AutoInputStartEnd()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);

                Init_Time(Kintai.WORK_TYPE);

                foreach (var detail in Kintai.DETAILS)
                {
                    if (detail.SYUKKETSU_KBN == SyukketsuKbn.EMPTY) continue;
                    if (detail.START != string.Empty) continue;
                    if (detail.END != string.Empty) continue;

                    var workShift = WorkingHoursList.FirstOrDefault(x => x.ShukketuKbn == detail.SYUKKETSU_KBN);
                    if (workShift == null) continue;
                    if (workShift.SigyoTime == string.Empty) continue;
                    if (workShift.SyugyoTime == string.Empty) continue;

                    detail.START = workShift.SigyoTime;
                    detail.END = workShift.SyugyoTime;
                }

                return Kintai;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Kintai.EMPLOYEE_NO);
            }
        }
    }
}