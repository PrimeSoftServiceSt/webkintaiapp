﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="KintaiStatusList.aspx.cs" Inherits="PssWebKintai.KintaiStatusList" %>

<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:DropDownList ID="drpNengetsu" runat="server" />
    <asp:Button ID="btnSearch" runat="server" Text="検索" OnClick="btnSearch_Click"/>

    <div style="height: 350px;">
        <asp:ListView ID="ListView1" runat="server">
            <LayoutTemplate>
                <table id="itemPlaceholderContainer" runat="server" border="1" >
                    <tr class="tableHeader">
                        <td>
                            <asp:Label runat="server" Text="年月" />
                        </td>
                        <td>
                            <asp:Label runat="server" Text="社員名" />
                        </td>
                        <td>
                            <asp:Label runat="server" Text="ステータス" />
                        </td>
                        <td>
                            <asp:Label runat="server" Text="取消" />
                        </td>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr class='<%# Container.DisplayIndex % 2 == 0 ? "even" : "odd" %>' >
                    <td>
                        <asp:Label runat="server" ID="NENGETSU" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="NAME" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="STATUS" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="TORIKESHI" OnClick="TORIKESHI_Click"/>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
</asp:Content>

