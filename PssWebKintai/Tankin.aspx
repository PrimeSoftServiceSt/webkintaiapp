﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tankin.aspx.cs" Inherits="PssWebKintai.Tankin" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%--   ヘッダー   --%>
    <div id="GridHeader">
        <table border="1">
            <tr class="tableHeader">
                <th runat="server">
                    <asp:Label Width="60px" runat="server" Style="text-align: center" Text="社員番号" /></th>
                <th runat="server">
                    <asp:Label Width="150px" runat="server" Style="text-align: center" Text="氏名" /></th>
                <th runat="server">
                    <asp:Label Width="80px" runat="server" Style="text-align: center" Text="単金" /></th>
            </tr>
        </table>
    </div>

    <%--   ボディ   --%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="GridBody" style="max-height: 350px; width: 309px;">
                <asp:ListView ID="ListView1" runat="server">
                    <LayoutTemplate>
                        <table id="itemPlaceholderContainer" runat="server" border="1">
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class='<%# Container.DisplayIndex % 2 == 0 ? "even" : "odd" %>' >
                            <td style="text-align: center">
                                <asp:Label Width="60px" ID="NO" runat="server" Text='<%# int.Parse(Eval("NO").ToString()).ToString("000000") %>' /></td>
                            <td>
                                <asp:Label Width="150px" ID="NAME" runat="server" Text='<%# Eval("NAME") %>' /></td>
                            <td>
                                <asp:TextBox Width="80px" ID="PRICE" runat="server" Text='<%# Eval("PRICE") %>' TextMode="Number" Style="text-align: right" /></td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
            <asp:Button ID="btnUpdate" runat="server" Text="更新" OnClick="btnUpdate_Click" />
            <asp:Button ID="btnBack" runat="server" Text="戻る" OnClick="btnBack_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
