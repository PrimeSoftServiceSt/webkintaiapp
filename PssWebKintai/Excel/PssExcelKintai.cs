﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.DBA;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PssWebKintai
{
    /// <summary> 勤怠表Excel出力処理
    /// </summary>
    class PssExcelKintai : ExcelLoader
    {
        #region 変数

        /// <summary> 勤怠情報
        /// </summary>
        private T_KINTAI Kintai;

        private bool IsKanrisya;
        #endregion

        #region コンストラクタ

        /// <summary> コンストラクタ
        /// </summary>
        public PssExcelKintai(string path, T_KINTAI kintai, bool isKanrisya) : base(path)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                Kintai = kintai;
                IsKanrisya = isKanrisya;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }

        #endregion

        #region EXCEL編集処理

        /// <summary> メイン処理
        /// </summary>
        /// <returns></returns>
        protected override bool Main()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                int iMeisaiRow = 6;         // 明細最初行
                int iMeisaiCol = 1;         // 明細最初列
                int iGoukeiCol = 8;         // 合計列
                const int iGoukeiRow = 37;  // 合計行

                var dba = new DbaPss();
                var lstSyukketsuKbn = dba.GetSyukketsuKbnList();
                var lstYakanTaiki = dba.GetYakanTaikiList();
                var lstSFTR = dba.GetSFTRList();

                // ヘッダ部作成
                base.SetValue(1, 1, Kintai.YEAR);
                base.SetValue(1, 3, Kintai.MONTH);
                base.SetValue(1, 9, Kintai.EMPLOYEE_NO);
                base.SetValue(2, 9, Kintai.NAME);

                base.SetValue(2, 15, dba.GetPostName(Kintai.POST));
                base.SetValue(1, 22, dba.GetWorkTypeName(Kintai.WORK_TYPE));

                // アテンドの場合
                if (Kintai.WORK_TYPE == (int)WorkStyle.Attend)
                {
                    // ヘッダの名称変更
                    base.SetValue(4, 8, "有休/特休");
                    base.SetValue(4, 10, "休憩前労働時間");
                    base.SetValue(4, 11, "休憩時間");

                    // 合計行（次回繰り越し）
                    base.SetValue(iGoukeiRow, 22, "次月繰り越し");
                }

                // 明細部作成
                var lstDay = new List<DateTime>();
                foreach (var detail in this.Kintai.DETAILS)
                {
                    var d = new DateTime(detail.YEAR, detail.MONTH, detail.DAY);
                    if (lstDay.Contains(d) == false)
                    {
                        lstDay.Add(d);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, d.ToString("yyyy/MM/dd"));
                        base.SetValue(iMeisaiRow, iMeisaiCol++, d.ToString("ddd"));
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.KYUJITSU);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.KINMU_KBN);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, lstSyukketsuKbn.GetValue1(detail.SYUKKETSU_KBN));
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.START);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.END);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.JUNKEKKIN_KAI.ToString("0.0"));
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.JUNKEKKIN_JIKAN);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.JIMUSYONAI_JITSUDO);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.YAKAN_TAIO);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.GOKEI_JITSUDO);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.ZAN_HUTSU);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.ZAN_SINYA);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.KYUJITSU_KINMU);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, lstYakanTaiki.GetValue1(detail.YAKAN_TAIKI));
                        base.SetValue(iMeisaiRow, iMeisaiCol++, lstSFTR.GetValue1(detail.TETUYA_AKEKIN));
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.GAISYUTSU_KAI);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.GAISYUTSU_JIKAN);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.DENKO ? Circle.WHITE : string.Empty);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.IDO_JIKAN);
                        base.SetValue(iMeisaiRow, iMeisaiCol++, detail.BIKO);
                    }

                    iMeisaiRow++;
                    iMeisaiCol = 1;
                }

                // 明細部合計行作成
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_JUNKEKKIN_KAI);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_JUNKEKKIN_JIKAN);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_JIMUSYONAI_JITSUDO);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_YAKAN_TAIO);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_GOKEI_JITSUDO);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_ZAN_HUTU);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_ZAN_SINYA);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_KYUJITSU_KINMU);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_YAKAN_TAIKI);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_TETUYA_AKEKIN);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_GAISYUTSU_KAI);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_GAISYUTSU_JIKAN);
                base.SetValue(iGoukeiRow, iGoukeiCol++, this.Kintai.SUM_DENKO);

                // フッタ部作成
                this.WriteFooter();

                return true;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }

        /// <summary> フッター部作成
        /// </summary>
        private void WriteFooter()
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                if (!IsKanrisya)
                {
                    // 印刷モード(管理者)以外の場合,フッターを初期化する
                    foreach (int i in Enumerable.Range(40, 11))
                    {
                        foreach (int j in Enumerable.Range(16, 7))
                        {
                            base.SetCleer(i, j);
                        }
                    }
                    return;
                }

                // 平日出勤数取得
                decimal decHeijitu = 0m;
                decHeijitu += this.Kintai.DETAILS.Where(dr => SyukketsuKbn.ListHeijitsuNormal.Contains(dr.SYUKKETSU_KBN))
                                                 .Select(dr => 1m)
                                                 .Sum();
                decHeijitu += this.Kintai.DETAILS.Where(dr => SyukketsuKbn.ListHeijitsuHalf.Contains(dr.SYUKKETSU_KBN))
                                                 .Select(dr => 0.5m)
                                                 .Sum(); // 半特/半休は0.5日分としてカウント
                                                         // 平日出勤数取得
                base.SetValue(40, 18, decHeijitu.ToString("0.0"));

                // 休日出勤数取得
                base.SetValue(41, 18, this.Kintai.DETAILS.Where(dr => SyukketsuKbn.ListKyujitsu.Contains(dr.SYUKKETSU_KBN)).Count());

                // 有給日数取得
                base.SetValue(42, 18, this.Kintai.DETAILS.Where(dr => SyukketsuKbn.ListYukyu.Contains(dr.SYUKKETSU_KBN))
                                                         .Select(dr => dr.SYUKKETSU_KBN == SyukketsuKbn.YUKYU ? 1m : 0.5m)
                                                         .Sum().ToString("0.0"));

                // 代休日数取得
                decimal decDaikyu = this.Kintai.DETAILS.Where(dr => dr.SYUKKETSU_KBN == SyukketsuKbn.DAIKYU).Count();
                base.SetValue(43, 18, decDaikyu);

                // 特休日数取得
                base.SetValue(44, 18, this.Kintai.DETAILS.Where(dr => SyukketsuKbn.ListTokukyu.Contains(dr.SYUKKETSU_KBN))
                                                         .Select(dr => dr.SYUKKETSU_KBN == SyukketsuKbn.TOKUKYU ? 1m : 0.5m)
                                                         .Sum().ToString("0.0"));

                // 欠勤回数
                base.SetValue(45, 18, this.Kintai.DETAILS.Where(dr => dr.SYUKKETSU_KBN == SyukketsuKbn.KEKKIN).Count());

                // アテンド以外の場合
                if (Kintai.WORK_TYPE != (int)WorkStyle.Attend)
                {
                    // 準欠勤回数
                    base.SetValue(46, 18, this.Kintai.DETAILS.Select(dr => dr.JUNKEKKIN_KAI).Sum().ToString("0.0"));

                    // 準欠勤(h)
                    base.SetValue(47, 18, this.Kintai.DETAILS.Select(dr => dr.JUNKEKKIN_JIKAN.ToTimeSpan()).Sum().GetDecimalTotalHours().RoundUp(2).ToString("0.00"));

                }

                // 出張旅費
                base.SetValue(48, 18, this.Kintai.RYOHI);

                // テレワーク補助
                int teleCount = this.Kintai.DETAILS.Where(dr => dr.DENKO).Count();
                base.SetValue(49, 18, teleCount >= 6 ? teleCount * 200 : 0);

                decimal decHutu = this.Kintai.SUM_ZAN_HUTU;                 // 普通残業
                if (this.Kintai.WORK_TYPE == (int)WorkStyle.Attend && decHutu < 0)
                {
                    // アテンドで普通残業(次月繰り越し)がマイナスの場合は0とする
                    decHutu = 0;
                }
                decimal decKyujitu = this.Kintai.SUM_KYUJITSU_KINMU;        // 休出
                decimal decSinya = this.Kintai.SUM_ZAN_SINYA;               // 深夜
                decimal decJyunkekkin = this.Kintai.SUM_JUNKEKKIN_JIKAN;    // 準欠勤

                // 欠勤回数,準欠勤(回,h)取得
                TimeSpan tsHourSum = TimeSpan.Zero;
                decimal intCountSum = 0;
                decimal decKekkin = this.Kintai.DETAILS.Where(dr => dr.SYUKKETSU_KBN == SyukketsuKbn.KEKKIN).Count();
                if (this.Kintai.WORK_TYPE != (int)WorkStyle.Attend)
                {
                    this.Kintai.DETAILS.Where(dr => dr.JUNKEKKIN_KAI != 0 || dr.JUNKEKKIN_JIKAN != string.Empty).ToList().ForEach(dr =>
                    {
                        tsHourSum += dr.JUNKEKKIN_JIKAN.ToTimeSpan();
                        intCountSum += dr.JUNKEKKIN_KAI;
                        if (tsHourSum.TotalHours >= 5)
                        {
                            decKekkin++;
                            intCountSum = 0;
                            tsHourSum -= new TimeSpan(5L * TimeSpan.TicksPerHour);
                        }
                        else if (intCountSum >= 4)
                        {
                            decKekkin++;
                            intCountSum = 0;
                            tsHourSum = TimeSpan.Zero;
                        }
                    });
                }

                // 賃金計算取得
                base.SetValue(40, 22, CalcManey(1.25m, decHutu));                                               // 普通残業
                if (this.Kintai.WORK_TYPE == (int)WorkStyle.Attend)
                {
                    base.SetValue(41, 22, CalcManey(0.25m, decKyujitu));                                        // 休出(アテンド)
                }
                else
                {
                    base.SetValue(41, 22, CalcManey(1.25m, decKyujitu));                                        // 休出
                    base.SetValue(43, 22, CalcManey(1m, tsHourSum.GetDecimalTotalHours()));                     // 準欠勤
                }
                base.SetValue(42, 22, CalcManey(0.25m, decSinya));                                              // 深夜
                base.SetValue(44, 22, CalcManey(8m, decKekkin));                                                // 欠勤
                base.SetValue(45, 22, CalcManey(8m, decDaikyu));                                                // 代休     
                base.SetValue(46, 22, this.Kintai.DETAILS.Where(dr => dr.YAKAN_TAIKI == "1").Count() * 2000);   // 夜間待機（平）
                base.SetValue(47, 22, this.Kintai.DETAILS.Where(dr => dr.YAKAN_TAIKI == "2").Count() * 3000);   // 夜間待機（休）

                base.SetValue(48, 22, this.Kintai.SHIKAKU_HOJO);                                                // 資格補助金     
                base.SetValue(49, 22, this.Kintai.SHIKAKU_HOSYO);                                               // 資格報奨金     

            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }

        /// <summary> 賃金計算
        /// </summary>
        /// <param name="fix">倍率</param>
        /// <param name="hours">時間</param>
        /// <returns></returns>
        private decimal CalcManey(decimal fix, decimal hours)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                decimal price = Kintai.PRICE;
                decimal temp = price * fix * hours;

                return temp.RoundOff(0);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }

        #endregion
    }
}