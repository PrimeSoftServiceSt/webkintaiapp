﻿namespace PssWebKintai.Model
{
    /// <summary> 勤務時間基本情報
    /// </summary>
    public class WorkingHoursBaseInfo
    {
        ///// <summary>
        ///// </summary>
        //public const string SIGYO_SYUGYO = "始業終業";
        ///// <summary>
        ///// </summary>
        //public const string REST = "休憩時間";
        ///// <summary>
        ///// </summary>
        //public const string ZAN = "残業時間";

        ///// <summary> 
        ///// </summary>
        //public bool IsNormal
        //{
        //    get
        //    {
        //        return ShukketuKbn != string.Empty
        //            && SigyoTime != string.Empty
        //            && SyugyoTime != string.Empty
        //            && RestTime != null
        //            && RestTime.Length != 0
        //            && RestTime.Length % 2 == 0
        //            && ZanTime != null
        //            && ZanTime.Length != 0
        //            && ZanTime.Length % 2 == 0;
        //    }
        //}

        /// <summary>
        /// </summary>
        public string ShukketuKbn = string.Empty;

        /// <summary>
        /// </summary>
        public string SigyoTime = string.Empty;

        /// <summary>
        /// </summary>
        public string SyugyoTime = string.Empty;

        /// <summary>
        /// </summary>
        public string[] RestTime = null;

        /// <summary>
        /// </summary>
        public string[] ZanTime = null;

        ///// <summary>
        ///// </summary>
        ///// <param name="shukketsuKbn"></param>
        ///// <param name="timeArray"></param>
        //public WorkingHoursBaseInfo(string shukketsuKbn, string[] timeArray)
        //{
        //    ShukketuKbn = shukketsuKbn;
        //    SetTiemArray(timeArray);
        //}

        ///// <summary>
        ///// </summary>
        ///// <param name="timeArray"></param>
        //public void SetTiemArray(string[] timeArray)
        //{

        //    if (timeArray[0].StartsWith(SIGYO_SYUGYO))
        //    {
        //        SigyoTime = timeArray[1];
        //        SyugyoTime = timeArray[2];
        //    }
        //    else if (timeArray[0].StartsWith(REST))
        //    {
        //        RestTime = timeArray.Skip(1).Where(x => x != string.Empty).ToArray();
        //    }
        //    else if (timeArray[0].StartsWith(ZAN))
        //    {
        //        ZanTime = timeArray.Skip(1).Where(x => x != string.Empty).ToArray();
        //    }
        //}
    }
}
