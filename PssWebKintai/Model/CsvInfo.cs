﻿using System.Collections.Generic;

namespace PssWebKintai.Model
{
    /// <summary>
    /// </summary>
    public class CsvInfo
    {

        /// <summary>
        /// </summary>
        public List<string> ValueList
        {
            get
            {
                return new List<string>()
                {
                    SYAINBANGOU,
                    SIMEI,
                    HEIJITUSYKKIN,
                    KYUJITISYUKKIN,
                    SYUKKINJIKAN,
                    CHIKOKU,
                    SOUTAI,
                    YUKYU,
                    DAIKYU,
                    KOUKYU,
                    SONOHOKA,
                    SEIRIKYUKA,
                    KYUSYOKU,
                    KEKKIN,
                    NITIGAKU,
                    KOUJYONISU,
                    KOUJYOJIKAN,
                    HOUTEINAI,
                    HEIJITU,
                    HEIJITISINYA,
                    KYUJITI,
                    KYUJITISINYA,
                    HOUKYUSINYA,
                    HOUTEIKYUJITU,
                    SYOTEIJIKAN,
                    KYUJITUKINMU,
                    HOUTEIKYUJITUWARI,
                    SINNYAKINNMU,
                    CHIKOKU_SOUTAI,
                    KEKKIN_DAIKYU,
                    HOUKYUSINYAWARI,
                    KAISUTEATE,
                    RYOHI,
                    JIKANGAI_TEATE,
                    KYUYOKUJYO,
                    TELEWORK
                };
            }
        }

        /// <summary>
        /// </summary>
        public string SYAINBANGOU;

        /// <summary>
        /// </summary>
        public string SIMEI;

        /// <summary>
        /// </summary>
        public string HEIJITUSYKKIN;

        /// <summary>
        /// </summary>
        public string KYUJITISYUKKIN;

        /// <summary>
        /// </summary>
        public string SYUKKINJIKAN;

        /// <summary>
        /// </summary>
        public string CHIKOKU;

        /// <summary>
        /// </summary>
        public string SOUTAI;

        /// <summary>
        /// </summary>
        public string YUKYU;

        /// <summary>
        /// </summary>
        public string DAIKYU;

        /// <summary>
        /// </summary>
        public string KOUKYU;

        /// <summary>
        /// </summary>
        public string SONOHOKA;

        /// <summary>
        /// </summary>
        public string SEIRIKYUKA;

        /// <summary>
        /// </summary>
        public string KYUSYOKU;

        /// <summary>
        /// </summary>
        public string KEKKIN;

        /// <summary>
        /// </summary>
        public string NITIGAKU;

        /// <summary>
        /// </summary>
        public string KOUJYONISU;

        /// <summary>
        /// </summary>
        public string KOUJYOJIKAN;

        /// <summary>
        /// </summary>
        public string HOUTEINAI;

        /// <summary>
        /// </summary>
        public string HEIJITU;

        /// <summary>
        /// </summary>
        public string HEIJITISINYA;

        /// <summary>
        /// </summary>
        public string KYUJITI;

        /// <summary>
        /// </summary>
        public string KYUJITISINYA;

        /// <summary>
        /// </summary>
        public string HOUKYUSINYA;

        /// <summary>
        /// </summary>
        public string HOUTEIKYUJITU;

        /// <summary>
        /// </summary>
        public string SYOTEIJIKAN;

        /// <summary>
        /// </summary>
        public string KYUJITUKINMU;

        /// <summary>
        /// </summary>
        public string HOUTEIKYUJITUWARI;

        /// <summary>
        /// </summary>
        public string SINNYAKINNMU;

        /// <summary>
        /// </summary>
        public string CHIKOKU_SOUTAI;

        /// <summary>
        /// </summary>
        public string KEKKIN_DAIKYU;

        /// <summary>
        /// </summary>
        public string HOUKYUSINYAWARI;

        /// <summary>
        /// </summary>
        public string KAISUTEATE;

        /// <summary>
        /// </summary>
        public string RYOHI;

        /// <summary>
        /// </summary>
        public string JIKANGAI_TEATE;

        /// <summary>
        /// </summary>
        public string KYUYOKUJYO;

        /// <summary> 
        /// </summary>
        public string TELEWORK;
    }
}
