﻿using Newtonsoft.Json;
using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PssWebKintai
{
    public partial class SiteMaster : MasterPage
    {
        private M_EMPLOYEE _loginEmployeeInfo;
        /// <summary> 社員情報
        /// </summary>
        public M_EMPLOYEE LoginEmployeeInfo
        {
            get
            {
                if (_loginEmployeeInfo != null)
                {
                    return _loginEmployeeInfo;
                }
                else
                {
                    var key = SessionKey.LOGIN_EMPLOYEE_INFO;
                    var value = Session[key] as string;
                    if (string.IsNullOrEmpty(value))
                        return null;
                    else
                    {
                        _loginEmployeeInfo = JsonConvert.DeserializeObject<M_EMPLOYEE>(value);
                        return _loginEmployeeInfo;
                    }
                }
            }
            set
            {
                _loginEmployeeInfo = value;
                Session[SessionKey.LOGIN_EMPLOYEE_INFO] = JsonConvert.SerializeObject(value);
            }
        }


        private M_EMPLOYEE _dispEmployeeInfo;
        /// <summary> 社員情報
        /// </summary>
        public M_EMPLOYEE DispEmployeeInfo
        {
            get
            {
                if (_dispEmployeeInfo != null)
                {
                    return _dispEmployeeInfo;
                }
                else
                {
                    var key = SessionKey.DISPLAY_EMPLOYEE_INFO;
                    var value = Session[key] as string;
                    if (string.IsNullOrEmpty(value))
                    {
                        return null;
                    }
                    else
                    {
                        _dispEmployeeInfo = JsonConvert.DeserializeObject<M_EMPLOYEE>(value);
                        return _dispEmployeeInfo;
                    }
                }
            }
            set
            {
                _dispEmployeeInfo = value;
                Session[SessionKey.DISPLAY_EMPLOYEE_INFO] = JsonConvert.SerializeObject(value);
            }
        }

        /// <summary> 勤怠情報
        /// </summary>
        public T_KINTAI KintaiInfo
        {
            get
            {
                var key = SessionKey.KINTAI_INFO;
                var value = Session[key] as string;
                if (string.IsNullOrEmpty(value))
                    return new T_KINTAI();
                else
                    return JsonConvert.DeserializeObject<T_KINTAI>(value);
            }
            set { Session[SessionKey.KINTAI_INFO] = JsonConvert.SerializeObject(value); }
        }

        /// <summary> 自宅夜間対応情報
        /// </summary>
        public T_YAKAN YakanInfo
        {
            get
            {
                var key = SessionKey.YAKAN_INFO;
                var value = Session[key] as string;
                if (string.IsNullOrEmpty(value))
                    return new T_YAKAN();
                else
                    return JsonConvert.DeserializeObject<T_YAKAN>(value);
            }
            set { Session[SessionKey.YAKAN_INFO] = JsonConvert.SerializeObject(value); }
        }

        /// <summary> ページロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (LoginEmployeeInfo == null)
                {
                    btnLogout.Visible = false;
                }
                else
                {
                    lblLoginUser.Text = string.Format($"{LoginEmployeeInfo.NO}:{LoginEmployeeInfo.NAME}");
                }
                // ページ内の検証コントロールを順番に走査。
                // もともと設定されていたErrorMessage属性（項目名）や
                // そのほかのパラメータから、
                // それぞれの検証型に応じたメッセージを動的に生成する
                foreach (BaseValidator valid in Page.Validators)
                {
                    switch (valid.GetType().Name)
                    {
                        case "RequiredFieldValidator":
                            valid.ErrorMessage = string.Format(
                              "{0}は必須項目です。", valid.ErrorMessage);
                            break;

                        case "CompareValidator":
                            // CompareValidatorでは、項目名は「項目1,項目2」の
                            // 形式で指定されているものとする。
                            // 項目名をカンマで分割し、配列aryMsgにセット
                            string[] aryMsg = valid.ErrorMessage.Split(',');

                            // Operator属性の値によって、メッセージを分岐
                            switch (((CompareValidator)valid).Operator)
                            {

                                case ValidationCompareOperator.Equal:
                                    valid.ErrorMessage = string.Format(
                                      "{0}は{1}と等しい値でなければなりません。",
                                      aryMsg[0], aryMsg[1]);
                                    break;

                                case ValidationCompareOperator.NotEqual:
                                    valid.ErrorMessage = string.Format(
                                      "{0}は{1}と異なる値でなければなりません。",
                                      aryMsg[0], aryMsg[1]);
                                    break;

                                case ValidationCompareOperator.GreaterThan:
                                    valid.ErrorMessage = string.Format(
                                      "{0}は{1}より大きい値でなければなりません。",
                                      aryMsg[0], aryMsg[1]);
                                    break;

                                case ValidationCompareOperator.GreaterThanEqual:
                                    valid.ErrorMessage = string.Format(
                                      "{0}は{1}以上でなければなりません。",
                                      aryMsg[0], aryMsg[1]);
                                    break;

                                case ValidationCompareOperator.LessThan:
                                    valid.ErrorMessage = string.Format(
                                      "{0}は{1}より小さい値でなければなりません。",
                                      aryMsg[0], aryMsg[1]);
                                    break;

                                case ValidationCompareOperator.LessThanEqual:
                                    valid.ErrorMessage = string.Format(
                                      "{0}は{1}以下でなければなりません。",
                                      aryMsg[0], aryMsg[1]);
                                    break;

                                case ValidationCompareOperator.DataTypeCheck:
                                    valid.ErrorMessage = string.Format(
                                      "{0}は正しい型でありません。",
                                      aryMsg[0]);
                                    break;
                            }
                            break;

                        case "RangeValidator":
                            valid.ErrorMessage = string.Format(
                              "{0}は{1}～{2}の間でなければなりません。",
                              valid.ErrorMessage,
                              ((RangeValidator)valid).MinimumValue,
                              ((RangeValidator)valid).MaximumValue);
                            break;

                        case "RegularExpressionValidator":
                            valid.ErrorMessage = string.Format(
                              "{0}は正しい形式ではありません。",
                              valid.ErrorMessage);
                            break;
                    }
                }
            }
        }

        /// <summary> ログアウトボタン押下時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogout_Click(object sender, EventArgs e)
        {
            string loginEmployeeNo = string.Empty;
            try
            {
                loginEmployeeNo = LoginEmployeeInfo == null ? string.Empty : LoginEmployeeInfo.NO;
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), LoginEmployeeInfo.NO);
                Session.Clear();
                Response.Redirect("Default.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), loginEmployeeNo);
            }
        }

        /// <summary> アラート表示
        /// </summary>
        /// <param name="message"></param>
        public void ShowAlart(string message)
        {
            var csname1 = "PopupScript";
            var cstype = this.GetType();

            var cs = Page.ClientScript;

            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                var cstext1 = string.Format("alert('{0}');", message);
                cs.RegisterStartupScript(cstype, csname1, cstext1, true);
            }
        }

        /// <summary> アラート表示(アップデートパネル内)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="panel"></param>
        public void ShowAlart(string message, UpdatePanel panel)
        {
            var script = string.Format("alert('{0}');", message);
            ScriptManager.RegisterStartupScript(panel, panel.GetType(), "alert", script, true);
        }

        /// <summary> エラー表示
        /// </summary>
        /// <param name="message"></param>
        public void ShowErrorAlart()
        {
            ShowAlart("エラーが発生しました");
        }

        /// <summary> エラー表示(アップデートパネル内)
        /// </summary>
        /// <param name="message"></param>
        /// <param name="panel"></param>
        public void ShowErrorAlart(UpdatePanel panel)
        {
            ShowAlart("エラーが発生しました", panel);
        }
    }
}