﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.DBA;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI.WebControls;

namespace PssWebKintai
{
    public partial class Tankin : System.Web.UI.Page
    {
        /// <summary> ページロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                    var dba = new DbaEmployee(Master.LoginEmployeeInfo.NO);
                    ListView1.DataSource = dba.GetEmployeeList(role: ((int)Role.Normal).ToString());
                    ListView1.DataBind();
                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                    Master.ShowErrorAlart();
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                }
            }
        }

        /// <summary> 更新ボタン押下時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var lstTankin = new List<M_EMPLOYEE>();
                foreach (var item in ListView1.Items)
                {
                    var txtNo = item.GetControl<Label>("NO");
                    var txtPrice = item.GetControl<TextBox>("PRICE");
                    var price = txtPrice.Text.Replace(",", "");

                    if (int.TryParse(price, out var i) == false)
                    {
                        Master.ShowAlart("単金に数値以外が入力されています。", UpdatePanel1);
                        txtPrice.Focus();
                        return;
                    }

                    var employee = new M_EMPLOYEE()
                    {
                        NO = txtNo.Text,
                        PRICE = i
                    };

                    lstTankin.Add(employee);
                }
                var dba = new DbaEmployee(Master.LoginEmployeeInfo.NO);
                if (dba.UpdateTankin(lstTankin))
                {
                    Master.ShowAlart("単金を更新しました", UpdatePanel1);
                }
                else
                {
                    Master.ShowAlart("単金の更新に失敗しました", UpdatePanel1);
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }


        /// <summary> 戻るボタン押下時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                Response.Redirect("Menu.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }
    }
}