﻿using Newtonsoft.Json;
using PssWebKintai.Common;

namespace PssWebKintai.Entity
{
    [JsonObject]
    public class M_EMPLOYEE : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("NO")]
        public string NO { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("NAME")]
        public string NAME { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("POST")]
        public int POST { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("LANK")]
        public string LANK { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("WORK_TYPE")]
        public int WORK_TYPE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("PRICE")]
        public int PRICE { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("SUPERIOR_NO")]
        public string SUPERIOR_NO { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("PASSWORD")]
        public string PASSWORD { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("KINTAI_KENGEN")]
        public int KINTAI_KENGEN { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("ANPI_KENGEN")]
        public int ANPI_KENGEN { get; set; }

        [JsonIgnore]
        public bool IsNormal { get { return KINTAI_KENGEN == (int)Role.Normal; } }
        [JsonIgnore]
        public bool IsSuperior { get { return KINTAI_KENGEN == (int)Role.Superior; } }
        [JsonIgnore]
        public bool IsClerk { get { return KINTAI_KENGEN == (int)Role.Clerk; } }
    }
}