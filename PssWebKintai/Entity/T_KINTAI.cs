﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PssWebKintai.Entity
{
    [JsonObject]
    public class T_KINTAI : BaseEntity
    {
        /// <summary>社員番号
        /// </summary>
        [JsonProperty("EMPLOYEE_NO")]
        public string EMPLOYEE_NO { get; set; }

        /// <summary>年
        /// </summary>
        [JsonProperty("YEAR")]
        public int YEAR { get; set; }

        /// <summary>月
        /// </summary>
        [JsonProperty("MONTH")]
        public int MONTH { get; set; }

        /// <summary>氏名
        /// </summary>
        [JsonProperty("NAME")]
        public string NAME { get; set; }

        /// <summary>部署
        /// </summary>
        [JsonProperty("POST")]
        public int POST { get; set; }

        /// <summary>等級
        /// </summary>
        [JsonProperty("LANK")]
        public string LANK { get; set; }

        /// <summary>勤務形態
        /// </summary>
        [JsonProperty("WORK_TYPE")]
        public int WORK_TYPE { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("PRICE")]
        public int PRICE { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("RYOHI")]
        public int RYOHI { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SHIKAKU_HOJO")]
        public int SHIKAKU_HOJO { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SHIKAKU_HOSYO")]
        public int SHIKAKU_HOSYO { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SCHEDULED_WORKING_HOURS")]
        public decimal SCHEDULED_WORKING_HOURS { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("STATUS")]
        public int STATUS { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("TANTO_NO")]
        public string TANTO_NO { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_JUNKEKKIN_KAI")]
        public decimal SUM_JUNKEKKIN_KAI { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_JUNKEKKIN_JIKAN")]
        public decimal SUM_JUNKEKKIN_JIKAN { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_JIMUSYONAI_JITSUDO")]
        public decimal SUM_JIMUSYONAI_JITSUDO { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_YAKAN_TAIO")]
        public decimal SUM_YAKAN_TAIO { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_GOKEI_JITSUDO")]
        public decimal SUM_GOKEI_JITSUDO { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_ZAN_HUTSU")]
        public decimal SUM_ZAN_HUTU { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_ZAN_SINYA")]
        public decimal SUM_ZAN_SINYA { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_KYUJITSU_KINMU")]
        public decimal SUM_KYUJITSU_KINMU { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_YAKAN_TAIKI")]
        public int SUM_YAKAN_TAIKI { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_TETUYA_AKEKIN")]
        public int SUM_TETUYA_AKEKIN { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_GAISYUTSU_KAI")]
        public decimal SUM_GAISYUTSU_KAI { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_GAISYUTSU_JIKAN")]
        public decimal SUM_GAISYUTSU_JIKAN { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_SINYA_JIKAN")]
        public decimal SUM_SINYA_JIKAN { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_DENKO")]
        public int SUM_DENKO { get; set; }

        /// <summary> 
        /// </summary>
        [JsonProperty("BIKO")]
        public string BIKO { get; set; }

        /// <summary> 
        /// </summary>
        [JsonProperty("ERROR_TEXT")]
        public string ERROR_TEXT { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("DETAILS")]
        public List<T_KINTAI_DETAIL> DETAILS { get; set; } = new List<T_KINTAI_DETAIL>();
    }
}
