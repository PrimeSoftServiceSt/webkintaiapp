﻿using Newtonsoft.Json;

namespace PssWebKintai.Entity
{
    public class M_POST : BaseEntity
    {
        /// <summary>
        /// </summary>
        [JsonProperty("CODE")]
        public int CODE { get; set; }
        /// <summary>
        /// </summary>
        [JsonProperty("NAME")]
        public string NAME { get; set; }
    }
}