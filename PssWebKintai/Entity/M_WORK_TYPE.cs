﻿using Newtonsoft.Json;

namespace PssWebKintai.Entity
{
    public class M_WORK_TYPE : BaseEntity
    {
        /// <summary>
        /// </summary>
        [JsonProperty("WORK_TYPE")]
        public int WORK_TYPE { get; set; }
        /// <summary>
        /// </summary>
        [JsonProperty("NAME")]
        public string NAME { get; set; }
    }
}