﻿using Newtonsoft.Json;

namespace PssWebKintai.Entity
{
    public class T_YAKAN_DETAIL : BaseEntity
    {
        /// <summary>
        /// </summary>
        [JsonProperty("EMPLOYEE_NO")]
        public string EMPLOYEE_NO { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("YEAR")]
        public int YEAR { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("MONTH")]
        public int MONTH { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("DAY")]
        public int DAY { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("START")]
        public string START { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("END")]
        public string END { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("TAIO_JIKAN")]
        public string TAIO_JIKAN { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SINYA_JIKAN")]
        public string SINYA_JIKAN { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("INCIDENT_NO")]
        public string INCIDENT_NO { get; set; }
    }
}