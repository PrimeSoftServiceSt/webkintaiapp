﻿using Newtonsoft.Json;

namespace PssWebKintai.Entity
{
    /// <summary> 勤怠情報詳細
    /// </summary>
    [JsonObject]
    public class T_KINTAI_DETAIL : BaseEntity
    {
        /// <summary> 社員番号
        /// </summary>
        [JsonProperty("EMPLOYEE_NO")]
        public string EMPLOYEE_NO { get; set; }

        /// <summary> 年
        /// </summary>
        [JsonProperty("YEAR")]
        public int YEAR { get; set; }

        /// <summary> 月
        /// </summary>
        [JsonProperty("MONTH")]
        public int MONTH { get; set; }

        /// <summary> 日
        /// </summary>
        [JsonProperty("DAY")]
        public int DAY { get; set; }

        /// <summary> 休日
        /// </summary>
        [JsonProperty("KYUJITSU")]
        public string KYUJITSU { get; set; }

        /// <summary> 勤務区分
        /// </summary>
        [JsonProperty("KINMU_KBN")]
        public string KINMU_KBN { get; set; }

        /// <summary> 出欠区分
        /// </summary>
        [JsonProperty("SYUKKETSU_KBN")]
        public string SYUKKETSU_KBN { get; set; }

        /// <summary> 出勤
        /// </summary>
        [JsonProperty("START")]
        public string START { get; set; }

        /// <summary> 退勤
        /// </summary>
        [JsonProperty("END")]
        public string END { get; set; }

        /// <summary> 準欠勤回
        /// </summary>
        [JsonProperty("JUNKEKKIN_KAI")]
        public decimal JUNKEKKIN_KAI { get; set; }

        /// <summary> 準欠勤時間
        /// </summary>
        [JsonProperty("JUNKEKKIN_JIKAN")]
        public string JUNKEKKIN_JIKAN { get; set; }

        /// <summary> 通常実働時間(休憩前労働時間)
        /// </summary>
        [JsonProperty("JIMUSYONAI_JITSUDO")]
        public string JIMUSYONAI_JITSUDO { get; set; }

        /// <summary> 夜間対応時間(休憩時間)
        /// </summary>
        [JsonProperty("YAKAN_TAIO")]
        public string YAKAN_TAIO { get; set; }

        /// <summary> 合計実働時間
        /// </summary>
        [JsonProperty("GOKEI_JITSUDO")]
        public string GOKEI_JITSUDO { get; set; }

        /// <summary> 普通残業時間
        /// </summary>
        [JsonProperty("ZAN_HUTSU")]
        public string ZAN_HUTSU { get; set; }

        /// <summary> 深夜残業時間
        /// </summary>
        [JsonProperty("ZAN_SINYA")]
        public string ZAN_SINYA { get; set; }

        /// <summary> 休日勤務
        /// </summary>
        [JsonProperty("KYUJITSU_KINMU")]
        public string KYUJITSU_KINMU { get; set; }

        /// <summary> 夜間待機
        /// </summary>
        [JsonProperty("YAKAN_TAIKI")]
        public string YAKAN_TAIKI { get; set; }

        /// <summary> 夜間 主・副・TR
        /// </summary>
        [JsonProperty("TETUYA_AKEKIN")]
        public string TETUYA_AKEKIN { get; set; }

        /// <summary> 私用外出回
        /// </summary>
        [JsonProperty("GAISYUTSU_KAI")]
        public decimal GAISYUTSU_KAI { get; set; }

        /// <summary> 私用外出時間
        /// </summary>
        [JsonProperty("GAISYUTSU_JIKAN")]
        public string GAISYUTSU_JIKAN { get; set; }

        /// <summary> 深夜時間
        /// </summary>
        [JsonProperty("SINYA_JIKAN")]
        public string SINYA_JIKAN { get; set; }

        /// <summary> 電故
        /// </summary>
        [JsonProperty("DENKO")]
        public bool DENKO { get; set; }

        /// <summary> 移動時間
        /// </summary>
        [JsonProperty("IDO_JIKAN")]
        public string IDO_JIKAN { get; set; }

        /// <summary> 備考
        /// </summary>
        [JsonProperty("BIKO")]
        public string BIKO { get; set; }

        /// <summary> エラーテキスト
        /// </summary>
        [JsonProperty("ERROR_TEXT")]
        public string ERROR_TEXT { get; set; }
    }
}