﻿using Newtonsoft.Json;

namespace PssWebKintai.Entity
{
    [JsonObject]
    public class M_KBN : BaseEntity
    {
        [JsonProperty("CODE")]
        public string CODE { get; set; }
        [JsonProperty("KEY1")]
        public string KEY1 { get; set; }
        [JsonProperty("KEY2")]
        public string KEY2 { get; set; }
        [JsonProperty("KEY3")]
        public string KEY3 { get; set; }
        [JsonProperty("VALUE1")]
        public string VALUE1 { get; set; }
        [JsonProperty("VALUE2")]
        public string VALUE2 { get; set; }
        [JsonProperty("VALUE3")]
        public string VALUE3 { get; set; }
        [JsonProperty("ORDER")]
        public int ORDER { get; set; }
        [JsonProperty("MEMO")]
        public string MEMO { get; set; }


    }
}