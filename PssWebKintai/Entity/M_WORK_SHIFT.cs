﻿using Newtonsoft.Json;

namespace PssWebKintai.Entity
{
    public class M_WORK_SHIFT : BaseEntity
    {
        /// <summary>
        /// </summary>
        [JsonProperty("WORK_TYPE")]
        public int WORK_TYPE { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SYUKKETSU_KBN")]
        public string SYUKKETSU_KBN { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("START")]
        public string START { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("END")]
        public string END { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("REST")]
        public string REST { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("ZAN")]
        public string ZAN { get; set; }
    }
}