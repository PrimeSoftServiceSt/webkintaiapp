﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PssWebKintai.Entity
{
    public class T_YAKAN : BaseEntity
    {
        /// <summary>
        /// </summary>
        [JsonProperty("EMPLOYEE_NO")]
        public string EMPLOYEE_NO { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("YEAR")]
        public int YEAR { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("MONTH")]
        public int MONTH { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_TAIO_JIKAN")]
        public decimal SUM_TAIO_JIKAN { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("SUM_SINYA_JIKAN")]
        public decimal SUM_SINYA_JIKAN { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty("DETAILS")]
        public List<T_YAKAN_DETAIL> DETAILS { get; set; }
    }
}