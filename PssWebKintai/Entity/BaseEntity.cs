﻿using Newtonsoft.Json;
using System;

namespace PssWebKintai.Entity
{
    [JsonObject]
    public class BaseEntity
    {
        [JsonProperty("CREATE_DATE")]
        public DateTime CREATE_DATE { get; set; }

        [JsonProperty("CREATE_USER")]
        public string CREATE_USER { get; set; }

        [JsonProperty("UPDATE_DATE")]
        public DateTime UPDATE_DATE { get; set; }

        [JsonProperty("UPDATE_USER")]
        public string UPDATE_USER { get; set; }
    }
}