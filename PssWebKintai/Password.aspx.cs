﻿using PssCommonLib;
using PssWebKintai.DBA;
using System;
using System.Reflection;
using System.Text.RegularExpressions;

namespace PssWebKintai
{
    public partial class Password : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                    txtNo.Focus();
                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                    Master.ShowErrorAlart();
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                if (txtNewPassword1.Text != txtNewPassword2.Text)
                {
                    Master.ShowAlart("新しいパスワードと新しいパスワード(確認)が一致しません。");
                    return;
                }
                else if (txtNewPassword1.Text.Length < 8 || txtNewPassword1.Text.Length > 20 || Regex.IsMatch(txtNewPassword1.Text, "[^0-9a-zA-Z]"))
                {
                    Master.ShowAlart("新しいパスワードは、8文字以上,20文字以下の半角英数(大小区別)で入力してください。");
                    return;
                }
                var dba = new DbaEmployee(string.Empty);
                var employee = dba.GetEmployee(txtNo.Text);

                if (employee == null || employee.PASSWORD != txtOldPassword.Text)
                {
                    Master.ShowAlart("社員番号またはパスワードが間違っています。");
                    return;
                }
                else
                {
                    dba.UpdatePassword(int.Parse(txtNo.Text), txtNewPassword1.Text);
                    Master.ShowAlart("パスワードを更新しました。");
                }
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), string.Empty);
                Response.Redirect("Default.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), string.Empty);
            }
        }
    }
}