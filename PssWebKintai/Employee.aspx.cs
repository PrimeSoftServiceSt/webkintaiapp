﻿using PssCommonLib;
using PssWebKintai.Common;
using PssWebKintai.DBA;
using PssWebKintai.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI.WebControls;

namespace PssWebKintai
{
    public partial class Employee : System.Web.UI.Page
    {
        #region メンバ変数
        private List<ListItem> lstPost;
        private List<ListItem> lstWorkType;
        private List<ListItem> lstSuperiorNo;
        private List<ListItem> lstRole;
        #endregion

        #region 初期化
        /// <summary> ページロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                    var dba = new DbaEmployee(Master.LoginEmployeeInfo.NO);

                    // 所属
                    lstPost = Util.CreateDrpDataSource(dba.GetPostList().Select(x => new ListItem() { Value = x.CODE.ToString(), Text = x.NAME }));
                    drpPost.SetItem(lstPost);

                    // 勤務形態
                    lstWorkType = Util.CreateDrpDataSource(dba.GetWorkTypeList().Select(x => new ListItem() { Value = x.WORK_TYPE.ToString(), Text = x.NAME }));
                    drpWorkType.SetItem(lstWorkType);

                    // 上長
                    lstSuperiorNo = Util.CreateDrpDataSource(dba.GetSuperiorNoList().Select(x => new ListItem() { Value = x.NO.ToString(), Text = x.NAME }));
                    drpSuperiorNo.SetItem(lstSuperiorNo);

                    // 権限
                    lstRole = Util.CreateDrpDataSource(dba.GetRoleList().Select(x => new ListItem() { Value = x.KEY1, Text = x.VALUE1 }));
                    drpRole.SetItem(lstRole);

                    ListView1.DataSource = new List<M_EMPLOYEE>();
                    ListView1.DataBind();
                }
                catch (Exception ex)
                {
                    PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                    Master.ShowErrorAlart();
                }
                finally
                {
                    PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                }
            }
        }
        #endregion

        #region イベント
        /// <summary> 検索ボタン押下時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var dba = new DbaEmployee(Master.LoginEmployeeInfo.NO);
                ListView1.DataSource = dba.GetEmployeeList(txtNo.Text, txtName.Text, drpPost.SelectedValue, drpWorkType.SelectedValue, drpSuperiorNo.SelectedValue, drpRole.SelectedValue);
                ListView1.DataBind();
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 戻るボタン押下時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                Response.Redirect("Menu.aspx");
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart();
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }
        #endregion

        #region グリッドイベント
        /// <summary> 部門ドロップダウン初期化のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void POST_Init(object sender, EventArgs e)
        {
            try
            {
                if (lstPost == null || lstPost.Count == 0)
                {
                    var dba = new DbaEmployee(string.Empty);
                    lstPost = Util.CreateDrpDataSource(dba.GetPostList().Select(x => new ListItem() { Value = x.CODE.ToString(), Text = x.NAME }), emptyValue: "0");
                }
                var drp = (DropDownList)sender;
                drp.SetItem(lstPost);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
        }

        /// <summary> 勤務形態ドロップダウン初期化のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void WORK_TYPE_Init(object sender, EventArgs e)
        {
            try
            {
                if (lstWorkType == null || lstWorkType.Count == 0)
                {
                    var dba = new DbaEmployee(string.Empty);
                    lstWorkType = Util.CreateDrpDataSource(dba.GetWorkTypeList().Select(x => new ListItem() { Value = x.WORK_TYPE.ToString(), Text = x.NAME }));
                }
                var drp = (DropDownList)sender;
                drp.SetItem(lstWorkType);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
        }

        /// <summary> 上長ドロップダウン初期化のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SUPERIOR_NO_Init(object sender, EventArgs e)
        {
            try
            {
                if (lstSuperiorNo == null || lstSuperiorNo.Count == 0)
                {
                    var dba = new DbaEmployee(string.Empty);
                    lstSuperiorNo = Util.CreateDrpDataSource(dba.GetSuperiorNoList().Select(x => new ListItem() { Value = x.NO.ToString(), Text = x.NAME }));
                }
                var drp = (DropDownList)sender;
                drp.SetItem(lstSuperiorNo);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
        }

        /// <summary> 権限ドロップダウン初期化のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ROLE_Init(object sender, EventArgs e)
        {
            try
            {
                if (lstRole == null || lstRole.Count == 0)
                {
                    var dba = new DbaEmployee(string.Empty);
                    lstRole = Util.CreateDrpDataSource(dba.GetRoleList().Select(x => new ListItem() { Value = x.KEY1, Text = x.VALUE1 }));
                }
                var drp = (DropDownList)sender;
                drp.SetItem(lstRole);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
        }

        /// <summary> 登録ボタン押下時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InsertBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var btn = (Button)sender;
                var item = btn.GetParentItem();
                var employee = GetEmployeeByControl(item);

                // 入力チェック
                if (employee.NO == string.Empty)
                {
                    Master.ShowAlart("社員番号を入力してください。", UpdatePanel1);
                    return;
                }
                else if (employee.NO.ToString().Length > 6)
                {
                    Master.ShowAlart("社員番号を6桁以内で入力してください。", UpdatePanel1);
                    return;
                }
                // 重複チェック
                var dba = new DbaEmployee(Master.LoginEmployeeInfo.NO);
                if (dba.GetEmployee(employee.NO) != null)
                {
                    Master.ShowAlart("社員番号が重複しています。", UpdatePanel1);
                    return;
                }
                // 登録
                dba.InsertEmployee(employee);

                ListView1.DataSource = dba.GetEmployeeList(txtNo.Text, txtName.Text, drpPost.SelectedValue, drpWorkType.SelectedValue, drpSuperiorNo.SelectedValue, drpRole.SelectedValue);
                ListView1.DataBind();

                Master.ShowAlart("社員情報を登録しました。", UpdatePanel1);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 更新ボタン押下時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var btn = (Button)sender;
                var item = btn.GetParentItem();
                var employee = GetEmployeeByControl(item);

                // 存在チェック
                var dba = new DbaEmployee(Master.LoginEmployeeInfo.NO);
                if (dba.GetEmployee(employee.NO) == null)
                {
                    Master.ShowAlart("存在しない社員番号です。", UpdatePanel1);
                    return;
                }
                // 更新
                dba.UpdateEmployee(employee);

                ListView1.DataSource = dba.GetEmployeeList(txtNo.Text, txtName.Text, drpPost.SelectedValue, drpWorkType.SelectedValue, drpSuperiorNo.SelectedValue, drpRole.SelectedValue);
                ListView1.DataBind();

                Master.ShowAlart("社員情報を登録しました。", UpdatePanel1);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        /// <summary> 削除ボタン押下時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var btn = (Button)sender;
                var item = btn.GetParentItem();
                var employee = GetEmployeeByControl(item);

                // 存在チェック
                var dba = new DbaEmployee(Master.LoginEmployeeInfo.NO);
                if (dba.GetEmployee(employee.NO) == null)
                {
                    Master.ShowAlart("存在しない社員番号です。", UpdatePanel1);
                    return;
                }
                // 削除
                dba.DeleteEmployee(employee);

                ListView1.DataSource = dba.GetEmployeeList(txtNo.Text, txtName.Text, drpPost.SelectedValue, drpWorkType.SelectedValue, drpSuperiorNo.SelectedValue, drpRole.SelectedValue);
                ListView1.DataBind();

                Master.ShowAlart("社員情報を削除しました。", UpdatePanel1);
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                Master.ShowErrorAlart(UpdatePanel1);
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }
        #endregion

        #region メソッド
        /// <summary> １行分の社員情報を取得する
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private M_EMPLOYEE GetEmployeeByControl(ListViewItem item)
        {
            try
            {
                PssLog.WriteStartLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
                var txtNo = item.GetControl<TextBox>("NO");
                var txtName = item.GetControl<TextBox>("NAME");
                var drpPost = item.GetControl<DropDownList>("POST");
                var txtLank = item.GetControl<TextBox>("LANK");
                var drpWorkType = item.GetControl<DropDownList>("WORK_TYPE");
                var drpSuperiorNo = item.GetControl<DropDownList>("SUPERIOR_NO");
                var drpRole = item.GetControl<DropDownList>("KINTAI_KENGEN");

                var employee = new M_EMPLOYEE
                {
                    NO = txtNo.Text,
                    NAME = txtName.Text,
                    POST = int.TryParse(drpPost.SelectedValue, out int i) ? i : 0,
                    LANK = txtLank.Text,
                    WORK_TYPE = int.TryParse(drpWorkType.SelectedValue, out i) ? i : 0,
                    SUPERIOR_NO = drpSuperiorNo.SelectedValue,
                    KINTAI_KENGEN = int.TryParse(drpRole.SelectedValue, out i) ? i : (int)Role.Normal
                };

                return employee;
            }
            catch (Exception ex)
            {
                PssLog.WriteErrorLog(MethodBase.GetCurrentMethod(), ex);
                throw ex;
            }
            finally
            {
                PssLog.WriteEndLog(MethodBase.GetCurrentMethod(), Master.LoginEmployeeInfo.NO);
            }
        }

        #endregion
    }
}